\documentclass{beamer}
%
% Choose how your presentation looks.
%
% For more themes, color themes and font themes, see:
% http://deic.uab.es/~iblanes/beamer_gallery/index_by_theme.html
%
\mode<presentation>
{
  \usetheme{CambridgeUS}      % or try Darmstadt, Madrid, Warsaw, ...
  \usecolortheme{default} % or try albatross, beaver, crane, ...
  \usefonttheme{default}  % or try serif, structurebold, ...
  \setbeamertemplate{navigation symbols}{}
  \setbeamertemplate{caption}[numbered]
}

\usepackage[english]{babel}
\usepackage[utf8x]{inputenc}
\usepackage{graphicx}


\title[Computational Neuroscience]{Computational Neuroscience: Mathematical Tools to Understand the Brain}
\author{Joseph Koblitz}
\institute{Seattle University}
\date{\today}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}{Outline}
  \tableofcontents
\end{frame}

\section{The Brain}
\subsection{Quick Overview}
\begin{frame}{The Big Picture}
  \begin{center}
    Congratulations, you're reading this sentence!
  \end{center}
  \pause
  \begin{itemize}
    \item{There are 12 cranial nerves in the brain, each a collection of neurons and other cells.}
    \begin{itemize}
      \item{Olfactory}
      \item{Optic}
      \item{Vestibulocochlear}
    \end{itemize}
    \pause
    \item{The brain has approximately 86 billion neurons in the brain.\cite{10.3389/neuro.09.031.2009}}
    \pause
    \item{Warren Buffett's Net Worth $\approx 86$ billion dollars}
  \end{itemize}
  \pause
  \textbf{If he spent 86000 dollars every day since 01/01/0000, he would still have money left over ($\approx 12$ billion).}
\end{frame}

\subsection{Neurons}
\begin{frame}{The Neuron}
  \begin{itemize}
    \item{Two Types:}
    \begin{itemize}
      \item{Efferent - Motor}
      \item{Afferent - Sensory}
    \end{itemize}
    \pause
    \item{``Neural Network''}
    \begin{itemize}
      \item{Neurons communicate through electro-chemical signals at a junction called a synapse.}
      \item{Some types of neurons have $\approx 7000$ synaptic connections.\cite{Drachman2004}}
      \item{All in all, we have somewhere around an estimated 100 trillion connections in our brain.}
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{Breaking it Down}
  \begin{itemize}
    \item{So where do we even start?}
    \begin{itemize}
      \item{Pick a nerve type (i.e. Vision, smelling, hearing...)}
      \item{Pick an animal (i.e. Macaque, rat, owl...)}
      \item{Give it a stimulus and record the output of the neurons}
      \item{Pick some neurons and start modeling!}
    \end{itemize}
    %definitions?
  \end{itemize}
  \vspace{.5cm}
  \pause
  \textbf{The Goal:} To mimic the response of our neurons given the same stimulus.
\end{frame}

\section{The Classical Analysis}

\subsection{Linear-Nonlinear Models}
\begin{frame}{Linear-Nonlinear (LN) Models}
  \begin{itemize}
    \item{A common neuroinformatic model is the Linear-Nonlinear model}
    \item{A Big Picture approach:}
    \begin{itemize}
      \item{Estimate linear filter(s), generally via STA/STC}
      \item{Pick a fixed nonlinear function that maps a linear combination of features and weights (feature importance) to an output firing rate}
      \item{Use the output firing rate to create a probabilistic model, generally the Poisson model}
    \end{itemize}
  \end{itemize}
\end{frame}

\subsection{Generalized Linear Models}
\begin{frame}{Generalized Linear Models}
  \begin{itemize}
    \item{GLMs are a type of LN model}
    \item{GLMs are a generalized form of linear regression which requires:}
    \begin{itemize}
      \item A distribution model from the exponential family
      \item An invertible link function (our nonlinearity), $g$, such that $\mu = g^{-1}(\boldmath{X\beta})$
    \end{itemize}
  \end{itemize}
  \begin{figure}[ht!]
    \centering
    \includegraphics[width=60mm]{Linear_regression.jpg}
    \caption{Example Simple Linear Regression Plot}
  \end{figure}
\end{frame}

\begin{frame}{Least Squares Estimation}
  When finding our parameters, $\beta$, we could pick random values, but that wouldn't make for a very accurate model (in general). How might we \textbf{optimize} our model?

  \textbf{Least Squares Estimation:}
  Define a cost function 
  $$C = \sum_{i=1}^{n}(y_i-\hat{y}_i)^2 \text{, where } \hat{y}_i = \sum_{j = 0}^{m} \beta_j x_{ij}$$
  $$\frac{\partial C}{\partial \beta_k} = -2 \sum_{i=1}^{n} \left(y_i- \sum_{j=0}^{m} \beta_j x_{ij} \right) x_{ik}$$

\end{frame}

\begin{frame}{Least Squares Estimation (cont'd)}
  Setting our derivative equal to zero gives:
  \begin{align*}
    0 &= -2 \sum_{i=1}^{n} \left(y_i - \sum_{j=0}^m \beta_j x_{ij} \right) x_{ik} \\
    \sum_{i=1}^n \sum_{j=0}^m \beta_j x_{ij} x_{ik} &= \sum_{i=1}^n x_{ik} y_i \\
    \sum_{j=0}^m \left( \sum_{i=1}^n x_{ij}x_{ik} \right) \beta_j &= \sum_{i=1}^n x_{ik} y_i \\
    \boldmath{X^TX\beta} &= \boldmath{X^Ty}\\
    \beta &= \boldmath{(X^TX)^{-1} (X^Ty)}
  \end{align*}
\end{frame}

\begin{frame}{Formalities}
  \begin{itemize}
    \item{Formally, let $r(t)$ be the response of the neuron and $\vec{s}(t)$ be our stimulus at time $t$, while $g(\cdot)$ is our fixed nonlinear function.}
    \item{Let our stimulus have $N_x$ spatial dimensions taken at $N_t$ discrete times. Then the dimension of our stimulus is $N = N_x \cdot N_t$}
  \end{itemize}
  \begin{equation}
    r(t)=g(r(t'),\vec{s}(t')) \text{ where } t' < t
  \end{equation}
\end{frame}

\begin{frame}{Spike Train History}
  To improve the GLM further, we can use \textbf{Spike Train History}.
  \begin{equation}
    r(t)=g(r(t'),\vec{s}(t')) \text{ where } t' < t
  \end{equation}
  \begin{figure}
    \centering
    \includegraphics[width=.5\textwidth]{SpikeTrainHistory.png}
    \caption{Diagram of Procedure}
  \end{figure}
  \vspace{-.3cm}
  \pause
  \textbf{What are some other approaches to predicting neural responses?}
\end{frame}

\section{Machine Learning}
\subsection{Unsupervised Machine Learning}
\begin{frame}{Machine Learning -- Unsupervised}
  \begin{enumerate}
    \item Unsupervised
    \begin{enumerate}
      \item What: Learning where the data is ``unlabeled''
      \item Why: To figure out the ``structure'' of our dataset
      \item Problems: Clustering, Anomaly Detection, Auto-Encoding
    \end {enumerate}
    \item Up Next... Why unsupervised learning could be the root of fashion struggles!
  \end{enumerate}
\end{frame}

\begin{frame}{Fashion and Math}
  A \textbf{K-Means} Approach:

  ``$k$-means clustering aims to partition $n$ observations into $k$ clusters in which each observation belongs to the cluster with the nearest mean, serving as a prototype of the cluster.''

  \vspace{.2cm}
  \pause
  In fashion terms:
  \begin{figure}
    \centering
    \begin{minipage}[b]{0.40\textwidth}
        \centering
        \includegraphics[width=0.9\textwidth]{unsortedTShirts.jpg}
        \caption{Before K-Means}
    \end{minipage}\hfill
    \pause
    \begin{minipage}[b]{0.55\textwidth}
        \centering
        \includegraphics[width=0.9\textwidth]{sortedTShirts.jpg}
        \caption{K-Means with $K = 3,5$}
    \end{minipage}
  \end{figure}
\end{frame}

\subsection{Supervised Machine Learning}
\begin{frame}{Machine Learning -- Supervised}
  \begin{enumerate}
  \item Supervised
    \begin{enumerate}
      \item What: Learning where we're given the ``labeled'' data
      \item Why: To make accurate predictions based on previous data
      \item Problems: Anything you can gather sufficient data on! The most common example is real estate or the cost of a house.
    \end {enumerate}
  \end{enumerate}

  \begin{figure}
    \centering
    \includegraphics[width=60mm]{LogisticRegressionExample.jpg}
    \caption{Pass-Fail Logistic Regression Model}
  \end{figure}

\end{frame}

\section{Predicting Spikes}
\subsection{Machine Learning Vs. Classical}
\begin{frame}{Machine Learning and Neuroscience}
  \begin{itemize}
    \item{Classic methods have been and continue to be effective, but machine learning (ML) is on the rise.}
    \pause
    \item{Worth noting:}
    \begin{itemize}
      \item{ML is less ``transparent''}
      \item{ML makes less assumptions about our data}
      \item{ML is becoming much more simple to write code for}
    \end{itemize}
    \pause
    \item{GLMs rely on a linearity assumption about how cells interpret stimuli, but ML algorithms may be able to capture arbitrary nonlinearity in our stimulus.}
    \pause
    \item{Through hyperparameters, we can optimize ML algorithms easily to vary our model to emphasize different aspects.}
  \end{itemize}
  Let's see walk through an example...
\end{frame}

\begin{frame}{Decision Tree}
  \begin{figure}
    \centering
    \includegraphics[width=0.75\textwidth]{DecisionTreeExample.png}
    \caption{Sci-Kit Learn's Decision Tree Example}
  \end{figure}
\end{frame}

\subsection{Hyperparameters}
\begin{frame}{Hyperparameters}
  ``In machine learning, a hyperparameter is a parameter whose value is set before the learning process begins. By contrast, the values of other parameters are derived via training.''

  \begin{figure}[h]
    \centering
    \includegraphics[width=\textwidth]{DecisionTreeHP.png}
    \caption{Sci-Kit Learn's Function for a Decision Tree}
  \end{figure}
\end{frame}

\begin{frame}{Optimizing Hyperparameters}
  So how do we optimize hyperparameters? Some common approaches are:
  \begin{enumerate}
    \item {Grid Search}
    \item {Random Search}
    \item {Gradient-Based Optimization}
  \end{enumerate}
\end{frame}
%slide here on results/my process/values
\section{Conclusion}
\begin{frame}{Comparison}
  \begin{figure}
    \centering
    \begin{minipage}[b]{0.55\textwidth}
        \centering
        \includegraphics[width=0.6\textwidth]{panic1.png}
        \caption{Single Neuron pR2 Values\cite{Benjamin111450}}
    \end{minipage}\hfill
    \pause
    \begin{minipage}[b]{0.45\textwidth}
        \centering
        \includegraphics[width=0.5\textwidth]{panic2.png}
        \caption{Mean pR2 Values\cite{Benjamin111450}}
    \end{minipage}
  \end{figure}
\end{frame}

\begin{frame}{Putting it Back Together}
  \begin{itemize}
    \item So what might a combined approach to computational neuroscience look like?
    \pause
    \begin{enumerate}
      \item{Use a machine learning model to test some suspected useful features}
      \pause
      \item{Use hyperparameters to try and improve our ML model}
      \pause
      \item{Return to GLMs to try and understand changes we can make to get approximations closer to ML accuracy}
    \end{enumerate}
  \end{itemize}
\end{frame}

\begin{frame}{Conclusion}
  \begin{itemize}
    \item{Computational Neuroscience is a large field of research with plenty of challenges to consider.}
    \begin{itemize}
      \item{Scale of the brain}
      \item{Variety in types of neurons}
      \item{Open-data challenges}
      \item{New technologies to learn}
    \end{itemize}
    \pause
    \item{GLMs offer key insights into what assumptions, approaches, and structures might offer deeper understanding of how the neuron is understanding its stimulus.}
    \pause
    \item{Machine learning is on the rise and may be a better choice for quick decisions or baselines for whether or not certain features are good indicators of the cell's response.}
  \end{itemize}
\end{frame}

\begin{frame}{Acknowledgements}
  \centering
  I would like to give a big thank you to my advisor, Dr. Fischer, for all his help over this year (especially during his sabbatical).

  I would also like to thank Elsa Magness for helping me revise this from its painful inception to it's slightly less painful current state.
\end{frame}

\begin{frame}
  \centering \Large
  \textbf{Thank you!}
\end{frame}

\newpage

\bibliographystyle{ieeetr}
\bibliography{neuronNum}

% \begin{itemize}
% \item Use \texttt{tabular} for basic tables --- see Table~\ref{tab:widgets}, for example.
% \item You can upload a figure (JPEG, PNG or PDF) using the files menu.
% \item To include it in your document, use the \texttt{includegraphics} command (see the comment below in the source code).
% \end{itemize}

% \begin{table}
% \centering
% \begin{tabular}{l|r}
% Item & Quantity \\\hline
% Widgets & 42 \\
% Gadgets & 13
% \end{tabular}
% \caption{\label{tab:widgets}An example table.}
% \end{table}

% \subsection{eh}

%\begin{itemize}
%  \item{We make a dimensionality reduction as our first step, so let $\phi_i$ be our important vectors where $i = 1,2,...K$ and $K < N$.}\item{We call $\phi_i$ a feature vector. Let's project the stimulus onto the feature vectors such that $z_i = \phi_i^{T} \vec{s}$}
%  \item{Thus our equation becomes:}
%\end{itemize}
%\begin{equation}
%  r(t)=g(z_1,z_2,...z_K,r(t'))
%\end{equation}

%  Maximum likelihood estimation:
%$$\prod_{i=1}^{n} f(y_{i} | \theta ) =
%\frac{\theta^{\sum y_{i}} e^{-n \theta}}
%{y_{1}!y_{2}! \ldots y_{n}!}$$

%$$ \log L = \left(\sum y_{i} \right) \log \theta - n \theta - \sum (\log y_{i}!)$$
%To find the optima we take the derivative with respect to theta and solve for zero to get our maximum.
%$$ \hat{\theta} = \sum y_{i}/n $$
\end{document}
