\babel@toc {english}{}
\beamer@sectionintoc {1}{The Brain}{3}{0}{1}
\beamer@subsectionintoc {1}{1}{Quick Overview}{3}{0}{1}
\beamer@subsectionintoc {1}{2}{Neurons}{8}{0}{1}
\beamer@sectionintoc {2}{The Classical Analysis}{12}{0}{2}
\beamer@subsectionintoc {2}{1}{Linear-Nonlinear Models}{12}{0}{2}
\beamer@subsectionintoc {2}{2}{Generalized Linear Models}{13}{0}{2}
\beamer@sectionintoc {3}{Machine Learning}{19}{0}{3}
\beamer@subsectionintoc {3}{1}{Unsupervised Machine Learning}{19}{0}{3}
\beamer@subsectionintoc {3}{2}{Supervised Machine Learning}{23}{0}{3}
\beamer@sectionintoc {4}{Predicting Spikes}{24}{0}{4}
\beamer@subsectionintoc {4}{1}{Machine Learning Vs. Classical}{24}{0}{4}
\beamer@subsectionintoc {4}{2}{Hyperparameters}{29}{0}{4}
\beamer@sectionintoc {5}{Conclusion}{31}{0}{5}
