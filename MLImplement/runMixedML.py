"""http://scikit-learn.org/stable/auto_examples/applications/plot_face_recognition.html#sphx-glr-auto-examples-applications-plot-face-recognition-py"""
#http://scikit-learn.org/stable/modules/classes.html
from time import time
from sklearn.neural_network import MLPClassifier
from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier
from sklearn.tree import DecisionTreeClassifier

def run_multiple_algs(X_train, y_train, X_test, y_test):

    print("Fitting the classifier to the training set")
    t0 = time()
    clf = [DecisionTreeClassifier(), RandomForestClassifier(),SVC(), MLPClassifier()]
    for my_clf in clf:
        my_clf = my_clf.fit(X_train, y_train)
    print("done in %0.3fs" % (time() - t0))

    print("Predicting people's names on the test set")
    t0 = time()
    pred = []
    for my_clf in clf:
        pred.append(my_clf.predict(X_test))
    print("done in %0.3fs" % (time() - t0))

    return y_test, pred
