# coding: utf-8

# # Beyond GLMs
# This notebook accompanies "Modern Machine Learning Far Outperforms GLMs at Predicting Spikes". We implement various Machine Learning algorithms for spike prediction and offer this as a Python template.
#
# ### Table of contents
# 0. Loading data
# 0. Define helper functions for scoring and cross-validation
# 0. Define Models
#     0. GLM
#     0. XGBoost
#     0. Neural Network
#     0. Random Forest
#     0. Ensemble
# 0. Nested cross-validation (for evaluating the ensemble)
# 0. Model Comparison
#     0. M1 original features
#     0. M1 engineered features
#     0. M1 all neurons
# 0. Appendix 1: Hyperparameter optimization
# 0. Appendix 2: Running R's glmnet in python

# ## Dependencies
# Basics
# - numpy
# - pandas
# - scipy
# - matplotlib
#
# Methods
# - sklearn
# - pyglmnet (glm)
# - xgboost
# - theano (NN)
# - keras (NN)
#
# Other
# - BayesOpt (Bayesian optimization for better hyperparameters)

import warnings, sys, os, time
warnings.filterwarnings('ignore')

import numpy as np
import pandas as pd
import scipy.io
from sklearn.model_selection import KFold
from pyglmnet import GLM
import xgboost as xgb
from sklearn.ensemble import RandomForestRegressor
from sklearn.neighbors import KNeighborsRegressor
from sklearn.linear_model import LinearRegression

import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

BINARY = False

def poisson_pseudoR2(y, yhat, ynull):
    # This is our scoring function. Implements pseudo-R2
    yhat = yhat.reshape(y.shape)
    eps = np.spacing(1)
    L1 = np.sum(y*np.log(eps+yhat) - yhat)
    L1_v = y*np.log(eps+yhat) - yhat
    L0 = np.sum(y*np.log(eps+ynull) - ynull)
    LS = np.sum(y*np.log(eps+y) - y)
    R2 = 1-(LS-L1)/(LS-L0)
    return R2

def binomial_R2(y, yhat, ynull): #actual, prob, mean
    yhat = yhat.reshape(y.shape) #what is this??
    eps = np.spacing(1)
    L1 = np.sum(y*np.log(eps+yhat)+((1-y)*np.log(eps+(1-yhat))))

    L0 = np.sum(y*np.log(eps+ynull)+(1-y)*np.log(eps+(1-ynull)))
    LS = np.sum(y*np.log(eps+y)+(1-y)*np.log(eps+(1-y)))
    R2 = 1-(LS-L1)/(LS-L0)
    return R2



def fit_cv(X, Y, algorithm, n_cv=10, verbose=1, binary=False):
    """Performs cross-validated fitting. Returns (Y_hat, pR2_cv); a vector of predictions Y_hat with the
    same dimensions as Y, and a list of pR2 scores on each fold pR2_cv.

    X  = input data
    Y = spiking data
    algorithm = a function of (Xr, Yr, Xt) {training data Xr and response Yr and testing features Xt}
                and returns the predicted response Yt
    n_cv = number of cross-validations folds

    """
    if np.ndim(X)==1:
        X = np.transpose(np.atleast_2d(X))

    #http://scikit-learn.org/stable/modules/generated/sklearn.model_selection.KFold.html
    cv_kf = KFold(n_splits=n_cv, shuffle=True, random_state=42)
    skf  = cv_kf.split(X)

    i=1
    Y_hat=np.zeros(len(Y))
    pR2_cv = list()
    for idx_r, idx_t in skf:
        if verbose > 1:
            print( '...runnning cv-fold', i, 'of', n_cv)
        i+=1
        Xr = X[idx_r, :]
        Yr = Y[idx_r]
        Xt = X[idx_t, :]
        Yt = Y[idx_t]

        if(algorithm != "xgb_run"):
            Yt_hat = eval(algorithm)(Xr, Yr, Xt)
        else:
            Yt_hat = eval(algorithm)(Xr, Yr, Xt, binary)
        #print("Length: {}".format(len(Yt_hat)))
        #print("Sample: {}".format(Yt_hat[0]))
        Y_hat[idx_t] = Yt_hat

        if(BINARY):
            pR2 = binomial_R2(Yt, Yt_hat, np.mean(Yr))
        else:
            pR2 = poisson_pseudoR2(Yt, Yt_hat, np.mean(Yr))
        pR2_cv.append(pR2)

        if verbose > 1:
            print( 'pR2: ', pR2)

    if verbose > 0:
        print("pR2_cv: %0.6f (+/- %0.6f)" % (np.mean(pR2_cv),
                                             np.std(pR2_cv)/np.sqrt(n_cv)))
    return Y_hat, pR2_cv


# GLM
# Note: Different problems may require different regularization parameters __alpha__ and __reg_lambda__. The __learning_rate, tol__, and __max_iter__ should also be adjusted to ensure convergence (they can be touchy).

def glm_pyglmnet(Xr, Yr, Xt):
    glm = GLM(distr='softplus', alpha=0.1, tol=1e-8, verbose=0,
              reg_lambda=np.logspace(np.log(0.05), np.log(0.0001), 10, base=np.exp(1)),
              learning_rate=2, max_iter=10000, eta=2.0, random_state=1)


    glm.fit(Xr, Yr)
    Yt = glm[-1].predict(Xt)

    return Yt


# ### XGBoost
#
# Note: Many of these parameters __(learning rate, # estimators, subsampling, max_depth, and gamma)__ should be optimized for the prediction problem at hand. Optimization can be done with a grid search, randomized search, or with Bayesian Optimization (see appendix at bottom.)

def xgb_run(Xr, Yr, Xt, binary):
    #http://xgboost.readthedocs.io/en/latest/parameter.html#learning-task-parameters
    if(binary):
        params = {'objective': "binary:logistic",
                  'eval_metric': "logloss", #loglikelihood loss
                  'seed': 2925, #for reproducibility
                  'silent': 1,
                  'learning_rate': 0.05,
                  'min_child_weight': 2, 'n_estimators': 580,
                  'subsample': 0.6, 'max_depth': 5, 'gamma': 0.4}
    else:
        params = {'objective': "count:poisson",
                  'eval_metric': "logloss", #loglikelihood loss
                  'seed': 2925, #for reproducibility
                  'silent': 1,
                  'learning_rate': 0.05,
                  'min_child_weight': 2, 'n_estimators': 580,
                  'subsample': 0.6, 'max_depth': 5, 'gamma': 0.4}

    dtrain = xgb.DMatrix(Xr, label=Yr)
    dtest = xgb.DMatrix(Xt)

    num_round = 200
    bst = xgb.train(params, dtrain, num_round)

    Yt = bst.predict(dtest)
    return Yt


# ### Neural Nets
#
# Note: Again, these parameters should be optimized. We highlight __dropout__, elastic net regularization __l1, l2__, and the number of nodes in the hidden layers. Optimization can be done with a grid search, randomized search, or with Bayesian Optimization (see appendix at bottom.)
#
# There are many, many options for implementing NNs. One might also test maxnorm regularization, e.g. RMSprop instead of Nadam, more or less layers, or different batch sizes or number of epochs.

"""
from keras.models import Sequential
from keras.layers.core import Dense, Dropout, Activation, Lambda
from keras.regularizers import l1l2
from keras.optimizers import Nadam


def nn(Xr, Yr, Xt):

    params = {'dropout': 0.5,
              'l1': 0.0,
              'l2': 0.0,
              'n1': 1980, #number of layers in 1st hidden layer
              'n2': 18}

    if np.ndim(Xr)==1:
        Xr = np.transpose(np.atleast_2d(Xr))

    model = Sequential()
    model.add(Dense(params['n1'], input_dim=np.shape(Xr)[1], init='glorot_normal',
                activation='relu', W_regularizer=l1l2(params['l1'],params['l2'])))
    model.add(Dropout(params['dropout']))
    model.add(Dense(params['n2'], init='glorot_normal'
                    , activation='relu',W_regularizer=l1l2(params['l1'],params['l2'])))
    model.add(Dense(1,activation='softplus'))

    optim = Nadam()
    model.compile(loss='poisson', optimizer=optim,)
    hist = model.fit(Xr, Yr, batch_size = 32, nb_epoch=5, verbose=0, validation_split=0.0)
    Yt = model.predict(Xt)[:,0]
    return Yt
"""


# ### Other methods
# These methods aren't highlighted in the paper but may help to improve the ensemble.
# ##### Random Forest

def rf(Xr, Yr, Xt):
    params = {'max_depth': 15,
             'min_samples_leaf': 4,
             'min_samples_split': 5,
             'min_weight_fraction_leaf': 0.0,
             'n_estimators': 471}

    clf = RandomForestRegressor(**params)
    clf.fit(Xr, Yr)
    Yt = clf.predict(Xt)
    return Yt


# ##### K-nearest neighbors

def knn(Xr, Yr, Xt):
    neigh = KNeighborsRegressor(n_neighbors=10,weights='distance')
    neigh.fit(Xr, Yr)
    Yt = neigh.predict(Xt)
    #returns list of probabilities for each category
    return Yt

def lin_comb(Xr, Yr, Xt):
    lr = LinearRegression()
    lr.fit(Xr, Yr)
    Yt = lr.predict(Xt)

    #rectify outputs
    Yt = np.maximum(Yt,np.zeros(Yt.shape))
    return Yt


# ## Method comparison
#
# Let's take a single neuron from the M1 set and test the above methods.
#

def menu():
    filename = input("Enter your .mat filepath: ")
    num_inputs = int(input("Enter how many entries(rows) you'd like: "))


def main():
    #load in matlab file
    if(len(sys.argv) != 1):
        menu()
    else:
        m1_imported = scipy.io.loadmat('../data/transposedData.mat')
        num_inputs = 1000
        #pull data into pandas
        data = pd.DataFrame()
        data['Xr0'] = m1_imported['Xr'][0]
        data['Xr1'] = m1_imported['Xr'][1]
        data['Xr2'] = m1_imported['Xr'][2]
        data.head()

        BINARY = False

        #We'll store results here.
        Models = dict()

        """
        Yt_hat, PR2 = fit_cv(X, y, algorithm = 'glm_pyglmnet', n_cv=8, verbose=2)

        Models['glm'] = dict()
        Models['glm']['Yt_hat'] = Yt_hat
        Models['glm']['PR2'] = PR2
        """

        #methods = ['xgb_run','rf','knn']

        method = 'knn'

        output_file = open("run_tests.txt", "a")
        output_file.write("\nNew Test! \n")

        for i in range(3, 8):
            num_inputs = 10**i
            X = data[['Xr0','Xr1','Xr2']].values[0:num_inputs]
            y = m1_imported['Yr'][0:num_inputs]
            print('Running '+method+'...')
            start = time.perf_counter()
            Yt_hat, PR2 = fit_cv(X, y, algorithm = method, n_cv=8, verbose=1,binary=BINARY)
            Models[method] = dict()
            Models[method]['Yt_hat'] = Yt_hat
            Models[method]['PR2'] = PR2
            timer = (time.perf_counter()-start)
            if BINARY: b = "Binary"
            else: b = "Poisson"
            string = "{} {}: n = {} t = {} PR2 = {}\n".format(b, method, num_inputs, timer, np.mean(PR2))
            output_file.write(string)

        output_file.close()
    #default vs menu
    #in same file, grab new one for fold
    #train on full first
    #run on full second
    #check random state

    #at least partial features

    
main()
