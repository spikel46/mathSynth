# ## Dependencies
# Basics
# - numpy
# - pandas
# - scipy
# - matplotlib
#
# Methods
# - sklearn
# - xgboost
#
# Other
# - BayesOpt (Bayesian optimization for better hyperparameters)

import warnings, sys, os, time, csv
from os import listdir
from os.path import isfile, join
warnings.filterwarnings('ignore')

import numpy as np
import pandas as pd
import scipy.io
from sklearn.model_selection import KFold
from pyglmnet import GLM
import xgboost as xgb
from sklearn.ensemble import RandomForestRegressor
from sklearn.neighbors import KNeighborsRegressor
from sklearn.linear_model import LinearRegression

import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

def simpleaxis(ax):
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.get_xaxis().tick_bottom()
    ax.get_yaxis().tick_left()
    ax.xaxis.set_tick_params(size=6)
    ax.yaxis.set_tick_params(size=6)

def binomial_R2(y, yhat, ynull, yrate): #actual, prob, mean
    print(ynull)
    yhat = yhat.reshape(y.shape) #what is this??
    eps = 10**(-1*8)
    L1 = np.sum(y*np.log(eps+yhat)+((1-y)*np.log((1-yhat)+eps)))
    L0 = np.sum(y*np.log(eps+ynull)+(1-y)*np.log((1-ynull)+eps))
    LS = np.sum(y*np.log(eps+yrate)+(1-y)*np.log((1-yrate)+eps))
    R2 = 1-(LS-L1)/(LS-L0)
    return R2


def xgb_run(Xr, Yr, Xt):
    #http://xgboost.readthedocs.io/en/latest/parameter.html#learning-task-parameters
    params = {'objective': "binary:logistic",
              'eval_metric': "logloss", #loglikelihood loss
              'seed': 2925, #for reproducibility                                                      ????
              'silent': 0, #if you want to see output
              'learning_rate': 0.05,
              'min_child_weight': 2, 'n_estimators': 580,
              'subsample': 0.6, 'max_depth': 5, 'gamma': 0.4}

    dtrain = xgb.DMatrix(Xr, label=Yr)
    dtest = xgb.DMatrix(Xt)

    num_round = 200
    bst = xgb.train(params, dtrain, num_round)

    Yt = bst.predict(dtest)
    return Yt, bst


def main():

    #describes path where feature importance images are going to be stored
    save_path = "./figures"

    #defines path where .mat files are stored

    #data_path = "../data" #for joey's macbook
    data_path = "D:\School\FischerResearch\Data\Data\XBGpre" #for joey's desktop
    onlyfiles = [join(data_path, f) for f in listdir(data_path) if (isfile(join(data_path, f)) and ("XGBpre.mat" in f))]

    #sets up file for data_output if not in current directory
    if not os.path.exists("data_output.csv"):
            output_file = open("data_output.csv",'a',newline='')
            csv_output = csv.writer(output_file)
            csv_output.writerow(["File Handle","Num Features","Train Size","Test Size", "Time", "PR2"])

    else:
        output_file = open("data_output.csv",'a',newline='')
        csv_output = csv.writer(output_file)


    for fh in onlyfiles:
        print(fh)

        m1_imported = scipy.io.loadmat(fh)
        m2_imported = scipy.io.loadmat(fh)
        num_features = 2

        #pull data into pandas
        train = pd.DataFrame()
        test = pd.DataFrame()

        input_arr = []
        prefix = 'Xr'

        for i in range(num_features):
            tag = prefix + str(i)
            input_arr.append(tag)
            train[tag] = m1_imported[prefix][i]
            test[tag] = m2_imported[prefix+'_test'][i]

        train.head()
        test.head()

        train_size = len(train)
        test_size = len(test)

        #train_size = 1000
        #test_size = 1000

        Models = dict()
        method = 'xgb_run'

        Yrate = m2_imported['yp'][0:test_size]

        Xr = train[input_arr].values[0:train_size]
        Yr = m1_imported['Yr'][0:train_size]
        Xt = test[input_arr].values[0:test_size]
        Yt = m2_imported['Yr_test'][0:test_size]


        start = time.perf_counter()
        Yt_hat, gen_model = eval('xgb_run')(Xr, Yr, Xt)
        #print("Length: {}".format(len(Yt_hat)))
        #print("Sample: {}".format(Yt_hat[0]))

        pR2 = binomial_R2(Yt, Yt_hat, np.mean(Yr), Yrate)
        #print('Yt_hat: {}'.format(Yt_hat[0:200]))
        print('pR2: {}'.format(pR2))

        Models[method] = dict()
        Models[method]['Yt_hat'] = Yt_hat
        Models[method]['PR2'] = pR2
        timer = (time.perf_counter()-start)

        #this generates our feature importance models and saves them to "save path" defined above
        """
        plt.subplot(xgb.plot_importance(gen_model))
        a = plt.gca()
        simpleaxis(a)
        output_img = os.path.split(fh)[1].split(".")[0]+"_" + str(num_features) + ".png"
        plt.savefig(join(save_path,output_img), bbox_inches="tight")

        """

        #if you have graph viz, this part will output a graph
        """
        #for i in range(num_features):
        plt.subplot(xgb.plot_tree(gen_model))#, num_trees=i))
        b = plt.gca()
        simpleaxis(b)
        plt.show()
        """

        csv_output.writerow([fh, num_features, train_size, test_size, timer, np.mean(pR2)])

    #closes file when done
    output_file.close()

        #scripting!!


        #check random state



main()
