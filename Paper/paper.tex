\documentclass{article}
\usepackage{amsmath}
\usepackage[margin=1in]{geometry}
\usepackage{graphicx}
\begin{document}

\def\vs{\vspace{.1in}}
\newcommand\tab[1][.5cm]{\hspace*{#1}}

\title{Mathematical Exploration of Machine Learning Models and Techniques}
\author{Joseph Koblitz}
\date{\today}
\maketitle

\begin{abstract}
  Throughout this year I have been exploring how we can use math, and a bit of code, to enhance our understanding of the brain and it's functionality. This paper will summarize my findings about mathematics applications in classical computational neuroscience, as well as machine learning's potential to predict neural spikes. In the process I will cover both mathematical topics, such as GLMs, and machine learning topics regarding supervised vs unsupervised learning, and hyperparameter optimization, among other things.
\end{abstract}

%\section{Background}
 % Machine Learning is one of those trendy topics in the news and Computer Science, but even a fairly shallow dive into the topic will reveal it could more accurately be described as applied probability and statistics. At its core, machine learning uses the computational power of computers to leverage probabilities and statistics to make predictions based on a given set of data. In general, machine learning could be described as a non-parametric statistical model as it either doesn't have a distribution, or doesn't have specified parameters before evaluating the data. In machine learning, there are two subclasses of algorithms: supervised and unsupervised. In general this just tells us if our data is ``labeled'' or ``unlabeled''.
 % Parameters can be thought of in the regular statistical sense (a probability distribution takes in parameters), whereas machine learning ``hyperparameters'' are things that need to be specified before the learning process begins. We are almost guaranteed to need to tune our hyperparameters to get an optimal model. Finally, ensemble learning allows us to ``mash'' together different models (of the same type) to get more accurate predictions from the combination of multiple learning processes.

\section{The Biology}
It is folly to presume we can model something we don't understand the basic mechanisms of. For this reason, I will begin my paper by discussing a few key biological concepts, though it is a poor substitute for any amount of focused biological study. Nonetheless, I will primarily discuss the neuron, which is the primary mechanism of ``neuroscience,'' and give a hint of the scale at which our brain operates. Hopefully this will give the reader a sense as to why this remains a difficult problem even with advances in all the sciences and computational power.
\subsection{The Brain}
  The current literature suggests our brain has 12 cranial nerves, each a collection of neurons and other supporting cells. Each cranial nerve is responsible for some important aspect of our lives; a few examples of important cranial nerves are the olfactory nerve, optic nerve, and vestibulocochlear nerve. These are responsible for our sense of smell, sight, and hearing, respectively. The brain as a whole is estimated to have approximately 86 billion neurons\cite{10.3389/neuro.09.031.2009}, and for any of these senses to work, we need these neurons to be working together. Let's dig further into what a neuron is and how it operates.

  \subsection{Neurons}
  Neurons can be catagorized in many ways, but most importantly for this paper, we can say neurons fall, broadly, into two categories. A neuron can either be afferent or efferent, which are our ``sensory'' and ``motor'' neurons. For any of the previously mentioned cranial nerves to work, we need both motor and sensory neurons to be functioning and communicating; we can't smell the roses without breathing in. So, how do these neurons communicate?

  Neurons communicate with each other through electro-chemical signals at a junction called a synapse. Ignoring the structure, the process has a few noteworthy points. First, a neuron receives the electro-chemical signals at its synapses. It is worth noting that a neuron can have up to 7000 synaptic connections\cite{Drachman2004}, and in total our brain has somewhere around 100 trillion connections. So, as a neuron takes in and processes all these signals, which we will refer to as a ``stimulus,'' and then responds to the stimulus. In effect the neuron decides whether or not it will release chemical signals in response to the stimulus it has received from other neurons. This response is thought to be binary in that it either releases its own signals completely or not at all. This is called the ``all-or-none'' principle.

  So, once we understand the basic mechanics, we need data to model. This data often comes from having an animal do a simple, repetitive task, while strapped to electrodes which monitor the neuronal responses to the action. For instance, one might play audio tracks for an owl and take readings from the vestibulocochlear nerve, or have a macaque do a reaching task and take readings from its arm. Once we have collected substantial amounts of data under a variety of conditions, we can begin modeling. Recall that the goal of modeling a neuron is to provide a model that accurately predicts the output response of a neuron upon given the same stimulus.

  \section{Classical Analysis}
  Before we get into classical analysis, we should be familiar with two broad classes of models, parametric and non-parametric models.
  \subsection{Parameteric Models}
    A parametric model already has set parameters that you can plug data into to get an output. As an example: %\cite{meh}

    \begin{center}``Let $x$ be the output of the excitatory neuron and $y$ be the output of the inhibitory neuron:
    $$\dot{x} = -\frac{1}{\tau}x + \tanh(\lambda x) - \tanh(\lambda y)$$
    $$\dot{y} = -\frac{1}{\tau}y + \tanh(\lambda x) + \tanh(\lambda y)$$
    where $\tau > 0$ is a characteristic time constant and $\lambda > 0$ is an amplitude gain.''
    \end{center}

    It's clear to get the output of these neurons we have a few variables. We have our outputs $x$ and $y$, as well as specified constants $\tau$ and $\lambda$ that we plug in to get our prediction for the rate of output of the neurons.

    Since we already have a fixed form for out model, we call this parametric. It's structure makes it a lot easier to work with, but often times when we fix a model to only consider certain features, we reduce our accuracy.

\subsection{Non-Parametric Models}
  A non-parametric model would be given data and derive the model structure and parameters from the data itself. The rest of the paper will be discussing non-parametric models.
  
  In classical analysis, the primary model used is a GLM, or generalized linear model. The GLM will be discussed shortly, but is part of the non-parametric class of models.
 
\subsection{Linear-Nonlinear (LN) Models}
  A common neuroinformatic model is the Linear-Nonlinear-Poisson model. The model is composed of three main steps. First, we estimate linear filters. Next, we pick a nonlinear function that maps our filter output to an output firing rate. Finally, we use the firing rate to create a poisson model of our neural spikes. Each one of these steps has an incredible amount of mathematical depth to them, but I hope to touch on the key ideas.

  The linear filtering step accomplishes two things. The first is a dimensionality reduction, which is important because of the size of a stimulus data set. If we measure 30 stimulus features for 1 million time points, we have a 20 million dimension stimulus. To try and optimize over data of that scale is virtually impossible by hand and computationally wasteful. By using linear filtering, we shrink the dataset we'll be analyzing. So how exactly is that accomplished?

  We filter our data to the important features. Not every feature at every time point is important, so we begin by analyzing where the spikes are, and what events preceded each of those spikes. We can then analyze what is happening with our features that preceded the spikes. By doing this we can isolate important features into ``feature vectors,'' and we should have isolated the number of feature vectors down to a much smaller number than our original collection of features. When we project the stimulus onto these feature vectors at each time point, we get values that contribute to the probability of spiking at each time point.

  In the image below we can see that an elevated sound level less than $100ms$ gives a collection of data that deviates from the mean. This is a good indicator that increased volume is an important feature for an auditory nerve.
  \begin{figure}[ht!]
    \centering
    \includegraphics[width=0.5\textwidth]{images/STA_filter.png}
    \caption{STA Filter Example\cite{tutorial}}
  \end{figure}

  We can take the outputs from this filtering process and use an invertible non-linear function, or transformation, to map our outputs to an expected firing rate. This can be calculated by bayes rule. If we let $\eta_i$ be our linear predictor for our $i^th$ filter, then
  $$g(\eta_i)=p(spike|\eta_i)=\frac{p(\eta_i|spike)p(spike)}{p(\eta_i)}$$ 
  Since all the values on the right hand side can be calculated from data, we can find what non-linear transformation best maps our data an expected firing rate.

  Finally, we can map this firing rate to a probabalistic Poisson model to predict neuron spikes for different stimulus with similar stimulus inputs.
    
\subsection{Generalized Linear Models}
  So, after that long explanation, what is a GLM\cite{GLMBook}?

  GLMs are a type of linear-nonlinear model where we fix the form of our nonlinearity. It can also be described as a generalized form of linear regression. With ordinary linear regression, we work with normal distributions, but with a GLM, we open ourselves to the possiblity of other random variables.

  \begin{itemize}
      \item A distribution model from the exponential family
      \item A linear predictor $\boldsymbol{\eta}=\boldsymbol{X\beta}$
      \item An invertible link function (the inverse of our nonlinearity), $g^{-1}$, such that $\boldsymbol{\mu} = g^{-1}(\boldsymbol{\eta})$
  \end{itemize}

  I'll take this time to explain a few terms. A distribution model from the exponential family is a model such as Bernoulli, Binomial, and Poisson, among others. This is what is meant when we say we open ourselves to other random variables.

  Our linear predictor which is a linear combination of our observed (or independent) variables with unknown parameters (weights).

  A link function which relates the linear model to the response (or dependent) variable such as a log-link function (taking the logarithm of both sides). In general there are commonly used link functions for each distribution type. Often these are decided upon by trying to match the range of the predictors to the response variable. For instance, the Poisson model is non-negative, so we use a log-link function cannonically.

  I mentioned that the parameters, our weights, are unknown. It's hard to use a model where you're missing a vector of variables. Let's investigate one method for that called ``Least Squares Estimation.''

\subsection{Least Squares Estimation}
When finding our parameters, $\beta$, we could pick random values, but that wouldn't make for a very accurate model (in general). How might we \textbf{optimize} our model?

\textbf{Least Squares Estimation:}
Define a cost function 
$$C = \sum_{i=1}^{n}(y_i-\hat{y}_i)^2 \text{, where } \hat{y}_i = \sum_{j = 0}^{m} \beta_j x_{ij}$$
$$\frac{\partial C}{\partial \beta_k} = -2 \sum_{i=1}^{n} \left(y_i- \sum_{j=0}^{m} \beta_j x_{ij} \right) x_{ik}$$
\begin{center}
Setting our derivative equal to zero gives:
\end{center}
\begin{align*}
  0 &= -2 \sum_{i=1}^{n} \left(y_i - \sum_{j=0}^m \beta_j x_{ij} \right) x_{ik} \\
  \sum_{i=1}^n \sum_{j=0}^m \beta_j x_{ij} x_{ik} &= \sum_{i=1}^n x_{ik} y_i \\
  \sum_{j=0}^m \left( \sum_{i=1}^n x_{ij}x_{ik} \right) \beta_j &= \sum_{i=1}^n x_{ik} y_i \\
\end{align*}
\begin{center}
In matrix form this becomes:
\end{center}
\begin{align*}
  \boldsymbol{X^TX\beta} &= \boldsymbol{X^Ty}\\
  \beta &= \boldsymbol{(X^TX)^{-1} (X^Ty)}
\end{align*}

This will work as long as $\boldsymbol{X^TX}$ is invertible. Least squares is not the only optimization algorithm. Another common one is MLE, which I will not be covering in this paper.

\newpage

\subsection{Spike Train History}
To improve the GLM further, we can use \textbf{Spike Train History}
Let $r(t)$ be the response of the neuron and $\boldsymbol{s(t)}$ be our stimulus at time $t$, while $g(\cdot)$ is our fixed nonlinear function\cite{ALJADEFF2016221}.

\begin{equation*}
  r(t)=g(r(t'),\boldsymbol{s(t)}) \text{ where } t' < t
\end{equation*}

Defining a parameter $r(t')$, a previous response, is an important part of the model from a biological standpoint. In the real world, a neuron can be more or less likely to fire based on if it has fired in its recent history. Therefore, we include this parameter to model this biological aspect of a neuron. We can see a full diagram of the procedure below.
\begin{figure}[ht!]
  \centering
  \includegraphics[width=.5\textwidth]{images/SpikeTrainHistory.png}
  \caption{Diagram of Procedure\cite{ALJADEFF2016221}}
\end{figure}

\section{Machine Learning}

Machine learning can broadly be classified into two categories, unsupervised machine learning and supervised machine learning. I'll take a moment to define both and provide a short example to illustrate the main concepts.

\subsection{Unsupervised Machine Learning}
Unsupervised Machine Learning algorithms are concerned with finding the ``structure'' of data. There is no sense of independent variables that combined in some way produce a value for a dependent variable; rather, we have a ton of independent variables and we want to find a pattern or perhaps cluster them in some way. We can use unsupervised learning approaches to solve problems such as clustering problems, and anomaly detection. The general idea of clustering is we want to groups of data points that are nearby in the respective hyperspace, and anomaly detection concerns itself with finding those data points that sit just outside a group or some groups of data points.

As a more concrete example, let's say we work for Nike's fashion team, as a very profitable, large corporation, we would like to produce a new line of T-shirts. We have some data (Figure 3) about the heights and weights of our target audience. With this data, we can create our sizes into, say, three roughly equal sized groups. By doing this, we can try and reduce the number of products we need to produce, at the cost of the size needing to fit more people. Alternatively, we could split into 5 different groups where getting the proper size is a little more effort to figure out, but fits a more specific combination of height and weight ranges (Figure 4).

\begin{figure}[ht!]
  \centering
  \begin{minipage}[b]{0.40\textwidth}
      \centering
      \includegraphics[width=0.9\textwidth]{images/unsortedTShirts.jpg}
      \caption{Before K-Means}
  \end{minipage}\hfill
  \begin{minipage}[b]{0.55\textwidth}
      \centering
      \includegraphics[width=0.9\textwidth]{images/sortedTShirts.jpg}
      \caption{K-Means with $K = 3,5$}
  \end{minipage}
\end{figure}

\newpage
This can be achieved via a k-means algorithm. $k$-means can be defined as such: ``$k$-means clustering aims to partition $n$ observations into $k$ clusters in which each observation belongs to the cluster with the nearest mean, serving as a prototype of the cluster.'' This is just one example of an unsupervised learning algorithm, but our data has a dependent variable, our response. Supervised learning is what will allow us to learn from ``labeled'' data.

\subsection{Supervised Machine Learning}
  With supervised learning algorithms, our goal is to make accurate predictions of the dependent variable based on past data. Some common examples of this are predicting the cost of a house based on variables such as number of rooms, square footage, number of houses nearby, etc... We can solve virtually any supervised learning problem if we have enough relevant data.

  This means if we can gather enough features and timepoints of our stimulus with the corresponding responses, we can turn our neural modeling problem into a supervised machine learning problem. We now have another method to model our neural data, so how does modern ML stack up against GLMs? There are a few things worth noting here. First, machine learning is less ``transparent'' than GLMs.

  Generalized Linear Models have a linear combination of variables with weighted parameters. We have algorithms to help us come up with accurate models, but the linear combination of weights and variables allow us to gather intuition on what the neuron may be doing biologically. There is a degree of intentionality and readability in generalize linear models that makes them more ``transparent'' than a machine learning model.

  This transparency does come at a cost; machine learning makes less assumptions about our data which gives it the possibility to increase accuracy. Most notably, GLMs rely on a linearity assumption about the way that stimulus features will combine, which biologically is the cell's interpretation of a stimuli. Since machine learing doesn't rely on any linearity assumption, it allows for arbitrary non-linear interactions between stimulus features, which potentially allows for more accurate models.

  It is worth noting that to implement the most basic machine learning algorithms, using python, it can take as little as 3-4 lines of code. There is mathematical understanding needed to work with GLMs, where as machine learning allows for us create and test models with relative ease to answer questions about neural data. With code becoming more easy to write, many beginners may find machine learning an optimal way to start working with neural data.

  Lastly, through hyperparameters, we can optimize ML algorithms easily to vary our model to emphasize different aspects and get more accurate predictions.

\newpage
\subsubsection{Decision Tree Example}
  I will be using decision trees as a machine learning algorithm to describe how hyperparameters might change a machine learning algorithm. A decision tree is a supervised machine learning algorithm which makes decisions based on a criterion at each node of a tree until it reaches a leaf node at which we are given our predicition. It can be used for either classification or regression but for clarity and simplicity I will use a classification tree.
  \begin{figure}[ht!]
    \centering
    \includegraphics[width=0.75\textwidth]{images/DecisionTreeExample.png}
    \caption{Sci-Kit Learn's Decision Tree Example}
  \end{figure}
  The above example of a decision tree is pretty simple to decompose; decision trees are nice starting algorithms because they mimic how the average person might go about making a decision. On a set of 14 data points we can see a pretty clear divide between a few features.

  The parameters for this model can be seen by how many splits the tree makes and what the criteria for each split are; so how might hyperparameters influence our decision tree?
  
  \subsubsection{Hyperparameters}
    A hyperparameter can be defined as follows: ``In machine learning, a hyperparameter is a parameter whose value is set before the learning process begins. By contrast, the values of other parameters are derived via training.'' Below we can see all 13 hyperparameters that sci-kit learn will implement (Figure 6). These features are more or less self explanatory, but as two examples, I'll describe the splitting criterion and maximum depth.

    \begin{figure}[ht!]
      \centering
      \includegraphics[width=\textwidth]{images/DecisionTreeHP.png}
      \caption{Sci-Kit Learn's Function for a Decision Tree\cite{SciKit}}
    \end{figure}
    
    There are different ways to characterize the ``best'' ways to split, two of these are information gain and gini impurity. The gini impurity criterion measures, in essence, the probability of misclassifying an item if you were to classify it randomly by the probability of falling in each class. Information gain measures a reduction in entropy which can be formally defined and explored but is outside the scope of this paper.

    Thus, depending if we want a reduction of entropy or a reduction in the random misclassification we might choose one or the other. Alternatively, there's a tradeoff in machine learning which is bias versus variance. This tradeoff describes the difference between fitting one specific data set very closely and being too general to get any valuable information out of a model.

    This can be seen using maximum depth. If we pretend this dataset came entirely from me, we can see that I'm not a fan of playing on sunny, humid days. Pretend for an instance that we try to model a student who is exactly the same except he or she enjoys sunny, humid days. We will predict poorly on sunny days because the model is ``over-fit'' to my data. We can fix this by increasing the variance; one method for doing this is reducing the maximum depth. By reducing the maximum depth to two we can keep the same 2-3 ratio for sunny days, without predicting poorly based on the humidity data. We trade off by making predictions on specifically me less accurate or predictions on rainy days less accurate.

    So how do we find optimal parameters?

  \subsubsection{Optimizing Hyperparameters}
    There are a few key algorithms I'll go over for optimizing hyperparameters.
    \begin{enumerate}
      \item{Grid Search}
      \begin{itemize}
        \item {Grid search is an exhaustive search of all valid hyperparameters; that is, a grid seach tests every possible combination of valid hyperparameters. If we had only two options for each hyperparameter in our decision tree, we would still need to test $2^{13}$ hyperparameters to pick an optimal combination of all 13 hyperparameters. Since this is computationally inefficient, though it does guarantee the best hyperparameters for analogous problems, it is often not the best choice for optimizing hyper parameters.}
      \end{itemize}
      \item{Random Search}
      \begin{itemize}
        \item {There is also random search which essentially relies on the idea that with a large enough sample size, again over all valid parameters, we will be able to randomly pick a combination of hyperparameters that is close to optimal. Since this does not guarantee that we will get the best hyperparameters, this algorithm may not be the best choice if we need to be sure that we get as optimal hyperparameters as possible.}
      \end{itemize}
      \item{Gradient Descent}
      \begin{itemize}
        \item {Finally, there is gradient descent which relies on the assumption that we can view our optimal hyperparameters as the minimum of a convex function that we can take incremental steps on, using the gradient, to eventually reach an optimum value. Note that with this algorithm we are not necessarily guaranteed a global optimum if our convex function has local optima.}
      \end{itemize}
    \end{enumerate} 

\section{Comparison}
  \begin{figure}[ht!]
    \centering
    \begin{minipage}[b]{0.33\textwidth}
        \centering
        \includegraphics[width=0.5\textwidth]{images/TypicalData.png}
        \caption{Fairly Typical pR2 Plot\cite{Benjamin111450}}
    \end{minipage}\hfill
    \begin{minipage}[b]{0.33\textwidth}
        \centering
        \includegraphics[width=0.5\textwidth]{images/GLMData.png}
        \caption{GLM Outperforms NN\cite{Benjamin111450}}
    \end{minipage}
    \begin{minipage}[b]{0.33\textwidth}
      \centering
      \includegraphics[width=0.5\textwidth]{images/sparseMean.png}
      \caption{Sparse Dataset\cite{Benjamin111450}}
  \end{minipage}
  \end{figure}
  I've included 1 typical plot of the mean psuedo-R2 values and 2 unusual cases to illustrate a few points. First, to note 
  
  In figure 7, we see a fairly typical spread of pR2-Values, and this one serves to show that in general ML can offer substantial improvements on GLMs with much less mathematical understanding of the underlying processes. When run on our auditory data, we got roughly analogous results to this plot.

  Figure 8 serves to show that the GLM can outperform the Neural Net in certain cases. This is important to note because it means that something about the data or model could be more accurately captured under the assumptions of the GLM. This gives insight into how these neurons \emph{might} be processing the data slightly differently.

  Lastly, figure 9 serves to show that machine learning algorithms can have fairly substantial gaps between efficacy depending on the data. In this case, we had a sparse dataset, which the author theorized meant GLMs and Neural Nets perform less effectively on the data. However, the XGBoost and Ensemble algorithms continued to perform well. Another case where it might be interesting to see what the modeling would imply about the neurons' interpretation of the stimulus.

  From our datasets, we focused mostly on XGB predictions, and we saw pR2 scores as high as $\approx.68$ in some cases! The neuron in this case may be worth special attention since we get abnormally high pR2 scores.

\section{Conclusion}
  Based on my research, I feel that there is a combined approach of techniques that may serve as an integrated ``pipeline'' to work with computational neuroscience data.

  I feel that it is best to run a variety of ML algorithms on the neurons, at first, because the code is easy to write and will give us a good sense as to whether or not the data can be leveraged by our current algorithms to provide accurate predictions.

  Once we have discovered the ``utility'' of our datasets, I feel it best to try applying hyperparameters to see how accurate we can get under optimal conditions. This gives some sense as to what I will refer to as the ``potential'' of our models. 

  Once we have a sense as to how accurately we can predict the output of this neuron, I feel it may be worthwhile to go back and try to use the more transparent GLM to try and build more biological insight on how the stimulus may be processed.

\section{Extras}

  \subsection{Challenges}
    Computational Neuroscience is a large field of research with plenty of challenges to consider. 
  
    \begin{enumerate}
      \item{Scale of the brain}
      \begin{itemize}
        \item{The brain operates at a scale that is technologically unattainable in the case of the average researcher or hobbyist at this time, and even if we could operate at this scale, we would likely fail miserably to understand the resulting product due to our lack of understanding of individual neurons at this time.}
      \end{itemize}
      \item{Variety in types of neurons}
      \begin{itemize}
        \item{There is also the variety in neuron types and how they may process stimuli differently. There is not just one type of ``vestibulocochlear neuron'', and there are many different areas of the brain and body that have different functions they help to fulfill. This adds to the complexity of analyzing the different types of neurons.}
      \end{itemize}
      \item{Open-data challenges}
      \begin{itemize}
        \item{Modeling relies largely on a having a lot of detailed, accurate data. This is hard to come by since all experiments involving animals are not only expensive, but need to be approved by a board. Due to factors like this, among others, the computational neuroscience community has been very closed door with their data, making it hard to work with computational neuroscience unless one has access to studies that can gather data like this.}
      \end{itemize}
      \item{New technologies to learn}
      \begin{itemize}
        \item{Lastly, as we enter a very computer-centric era; people are independently developing tools and algorithms at a speed that is impossible to keep up with if a person were to try and leverage every tool they could find. This makes it important to try and find a few tools that we can get very effective at using instead of needing to spend our time learing a new tool each week.}
        \item{For this project, I leveraged Sci-Kit learn\cite{SciKit}, KordingLab's spykesML code\cite{Benjamin111450}, and MatLab}
      \end{itemize}
    \end{enumerate}

    \bibliographystyle{abbrv}
    \bibliography{neuronNum}

\end{document}