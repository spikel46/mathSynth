clear all;
%% Load the .mat file
%%
load('1.2.3.abi.mat')

%call the file data so we don't need to change stuff below for a new file
data=a1u2f3;
%% Parameters
%%
SR=48077; %Sampling rate
Ts=1/SR*1000;

%Number of trials to use in the fit; use 1 to keep all trials (works OK
%with 50 trials to test it out)
Nt=100;

%Number of time points to keep in filter
nLags=200;

%Number of basis functions
Nb=25;
%% Change directory so that the ear.cal file is in working directory
%%
%cd AN_OneNeuron/
%cd /Users/fischer9/Dropbox/MATLAB/OwlData/AN_OneNeuron/
%cd /Users/jacob_000/DropBox/AN_OneNeuron
%% Get ABI values. There may be more than one for each neuron
%%
value = BEDSgetStimParam(data,'abi');

A=unique(value);
%% Get all stimuli, spikes
%%
[stims,~,spikes]=getBC0stimsspikes(data);

figure(222); clf;
plot(stims)
title('Input Stimulus')
%% Get timing information, start, duration, total
%%
[ep,du,dl]=BEDSgetEpoch(data);
%% Get stimuli and spikes for one ABL value
%%
ST=stims(value==A(3),:);
SP=spikes(value==A(3),:);

if Nt==1
    Nt=size(ST,1);
end
%% Time index for stimulus
%%
%Initial stimulus segment to discard
in1=481+1;
%Last stimulus part to keep
in2=floor((dl+du)/Ts);
%Time to use
tBINS=(in1:in2)*Ts;
N=length(tBINS);
%Indicies of times to use
tINDEX=in1:in2;
%% Get the spiking response into a vector of ones and zeros
%%
R=zeros(N,Nt);

for n=1:Nt
    R(:,n)=histc(SP{n},tBINS);
end

Robs=R(:);

L=length(Robs);
%% Get the stimulus portions preceeding each time point
%%
Xstim=zeros(L,nLags);

cnt=0;
for n=1:Nt
    for m=1:N
        cnt=cnt+1;
        
        Xstim(cnt,:)=ST(n,tINDEX(m)-nLags+1:tINDEX(m));
    end
end
%% Get basis functions for representing filter
%%
t=(0:size(Xstim,2)-1)*Ts;
t=t/max(t);

Phi=zeros(length(t),Nb);

for n=1:Nb
    Phi(:,n)=sin(pi*(n+25)*(2*t-t.^2));    %You need to choose where to start (25 here)
end

figure(1);clf;plot(t,Phi)

%Orthonormalize
Phi=gramschmidt(Phi);
%% Represent stimulus in basis functions (reduces the dimension)
%%
X=Xstim*Phi;
%% Fit the GLM
%%
[b,dev,stats]=glmfit(X,Robs, 'binomial', 'link','probit');

%[b,dev,stats]=glmfit(X,Robs, 'binomial', 'link','logit');

%[b,dev,stats]=glmfit(X,Robs, 'normal','link','identity');
%% Compute model filter from parameters, basis functions
%%
f=Phi*b(2:end);   %First element is a constant bias
%% Compute STA
%%
STA=Robs.'*Xstim;
%% Compare filter to STA
%%
figure(2);clf;
plot(t,STA/max(abs(STA)),'r',t,f/max(abs(f)),'b')
xlabel('Time','fontsize',15)
ylabel('Normalized amplitude','fontsize',15)
set(gca,'fontsize',12,'box','off')

legend('STA','GLM')