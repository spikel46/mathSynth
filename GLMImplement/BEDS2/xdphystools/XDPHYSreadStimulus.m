function [waveform, chan_ids] = XDPHYSreadStimulus(filefid,params,analog)

% XDPHYSreadStimulus   read stimulus data section of
%                    xdphys files
%
%           THIS IS A PRIVATE M-FILE!
%
% This function is a NOOP if analog == 0
%
%
% See:  readXDPHYSfile

% Written by cmalek, Aug 2002
% RCS: $Id: XDPHYSreadStimulus.m,v 1.4 2003/10/15 22:03:24 bjorn Exp $

%The *only* reason this is a separate file is for
%legibility. 

waveform = {};
chan_ids = '?';

if (analog)
  % the first thing we have to do is determine whether we can expect 
  % saved stimuli in this file.  If file.version < 4, or if file.version
  % is undefined (indicating an xdphys file pre version 2, or a dowl file) 
  % there are no saved stimuli.

  [file_version,success]=hashFind(params,'file.version','num');
  if (success & (file_version >= 4))
    fid=filefid;
  
    daFc = hashFind(params,'daFc','num');
    epoch = hashFind(params,'epoch','num') / 1000;
    stimdec = hashFind(params,'stim.decimate','num');
    nsamps = (daFc * epoch) / stimdec;
  
    % There are zero, one or two stimuli saved, depending on whether
    % (a) stimuli were saved for this record and (b) it was a dichotic 
    % or monaural stimulus.  Channel 1 is the left channel and channel 2 
    % is the right -- but we'll rewrite the channel ID to
    % be useful.
    done_stimulus=0;
    i=1;
    while (~done_stimulus)
      % Get the first five characters (so we can check if
      % there actually is a stimulus) and then rewind
      % back to the first position (either because there
      % was no data or to simplify the loop).
      position = ftell(fid);
      tmp=char(fread(fid,21,'char')');
      fseek(fid,position,'bof');
      if (~isempty(findstr(tmp,'depvar')) | ~ ...
		  isempty(findstr(tmp,'END_R'))) | isempty(tmp)
        % There are no further stimuli associated with this one 
        % so we're done.
      	done_stimulus=1;
      else 
		tmp = skipTo(fid,'STIMULUS');
		tmp = skipTo(fid,'channel=');
		chan_id = sscanf(tmp,'channel=%d',1);
		if chan_id==1
		  chan_ids(i)='L';
		elseif chan_id==2
		  chan_ids(i)='R';
		else
		  chan_ids(i)='?';
		end
		% Read the waveform      
		waveform{i}=XDPHYSreadWaveform(fid,nsamps);
      end
      i=i+1;
    end
  end
end
