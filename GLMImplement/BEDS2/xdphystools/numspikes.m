function [depvar,smean,sstd,nspikes] = numspikes(filename,timewin)
% numspikes              Return the number of spikes in an XDPHYS file
%
% [D,M,S,N] = NUMSPIKES(FILENAME) will retrieve all
% spikes during the stimulus period in the XDPHYS file
% FILENAME.  D will be a vector of the depvars in the
% file, while N will be a (#depvar x iteration) matrix,
% where the (i,j) entry will be the number of spikes for
% iteration j of the depvar listed in D(i).  M is a
% vector of the mean number of spikes, and S is the
% standard deviations of the same, organized in the same
% order as D.
%
% [...] = NUMSPIKES(FILENAME,TIME) will do the same, but
% allows you to specify the time over which spikes will
% be counted.  TIME can either be a single number, in
% which case it will be interpreted as the window length
% starting from the beginning of the stimulus, or it can
% be a vector of the form [starttime endtime].  The times
% are specified in milliseconds.
%
% [...] = NUMSPIKES(DATA,...) will do the same, but
% allows you to specify the BEDS data structure DATA,
% rather than a filename.
%
% This program is superseded by bsPlotDepvar.
%
% See also:  BEDS, bsPlotDepvar.

% Written by GBC, 25 Apr 2001.
% RCS: $Id: numspikes.m,v 1.2 2001/05/22 18:39:14 bjorn Exp $
% Depends on bedstools/isbeds.m
%            bedstools/BEDSgetSpikes.m
%            xdphystools/readXDPHYSfile.m
%            hashtable/hashFind.m

% Get the data
if ~isBEDS(filename);
  data = readXDPHYSfile(filename,0);
else
  data = filename;
end

% Do some sanity checks on timewin, plus set the default.
epoch = hashFind(data.params,'epoch','num');
duration = hashFind(data.params,'dur','num');
delay = hashFind(data.params,'delay','num');
if nargin == 1
  timewin = [delay delay+duration];
else
  if (length(timewin) == 1)
    if ((delay + timewin) > epoch)
      error('Specified duration exceeds epoch')
    else
      timewin = [delay delay+timewin];
    end
  elseif ((length(timewin)==2)&(min(size(timewin))==1))
    if timewin(1) < 0
      error(['Specified start of timewindow must be' ...
	     ' non-negative.'])
    elseif ((timewin(1)+timewin(2)) > epoch)
      error('Specified timewindow exceeds epoch');
    end
  else
    error('TIME must be [starttime endtime]');
  end
end

% Get the number of spikes.  This is a bit cludgy, but hey.
iters = hashFind(data.params,'reps','num');
nspikes = [];
depvar = [];
counts = 0;
for n = 1:length(data.trial)
  if isempty(depvar)
    ind = [];
  else
    ind = find(depvar == data.trial(n).depvar);
  end
  if isempty(ind)
    ind = length(depvar) + 1;
    depvar(ind) = data.trial(n).depvar;
    nspikes(ind,:) = zeros(1,iters);
    counts(ind)=0;
  end
  spikes = BEDSgetSpikes(data,n);
  vspikes = find((timewin(1) < spikes{:}) & (timewin(2) > spikes{:}));
  counts(ind) = counts(ind)+1;
  nspikes(ind,counts(ind)) = length(vspikes);
end

smean = mean(nspikes');
sstd = std(nspikes');