function [spikebuf,toms] = XDPHYSreadRaster(fid,params,analog)

% XDPHYSreadRaster    read discrete event data in xdphys
%                     files
%
%           THIS IS A PRIVATE M-FILE!
%
% See:  readXDPHYSfile

% Written by GBC, 19 Mar 2001.
% RCS:  $Id: XDPHYSreadRaster.m,v 1.7 2003/01/24 08:15:21 bjorn Exp $

%The *only* reason this is a seperate file is for
%legibility. 

junk = fgetl(fid);
progname = hashFind(params,'bedsProg');
minor_version = hashFind(params,'bedsMin');
fc = hashFind(params,'evtfc');

% Read in the discrete event counting data.
% First case is something to do with spike sorting.
if (strncmp(junk,'avepmk=',7))
  error(['Spike sorting shouldn''t work, so who are you' ...
	 ' kidding?']);
  
% Next case is normal spikes.  
elseif (strncmp(junk,'nevents=',8))
  nspikes = sscanf(junk,'nevents=%d');
  spikebuf = fscanf(fid,'%d %d\n',[2,nspikes]);
			    
  % If you use the software spike discriminator, then spikes
  % over threshold but outside the windows are saved.  We
  % discard those.
  spikebuf = spikebuf(1,find(spikebuf(2,:)~=-2));
  
  if (((minor_version < 45) | (minor_version == 51)) &  ...
      (strncmp(progname,'dowl',4)))
    toms = 1/fc*1e3;
  else
    toms = 1/10000;
    
  end
end 
