function convertXDPHYSGZfiles(glob,compress,analog)

% convertXDPHYSGZfiles    Convert xdphys files to MAT
%                         files with support for compresison.
%
% CONVERTXDPHYSGZFILES(GLOB) will convert all xdphys files
% matching GLOB (as if you were using ls) into mat-files,
% saving them with file extension .mat.
%
% CONVERTXDPHYSGZFILE(GLOB,ANALOG) will do the same, but if
% ANALOG is 0, will not process any analog data in the
% files.  By default, ANALOG is 1 (process analog data).
%
% CONVERTXDPHYSZFILES(GLOB,COMPRESS) will do the same
% (setting ANALOG to 1), but will compress the resulting
% mat file.  The valid options for COMPRESS are 'gzip',
% 'bzip2', and 'none'.  The default option for COMPRESS
% is 'none'.  Compression will only work on Unix systems.
%
% CONVERTXDPHYSGZFILES(GLOB,COMPRESS,ANALOG) allows you to
% specify all three options.
%
% convertXDHYSGZfiles gives a unique name to the contents
% of the matfile.  To reassign the name upon loading,
% use BEDSload.
%
% See also:  BEDS, readXDPHYSfile, BEDSload

% Written by GBC, 21 Mar 2001.
% RCS: $Id: convertXDPHYSGZfiles.m,v 1.1 2005/01/15 00:46:49 bjorn Exp $
% Depends on:  xdphystools/readXDPHYSfile.m
%              xdphystools/XDPHYSprocessFilename.m
%              gzip
%              bzip2

% Sort out all the command line options.  By default,
% analog=1, compress='n'.
if nargin < 2
  compress = 'n';
  analog = 1;
elseif nargin == 2
  if ischar(compress)
    analog = 1;
  else
    analog = compress;
    compress = 'n';
  end
end

% Take a look at the value of compress.  We only care
% about the first character of the filename.  Make sure
% that it's one of the valid options, and that the
% appropriate compression program exists on the system.
if ischar(compress)
  compress = lower(compress);
  if ~isunix
    warning('No compression available on non-Unix systems.');
    compress = 'n';
  elseif strncmp(compress,'g',1)
    failed = unix('which gzip &> /dev/null');
    if failed
      error('Could not find gzip on system.');
    else
      compress = 'g';
    end
  elseif strncmp(compress,'b',1)
    failed = unix('which bzip2 &> /dev/null');
    if failed
      error('Could not find bzip2 on system');
    else
      compress = 'b';
    end
  elseif strncmp(compress,'n',1)
    compress = 'n';
  else
    error('Unsupported compression method.');
  end
else
  error(['COMPRESS must be a string containing a valid' ...
	 ' compression method.']);
end

% Get the files to be processed. Sort the list so that it
% gets done in the order that people think (in case of
% mistake, etc.)
filelist = dir(glob);
filenames = sort({filelist.name});
% Now go through the files.
for n = 1:length(filelist);
  if ~filelist(n).isdir
    filename = filenames{n};
    disp(filename);
    [animal,unit,filenum,extranum]=XDPHYSprocessFilename(filename,'str');
    
    dotsInFile = findstr(filename,'.');
    
    if ~isempty(findstr('gz',filename))
      newfile = sprintf('%s.mat',filename(1:dotsInFile(end)-1));
    else
      newfile = sprintf('%s.mat',filename);
    end
    
    %We name the data 'a###u%%f--, where ### is the animal
    %number, %% is the unit number, and -- is the file
    %number.  Additionaly, we may append a e!, where ! is
    %whatever extra number has been tacked on in a
    %four-number filename. 
    if (isempty(extranum))
      dataname = sprintf('a%su%sf%s',animal,unit,filenum);
    else
      dataname = sprintf('a%su%sf%se%s',animal,unit,filenum,extranum);
    end
    
    % Process the file using readXPDHYSfile.
    eval(sprintf('%s=readXDPHYSfile(''%s'', %d);',dataname,filename,analog));
    % Save the new file.
    eval(sprintf('save %s %s;',newfile,dataname));
    % Since these variables can take up a lot of space
    % (especially if we save the analog data) clear them
    % from memory as we go along.
    eval(sprintf('clear %s;',dataname));
			
    % Handle the compression if we're doing it.
    switch compress
     case 'g'
      failed = unix(sprintf('gzip %s',newfile));
      if failed
	error(sprintf('gzip compression failed on %s',newfile));
      end
     case 'b'
      failed = unix(sprintf('bzip2 %s',newfile));
      if failed
	error(sprintf('bzip2 compression failed on %s',newfile));
      end
    end
  end
end
