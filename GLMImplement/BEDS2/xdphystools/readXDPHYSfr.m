function calib=readXDPHYSfr(glob)
  
% readXDPHYSfr           Read in an XDPHYS .fr file.
%
% CAL=READXDPHYSFR(GLOB) will return the calibration
% data in all XDPHYS .fr files matching GLOB.  If GLOB
% matches only a single file, then CAL will be a matrix;
% otherwise, CAL will be a cell array of calibration
% matrices.
%
% The format of the returned calibration matrix is:
%
% Column 1: freq(Hz)
% Column 2: magnitude (dB)
% Column 3: phase (rad)
%
%  See also: readXDPHYSfile, readXDPHYScalib.
 
% Written by GBC, 29 Jul 2003.
% RCS: $Id: readXDPHYScalib.m,v 1.1 2003/07/23 20:21:27 bjorn Exp $
% Depends on:  misc/skipTo.m

%  The format of the XDPHYS fr matrix is:
%  
%  Column 1: freq (Hz)
%  Column 2: left_mag (dB)
%  Column 3: left_phase (rad)
%  Column 4: left_distortion (100*f2_mag/f1_mag)
%  Column 5: right_mag (dB)
%  Column 6: right_phase (rad)
%  Column 7: right_distortion (100*f2_mag/f1_mag)
%  Column 8: left_mag_stderr
%  Column 9: left_phase_stderr
%  Column 10: right_mag_stderr
%  Column 11: right_phase_stderr

  
if nargin<2
  fr=0;
end

% Get the files to be processed. Sort the list so that it
% gets done in the order that people think (in case of
% mistake, etc.)
filelist = dir(glob);
filenames = sort({filelist.name});

numcols=11;

% Return value is a little different depending on the
% number of files to process.
if isempty(filelist)
  error('No such file!')
elseif length(filelist)==1
  calib=processCalibFile(filenames{1},numcols);
else
  for n=1:length(filelist)
	disp(filenames{n})
	calib{n}=processCalibFile(filenames{n},numcols);
  end
end

% This does the work.
function data=processCalibFile(filename,numcols)
  fid=fopen(filename,'r');
  if (fid==-1)
	error(sprintf('Unable to open file: %s',filename));
  end
  
  % Skip over comments and go to parameters
  if isempty(skipTo(fid,'PARAMS'))
	error('Could not find PARAMS ; short data file?');
  end
  % Load up a hashtable with the parameters.
  params = XDPHYSreadParams(fid);
  refChannel=hashFind(params,'Reference_Channel','str');
  %  skip on to the rasters.
  junk=skipTo(fid,'nrasters=');
  if isempty(junk)
	error('No calibration data in file!')
  end
  
  nrasters=sscanf(junk,'nrasters=%f');
  
  % Since it's a matrix, we can read it all in with one
  % line of code.  Matlab fills in column order, so we
  % have to transpose the result.
  [rawdata,count]=fscanf(fid,'%e',[numcols nrasters]);
  rawdata=rawdata';
  
  if count<numcols*nrasters
	warning('Calibration file is data-light');
  end
  
  % The double fgetl is to skip past the newline that
  % reading in the calibration matrix leaves for us.
  junk=fgetl(fid);
  junk=fgetl(fid);
  if isempty(findstr('END_R',junk))
	error('Extra data in calibration file!')
  end
  
  data(:,1)=rawdata(:,1);
  
  if strncmp(refChannel,'L',1);
	So=hashFind(params,'So_Left','num');
	data(:,2)=rawdata(:,5)./rawdata(:,2);
	data(:,3)=rawdata(:,6)./rawdata(:,3);
  else
	So=hashFind(params,'So_Right','num');
	data(:,2)=rawdata(:,2)./rawdata(:,5);
	data(:,3)=rawdata(:,3)./rawdata(:,6);
  end
  data(:,3)=unwrap(data(:,3));
  data(:,2)=sqrt(2).*So.*data(:,2);