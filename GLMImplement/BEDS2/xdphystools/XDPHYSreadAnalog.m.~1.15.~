function [waveform, chan_ids, tomv, offset] = XDPHYSreadAnalog(filefid,anafid,params,analog)

% XDPHYSreadAnalog   read analog data section of
%                    xdphys files
%
%           THIS IS A PRIVATE M-FILE!
%
% This function is a NOOP if analog == 0
%
%
% See:  readXDPHYSfile

% Written by GBC, 19 Mar 2001.
% RCS: $Id: XDPHYSreadAnalog.m,v 1.15 2003/05/30 17:36:10 bjorn Exp $

%The *only* reason this is a separate file is for
%legibility. 

% Here are some notes about the old dowl/early xdphys .ana file format:
%
% I make a big assumption here that the order and number of rasters in the 
% main DOWL file is the same as the order and number of traces in this 
% analog file.  This should be the case, since the two files were written
% simultaneously.
%
% All of these type .ana files are big-endian.
%
%  An old style header struct looks like, for dowl, and pre-gen module xdphys:
%
%   short      magic   ( = 0x5731 )
%   int        fc
%   int        nsamps
%   int        nchans
%   char[100]  comment
%   int        bits
%   char[20]   program
%   char[20]   arch
%   int        tomv
%   int        tomv_div
%   int        offset
%   int        offset_div
%   char[32]   padding
%   int        rasternum
%   char[300]  padding
%
%  It is 512 bytes long, and is defined in xdphys/src/waveio.h
%  xdphys is probably compiled to align structs along 4 byte boundries,
%  so types with size < 4 get converted to 4 bytes in the header. This 
%  header preceeds each trace.  Old style ana files are always written in
%  big endian format.


waveform = {};
chan_ids = 1;
tomv = 0;
offset = 0;

if (analog)
  % the first thing we have to do is determine whether we're dealing with
  % a dowl or xdphys file.
  
  % dowl analog traces are stored in a different file from the raster file.

  % if anafid is ~= -1, a separate analog file exists
  % filefid should always be ~= -1

  if (anafid ~= -1)
    % --------------------------------
    % Separate .ana file
    % --------------------------------
    fid = anafid;
    fread(fid,1,'int');  % magic
    fread(fid,1,'int');  % fc
    nsamps = fread(fid,1,'int');
    nchans = fread(fid,1,'int');

    fread(fid,100,'char'); % comment
    fread(fid,44,'char');  % bits, program, arch
    tomv_base=fread(fid,1,'int');  % tomv
    tomv_div=fread(fid,1,'int');  % tomv_div
    offset_base=fread(fid,1,'int');  % offset
    offset_div=fread(fid,1,'int');  % offset_div

    tomv=ones(1,nchans)*(tomv_base/tomv_div);
    offset=ones(1,nchans)*(offset_base/offset_div);

    fread(fid,32,'char');  % padding
    fread(fid,1,'int');    % rasternum
    fread(fid,300,'char'); % more padding

    % Now read the trace(s)

    trace=fread(fid,nsamps*nchans,'short');

    % If nchans > 1, channels are interleaved
    if (nchans > 1) 
      for i=1:nchans
	  waveform{i}=trace(i:nchans:length(trace));	
	  chan_ids(i)=i; % really have no clue, so make something up
      end
    else 
      waveform = {trace};
      chan_ids = 1;
    end
  else
    % --------------------------------
    % No separate .ana file
    % --------------------------------
    fid=filefid;

    adFc = hashFind(params,'adFc','num');
    epoch = hashFind(params,'epoch','num') / 1000;
    [anadec,success] = hashFind(params,'ana-decimate','num');
    % So it might happen that we're passed a dowl data
    % file with no accompanying .ana file.  If that's
    % the case, then anadec will not be in the params
    % file, so we'll use that as the test.  This may not
    % be a good idea, because this might actually turn
    % out to be a silent failure for some other unknown
    % cases, but do I look like I give a fuck?
    if ~success
      return;
    end
    
    nsamps = (adFc * epoch) / anadec;
    
    % Okay if this fails, since any version that doesn't
    % have it doesn't support multiple channels anyways. 
    nchans = hashFind(params,'ana.nchans','num');

    [file_version,success] = hashFind(params,'file.version', 'num');
    if (~success)
      % This is an xdphys file,  file format version 2 or lower 
      % (only file format version 3 and higher have "file.version")
      file_version = 0;
      tomv=hashFind(params,'ana-tomv','num');
      offset=hashFind(params,'ana-offset','num');
    else
      i=1;
      j=1;
      while j < (nchans+1)
	tmp=sprintf('ana-tomv.chan%d',i);
	[tomv_tmp,success]=hashFind(params,tmp,'num');
	if (success)
	  tomv(j)=tomv_tmp;
	  % if success == 1, this hashFind will succeed,
          % also
	  tmp = sprintf('ana-offset.chan%d',i);
	  offset_tmp = hashFind(params,tmp,'num');
	  offset(j)=offset_tmp;
	  j=j+1;
	end
	i=i+1;
      end
    end

    % Get the first four characters (so we can check if
    % there actually is any analog data) and then rewind
    % back to the first position (either because there
    % was no data or to simplify the loop).
    position = ftell(fid);
    tmp=char(fread(fid,5,'char')');
    fseek(fid,position,'bof');
    if (strcmp(tmp,'depva') | strcmp(tmp,'STIMU') | strcmp(tmp,'END_R')) 
      %There's no trace associated with this one (or this is a
      %dowl file, for which the analog waveforms are stored
      %separately), so we're done.
      return
    else 
      % Now we have to distinguish between v3 files and < v3
      % files
      if (file_version >= 3)
        % We have v3
        for i=1:nchans
	  tmp = skipTo(fid,'channel=');
	  chan_ids(i) = sscanf(tmp,'channel=%d',1);
	  % Read the waveform      
	  waveform{i}=XDPHYSreadWaveform(fid,nsamps);
	  % Skip over the "END_TRACE\n"
          tmp=char(fread(fid,10,'char')');
        end
      else
        % This is a v2 file
        waveform = {XDPHYSreadWaveform(fid,nsamps)};
      end
    end
  end
end
