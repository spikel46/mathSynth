function convertXDPHYSfiles(glob,analog)

% convertXDPHYSfiles    Convert xdphys files to MAT
%                       files.
%
% CONVERTXDPHYSFILES(GLOB) will convert all xdphys files
% matching GLOB (as if you were using ls) into mat-files,
% saving them with file extension .mat.
%
% CONVERTXDPHYSFILE(GLOB,ANALOG) will do the same, but if
% ANALOG is 0, will not process any analog data in the
% files.  By default, ANALOG is 1 (process analog data).

% convertXDHYSfiles gives a unique name to the contents
% of the matfile.  To reassign the name upon loading,
% use BEDSload.
%
% See also:  BEDS, readXDPHYSfile, BEDSload, convertXDPHYSGZfiles

% Written by GBC, 21 Mar 2001.
% RCS: $Id: convertXDPHYSfiles.m,v 1.16 2005/01/15 00:57:38 bjorn Exp $
% Depends on:  xdphystools/readXDPHYSfile.m
%              xdphystools/XDPHYSprocessFilename.m
%              gzip
%              bzip2

% Sort out all the command line options.  By default,
% analog=1, compress='n'.
if nargin < 2
  analog = 1;
end

% Get the files to be processed. Sort the list so that it
% gets done in the order that people think (in case of
% mistake, etc.)
filelist = dir(glob);
filenames = sort({filelist.name});
% Now go through the files.
for n = 1:length(filelist);
  if ~filelist(n).isdir
    filename = filenames{n};
    disp(filename);
    [animal,unit,filenum,extranum]=XDPHYSprocessFilename(filename,'str');
    
    dotsInFile = findstr(filename,'.');

	newfile = sprintf('%s.mat',filename);
    %We name the data 'a###u%%f--, where ### is the animal
    %number, %% is the unit number, and -- is the file
    %number.  Additionaly, we may append a e!, where ! is
    %whatever extra number has been tacked on in a
    %four-number filename. 
    if (isempty(extranum))
      dataname = sprintf('a%su%sf%s',animal,unit,filenum);
    else
      dataname = sprintf('a%su%sf%se%s',animal,unit,filenum,extranum);
    end
    
    % Process the file using readXPDHYSfile.
    eval(sprintf('%s=readXDPHYSfile(''%s'', %d);',dataname,filename,analog));
    % Save the new file.
    eval(sprintf('save %s %s;',newfile,dataname));
    % Since these variables can take up a lot of space
    % (especially if we save the analog data) clear them
    % from memory as we go along.
    eval(sprintf('clear %s;',dataname));
			
  end
end
