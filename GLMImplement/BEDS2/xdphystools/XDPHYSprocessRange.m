function vector=XDPHYSprocessRange(rangestr)
% XDPHYSprocessRange   Evaluate a XDPHYS-style range string
%
% V = XDPHYSPROCESSRANGE(STR) will evaluate the XDPHYS-style
% range expression (i.e, MIN:MAX:STEP, or N1,N2,...) in STR
% and return the corresponding vector V.  STR must be a
% string containing the XDPHYS range string, and nothing
% else.  No checking is done on the input.
%
% See also:  BEDS, readXDPHYSfile

% Written by GBC, 30 May 2003.
% RCS:  $Id: XDPHYSprocessRange.m,v 1.1 2003/05/30 17:36:10 bjorn Exp $

  commas=findstr(rangestr,',');
  if ~isempty(commas)
	vector=[];
	junk=rangestr;
	for n=1:length(commas);
	  vector=[vector str2num(junk(1:commas(n)-1))];
	  junk=junk(commas(n)+1:end);
	end
	vector=[vector str2num(junk)];
  else
	colons=findstr(rangestr,':');
	if length(colons)~=2
	  error(['Bjorn is an idiot who forgets how XDPHYS' ...
			 ' range strings work']);
	end
	minval=str2num(rangestr(1:colons(1)-1));
	maxval=str2num(rangestr(colons(1)+1:colons(2)-1));
	stepsize=str2num(rangestr(colons(2)+1:end));
	eval(sprintf('vector=[%d:%d:%d];',minval,stepsize, ...
				 maxval));
  end
  
  