function params = XDPHYSreadParams(fid)

% XDPHYSreadParams       process parameters in xdphys files
%
%           THIS IS A PRIVATE M-FILE!
%
% See:  readXDPHYSfile

% Written by GBC, 19 Mar 2001.
% RCS: $Id: XDPHYSreadParams.m,v 1.2 2001/03/20 04:50:26 bjorn Exp $
% Depends on:  hashtable/hashMake.m
%              hashtable/hashReplace.m

%The *only* reason this is a seperate file is for
%legibility. 

params = hashMake(691);
buffer = fgetl(fid);
while (~strncmp(buffer,'END',3))
  [name,value]=strtok(buffer,'=');
  if ~isempty(value)
    value = value(2:length(value));
    % If the parameter contains a newline, it's a special
    % case.  We use the !BEGIN to deal with this.
    if ~strncmp(value,'!BEGIN',6)
      params = hashReplace(params,name,value);
    else
      warning('!BEGIN encountered in PARAMS');
      [count,suc] = sscanf(value,'!BEGIN%d');
      if suc ~= 1
	error(sprintf('Warning: bad value for %s', ...
		      name));
      end
      [value, rcount] = fread(fid,count,'char');
      if rcount ~= count
	error(sprintf(['Warning: short value for' ...
	' %s'],name));
      else
	params = hashReplace(params,name,value);
      end
    end
  end
  buffer = fgetl(fid);
end
