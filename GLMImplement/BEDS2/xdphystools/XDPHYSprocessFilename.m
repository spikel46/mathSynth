function [animal,unit,filenum,extra]=XDPHYSprocessFilename(filename,format)
% XDPHYSprocessFilename -- break down and extract
% relevant information from an XDPHYS filename.
%
% THIS IS FOR INTERNAL USE ONLY.  Do not call directly.

% Written by GBC, 9 Aug 2001.
% RCS: $Id: XDPHYSprocessFilename.m,v 1.8 2002/03/20 22:31:20 bjorn Exp $

% Basically, everyone and their mother has their own
% little variation on how they name xdphys datafiles.
% Hunting down all the references to such is a pain.  So
% this concentrates it.

% Sometimes we need the information as numbers, sometimes
% as strings.  This allows the conversion.  Default is
% 'num', for a strange, arbitary, impractical reason that
% no longer applies.
if nargin < 2
  format = 'num';
end

if strncmp('str',format,3)
  conv = 0;
else
  conv = 1;
end

% We may be passed a fully-qualified file name, in which
% case we should ignore the path in our calculations.
index = findstr(filesep,filename);
if ~isempty(index)
  filename = filename(index(end)+1:end);
end

% Jose names files with the date prefixing them.
josefiles = findstr(filename,'-');
if isempty(josefiles);
  junk = filename;
else
  junk = filename(josefiles(end)+1:end);
end

% Kazuo, on the other hand, has files named in the form
% x.y.z.w.itd, where w is an extra parameter that
% indiciates the nature of the stimulus.  So now I get
% paranoid and count the number of dots in the filename.

numDotsInName = length(findstr(junk,'.'));
[animal,junk]=strtok(junk,'.');
[unit,junk]=strtok(junk,'.');
[filenum,junk]=strtok(junk,'.');

% We might be passed a compressed filename (though I
% don't think it happens) or a .mat file (which does
% happen -- BEDSload).  In either case, we should ignore
% that suffix, so we subtract one dot.
if ~isempty(findstr('gz',filename))
  numDotsInName = numDotsInName - 1;
end

% Now, sometimes (shouldn't happen anymore, but...),
% there's a .mat suffix without the .itd/.iid/whatever
% suffix.  In which case we *shouldn't* subtract a dot.
% So we check to see if the character two before the mat
% suffix is a character.  Now, *if* we hit a character,
% we know for sure we're okay.  But if we *don't*, it
% might be some strange XDPHYS suffix.  So generate a
% warning, but not an error.
matloc = findstr('mat',filename);
if ~isempty(matloc)
  if isletter(filename(matloc-2))
    numDotsInName = numDotsInName - 1;
  else
    warning(['File identified as not having an XDPHYS file' ...
	     ' type suffix.']);
  end
end

% Finish handling Kazuo's extra number.
if (numDotsInName == 4)
  [extra,junk]=strtok(junk,'.');
elseif (numDotsInName == 3)
  extra = '';
else
  error('Another friggin'' style of XDPHYS filename!');
end

% Now, if necessary, convert from strings to numbers.
if conv
  animal = str2num(animal);
  unit = str2num(unit);
  filenum = str2num(filenum);
  extra = str2num(extra);
end
