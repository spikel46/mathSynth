function flat=XDPHYSremoveCalib(calib,filtered,samprate,side,band)
% XDPHYSremoveCalib        Undo calibration filtering.
%
% FLAT=XDPHYSREMOVECALIB(CAL,STIM,SR,SIDE,BAND) takes the
% stimulus STIM (which was sampled at SR (Hz), presented on
% SIDE (a string, either 'L' or 'R'), and was bandpassed at
% BAND (Hz)), and removes the calibration filtering done
% by CAL, returning the result in FLAT.
%
% This should only be run on the actual stimulus.  Most
% saved stimuli also include the zero-padding before and
% after the stimulus, and that will distort your results.
%
% This function is vectorized.  If given a matrix of
% filtered stimuli, one stimuli per column (NOT per row), it
% will process all of them at once.  Obviously, all
% stimuli must be the same length, sampling rate, etc.
%
% It is also not recommended to use
% this with anything but broad-band or narrow-band stimuli.
%
% See also: readXDPHYScalib
  
% RCS: $Id: XDPHYSremoveCalib.m,v 1.6 2005/02/03 02:08:38 bjorn Exp $
% Written by GBC, 17 Jul 2003.

% These column values have 1 subtracted, since we strip
% the freq column from the calibration matrix below.

if strncmp(upper(side),'L',1)
  magCol = 1;
  phaseCol=2;
elseif strncmp(upper(side),'R',1)
  magCol = 7;
  phaseCol=8;
end

junk=size(filtered);
outlen=junk(1);
numstims=junk(2);

fft_len=2^nextpow2(outlen);

% Get the calibration data for the frequencies present in
% the discrete Fourier Transform.  Note that because
% radians are frequency-dependent, the linear
% interpolation needs to be done with the phases stored
% in microseconds.  Convert to us, and then back after
% the interp1 call.
freqs=(1:fft_len/2)./(fft_len-1)*samprate;
calib(:,3)=1000.*calib(:,3)./(2*pi*calib(:,1));
calib(:,9)=1000.*calib(:,9)./(2*pi*calib(:,1));
newCalib=interp1(calib(:,1),calib(:,2:end),freqs,'linear');
newCalib(:,2)=2*pi*freqs'.*newCalib(:,2)./1000;
newCalib(:,8)=2*pi*freqs'.*newCalib(:,8)./1000;

% The calibration is finite, and the stimuli is
% bandpass.  Choose the narrowest width.
if nargin<5
  lowFreq=min(calib(:,1));
  highFreq=max(calib(:,1));
else
  if min(calib(:,1))<min(band)
	lowFreq=min(band);
  else
	lowFreq=min(calib(:,1));
  end
  if max(calib(:,1))>max(band)
	highFreq=max(band);
  else
	highFreq=max(calib(:,1));
  end
end

invalidFreqs=find((freqs<lowFreq)|(freqs>highFreq));

% FFT.
Filtered=fft(filtered,fft_len);
% Scale factors.  Don't touch stuff outside the band-pass range.
scale=1./(10.^((min(newCalib(:,magCol))-newCalib(:,magCol))./20));
shift=exp(-i.*newCalib(:,phaseCol));
scale(invalidFreqs)=1;
shift(invalidFreqs)=1;
scale=repmat(scale,1,numstims);
shift=repmat(shift,1,numstims);
% Scale and shift the real frequencies, and then mirror
% the results into the negative frequencies with a
% complex conjugation.  Since we had a real signal to
% start with, this is fine, and it's easier than
% reversing the phase shift for negative frequencies.
Flat=Filtered;
Flat=zeros(size(Filtered));
Flat(2:length(freqs)+1,:)=scale.*shift.*Filtered(2:length(freqs)+1,:);
Flat(length(freqs)+2:end,:)=conj(Flat(length(freqs):-1:2,:));

% Inverse transform, drop to real space, and cut off the
% zero padding.  Note that specifying the output length
% in the IFFT downsamples rather than shortening (!?)
flat=ifft(Flat);
flat=real(flat(1:length(filtered),:));
