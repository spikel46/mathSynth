function waveform = XDPHYSreadWaveform(fid, nsamps)

% XDPHYSreadWaveform
% 
%  This function converts the hex block that is the 
%  analog waveform in an xdphys data file to integers
%
%
%           THIS IS A PRIVATE M-FILE!
%
%
% See:  readXDPHYSfile

% Separated from XDPHYSreadAnalog.m by CPM, 10 Jul 2002 so that that file
% and XDPHYSreadStimulus could share it.

% RCS: $Id: XDPHYSreadWaveform.m,v 1.2 2005/01/15 02:20:32 bjorn Exp $

waveform = [];
finished = 0;

% Process a whole line (20 samples) in one go.
while ~finished
  position=ftell(fid);
  tmp = fgetl(fid);
  [wavebuf,count,err] = sscanf(tmp,'%4x',20);
  % if we hit END_TRACE or 'depvar=', wavebuf is empty.
  % We back up one line then, since some older files
  % don't have END_TRACE, and without backing up, we'll
  % skip a raster.
  if isempty(wavebuf) | ~isempty(err)
    fseek(fid,position,'bof');
    finished = 1; 
  else
    % Xdphys does this (stupid (but legal (but undefined)))
    % thing of going signed short->unsigned long->signed short
    % in saving data.  Because Matlab stores internally in
    % doubles, we don't get the bit wrap that makes this
    % work.  So we make our own.
    wavebuf = wavebuf - (wavebuf > 32767)*65536;
    waveform = [waveform;wavebuf];
  end
end
