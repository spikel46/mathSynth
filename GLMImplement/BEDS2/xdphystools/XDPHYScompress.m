function newfile = XDPHYScompress(filename)

% XDPHYScompress        Compress an XDPHYS file.
%
% NEWFILE = XDPHYSCOMPRESS(FILE) will compress the XDPHYS
% file FILE and return the new filename in NEWFILE.
%
% This function is meant for use only within other
% xdphystools functions.

% Written by GBC, 22 Mar 2001
% RCS: $Id: XDPHYScompress.m,v 1.4 2001/04/25 21:11:42 jose Exp $
% Depends on:  /usr/bin/gzip (or wherever)

% Try to figure out what our OS is ...
os=computer;
if ((strncmp(os,'LNX86',5)) | (strncmp(os,'GLNX86',6)))
  failed = unix(sprintf('gzip %s',filename));
  if failed
    error(sprintf('Unable to compress %s',filename));
  end
  newfile = sprintf('%s.gz',filename);
else
  warning(sprintf('You are not using UNIX, unable to compress %s', filename));
end
