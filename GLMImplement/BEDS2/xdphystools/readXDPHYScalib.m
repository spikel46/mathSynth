function calib=readXDPHYScalib(glob)
  
% readXDPHYScal           Read in an XDPHYS .cal file.
%
% CAL=READXDPHYSCALIB(GLOB) will return the calibration
% data in all XDPHYS .cal files matching GLOB.  If GLOB
% matches only a single file, then CAL will be a matrix;
% otherwise, CAL will be a cell array of calibration
% matrices.
%
% The format of the calibration matrix is as follows:
%  Column 1: freq (Hz)
%  Column 2: left_mag (dB)
%  Column 3: left_phase (rad)
%  Column 4: left_distortion (100*f2_mag/f1_mag)
%  Column 5: left_leak_mag (dB, delta_mag for right stim)
%  Column 6: left_leak_phase (rad, delta_phase for right stim)
%  Column 7: left_leak_distortion (100*f2_leak_mag/f1_leak_mag)
%  Column 8: right_mag (dB)
%  Column 9: right_phase (rad)
%  Column 10: right_distortion (100*f2_mag/f1_mag)
%  Column 11: right_leak_mag (dB, delta_mag for left stim)
%  Column 12: right_leak_phase (rad, delta_phase for left stim)
%  Column 13: right_leak_distortion (100*f2_leak_mag/f1_leak_mag)
%  Column 14: left_mag_stderr
%  Column 15: left_phase_stderr
%  Column 16: right_mag_stderr
%  Column 17: right_phase_stderr
%
%  In practice, only columns 1, 2, 3, 8, and 9 are used.
%
%  See also: readXDPHYSfile, readXDPHYSfr.
 
% Written by GBC, 17 Jul 2003.
% RCS: $Id: readXDPHYScalib.m,v 1.2 2003/08/04 16:53:53 bjorn Exp $
% Depends on:  misc/skipTo.m

% Get the files to be processed. Sort the list so that it
% gets done in the order that people think (in case of
% mistake, etc.)
filelist = dir(glob);
filenames = sort({filelist.name});

numcols=17;

% Return value is a little different depending on the
% number of files to process.
if isempty(filelist)
  error('No such file!')
elseif length(filelist)==1
  calib=processCalibFile(filenames{1},numcols);
else
  for n=1:length(filelist)
	disp(filenames{n})
	calib{n}=processCalibFile(filenames{n},numcols);
  end
end

% This does the work.
function data=processCalibFile(filename,numcols)
  fid=fopen(filename,'r');
  if (fid==-1)
	error(sprintf('Unable to open file: %s',filename));
  end
  
  % The .cal file actually stores a bunch of parameters
  % and such, but as of now, we don't really care about them.
  junk=skipTo(fid,'nrasters=');
  if isempty(junk)
	error('No calibration data in file!')
  end
  
  nrasters=sscanf(junk,'nrasters=%f');
  
  % Since it's a matrix, we can read it all in with one
  % line of code.  Matlab fills in column order, so we
  % have to transpose the result.
  [data,count]=fscanf(fid,'%e',[numcols nrasters]);
  data=data';
  
  if count<numcols*nrasters
	warning('Calibration file is data-light');
  end
  
  % The double fgetl is to skip past the newline that
  % reading in the calibration matrix leaves for us.
  junk=fgetl(fid);
  junk=fgetl(fid);
  if isempty(findstr('END_R',junk))
	error('Extra data in calibration file!')
  end
  data(:,3)=unwrap(data(:,3));
  data(:,9)=unwrap(data(:,9));