function [anafid,nparams,ana_filename,iscompressed] = XDPHYSopenExternalAnafile(filename, params)

% XDPHYSopenExternalAnafile
%
% Deal with external analog files.  dowl and pre-gen 
% xdphys both stored analog traces in a separate file.
%
% Returns anafid = -1 if no external analog file exists

% Written by CM, 1 Apr 2001.
% RCS: $Id: XDPHYSopenExternalAnafile.m,v 1.5 2001/06/08 19:51:29 bjorn Exp $

nparams = params;

% Okay, this is pretty ridiculous.  Basically, the .ana
% file may or may not be compressed.  So we try to open
% it.  If we fail, then either it doesn't exist or it is
% gzipped, so we change the name to foo.gz.  Regardless,
% the only file opening that counts is the one *after*
% XDPHYSdecompress -- the first is just a check (which is
% why we close it right away).
ana_filename=sprintf('%s.ana',filename);
anafid = fopen(ana_filename,'r','ieee-be');
if anafid == -1
  ana_filename=sprintf('%s.gz',ana_filename);
  anafid = fopen(ana_filename,'r','ieee-be');
  if anafid == -1
    iscompressed = 0;
    ana_filename = '';
    return;
  end
end
fclose(anafid);
[ana_filename,iscompressed]=XDPHYSdecompress(ana_filename);

% Dowl/xdphys external ana-files are big endian
anafid=fopen(ana_filename,'r','ieee-be'); 
%	anafid=fopen(ana_filename,'r'); 
% if the file doesn't exist, anafid == -1
if (anafid ~= -1)
  % We need to extract the conversion factors from the
  % header
  
  % skip over stuff in the header that we don't care
  % about
%  [nsamps,nchans,tomv,tomv_div,offset,offset_div] = ...
%      XDPHYSparseAnaHeader(ana_filename,0)
  fread(anafid,160,'char');
  to_mv=fread(anafid,1,'int');
  to_mv_div=fread(anafid,1,'int');
  offset=fread(anafid,1,'int');
  offset_div=fread(anafid,1,'int');
  nparams=hashInsert(params,'ana-tomv',sprintf('%f',(to_mv/to_mv_div)));
  nparams=hashInsert(nparams,'ana-offset',sprintf('%f',(offset/offset_div)));
  
  % Now rewind the file
  fseek(anafid,0,'bof');
end



