function [newfile,compressed] = XDPHYSdecompress(filename);

% XDPHYSdecompress     Decompress an xdphys file.
%
% [NEWFILE,COMPRESS] = XDPHYSDECOMPRESS(FILE) will
% decompress FILE is it was compressed, returning the new
% file name in NEWFILE.  COMPRESS will be 1 if the file
% was compressed, and 0 otherwise.  If the file was not
% compressed, NEWFILE == FILE.
%
% This function is meant for usage only within other
% XDPHYStools functions.

% Written by GBC, 22 Mar 2001.
% RCS: $Id: XDPHYSdecompress.m,v 1.5 2001/04/27 23:08:28 bjorn Exp $
% Depends on:  /usr/bin/gzip (or wherever it is)

os=computer;
newfile = filename;
compressed = 0;

iscompressed = findstr(filename,'.gz');

if (~isempty(iscompressed))
  if ((strncmp(os,'LNX86',5)) | (strncmp(os,'GLNX86',6)))
    failed = unix(sprintf('gzip -d %s',filename));
    if failed
      error(sprintf('Unable to decompress %s',filename));
    end
    newfile = filename(1:iscompressed-1);
    compressed = 1;
  else
    error(sprintf(['You are not using UNIX, you must decompress' ...
		   ' %s by hand.'],filename));
  end
end

