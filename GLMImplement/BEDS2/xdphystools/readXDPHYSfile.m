function data = readXDPHYSfile(filename,analog)

% readXDPHYSfile   Read Xdphys data.
%
% DATA = READXDPHYSFILE(FILE) will process the xdphys
% file FILE and return the data in the BEDS structure
% DATA.
%
% DATA = READXDPHYSFILE(FILE,ANALOG) will do the same,
% but if ANALOG is 0, then any analog data in the file
% will be skipped.  This includes saved stimuli from
% file.version 4 onwards. By default, ANALOG is 1.
%
% readXDPHYSfile can also process a .ana file.  However,
% since much of the needed information is not available,
% many of the analysis tools will not work correctly.
% THEREFORE, THIS USE IS DEPRECATED.  The correct way to
% process a .ana file is to give readXDPHYSfile the name
% of the accompanying spike data file, and the .ana file
% will automatically be processed with it.
%
% See also:  BEDS.

% Written by GBC, 01 Mar 2001.
% RCS:  $Id: readXDPHYSfile.m,v 1.47 2005/02/03 02:08:38 bjorn Exp $
% Depends on:  misc/skipTo.m
%              xdphystools/XDPHYSreadParams.m
%              xdphystools/XDPHYSreadRaster.m
%              xdphystools/XDPHYSreadAnalog.m
%              xdphystools/XDPHYSreadStimulus.m
%              xdphystools/XDPHYSdecompress.m
%              xdphystools/XDPHYScompress.m
%              xdphystools/XDPHYSopenExternalAnafile.m
%              xdphystools/XDPHYSprocessFilename.m
%              xdphystools/XDPHYSprocessRange.m
%              hashtable/hashReplace.m
%              hashtable/hashFind.m
%              bedstools/BEDSmakeVersion.m
%              bedstools/BEDSmakeTrial.m
%              bedstools/BEDSmakeChannel.m
%              bedstools/BEDSmakeStimlus.m
%              bedstools/BEDSstimulusType.m
%              bedstools/BEDSinit.m

% This file also contains the internal functions
% parseDOWLparams, parseDOWLfreqParam, assignDOWLparams,
% and processXDPHYSfreqParam.

% Most of the actual guts of processing the file is done
% by the (private) XDPHYSread* functions.  Basically, all
% we do here is advance the file pointer and some string
% parsing crap.

% BUGS/TODO:
%
%   - Right now, assume that filename is in xdphys format.
%   Should probably do some checking on that.
%   - Not all the legacy code is in place.  (See
%   XDPHYSreadRaster)
%   - No support for spike categories.  This *only* matters
%   if you're using the software spike discriminator, in
%   which case outlier spikes will be silently discarded.

% Current supported xdphys file format
XDPHYS_FILE_VERSION = 4;

% These are for describing the data contents of a BEDS channel struct
% These should match up with what is defined in BEDSmakeChannel.m
% (Is there a better way to do this?)

anafid = -1;

% Open the file, decompressing if needed.
[filename,iscompressed]=XDPHYSdecompress(filename);
fid = fopen(filename,'r');
if (fid == -1)
    error('unable to open file');
end

% If not specified, assume we should read the analog
% data.
if (nargin < 2)
    analog = 1;
end

% On occasion, we may be required to process nothing but
% a .ana file.  But we won't.
dots = findstr(filename,'.');
if (strncmp(filename(dots(end)+1:end),'ana',3))
    if ~analog
        warning(['Attempting to process analog data file with' ...
            ' ANALOG 0: returned value is empty.']);
        data = [];
    else
        warning(['Direct processing of .ana files not' ...
            ' supported: returned value is empty.']);
        data = [];
    end
    return;
end

% Process the file header, which will give us the
% program, the module run within the program, and the
% version of the program.
header = fgetl(fid);
[junk,header]=strtok(header);
if (junk ~= ';;')
    error('Not xdphys file');
end
[progname,header]=strtok(header);
[modulename,header]=strtok(header);
[junk,header]=strtok(header);
if (strncmp(header(2:end),'$R',2))
    [junk,version]=strtok(header);
else
    version = header;
end
[major_version_str,version]=strtok(version,'.');
[minor_version_str,version]=strtok(version,'.');
if (~isempty(version))
    revision_str=strtok(version(2:end),'-');
else
    revision_str='0';
end
major_version=sscanf(major_version_str,'%d');
minor_version=sscanf(minor_version_str,'%d');
revision=sscanf(revision_str,'%d');

% Check if we're DOWL or xdphys.
if strncmp('dowl',progname,4)
    IS_DOWL = 1;
else
    IS_DOWL = 0;
end

% Make the BEDS version entry.
Bversion = BEDSmakeVersion(progname,major_version,minor_version,revision);

% Skip over comments and go to parameters
if isempty(skipTo(fid,'PARAMS'))
    error('Could not find PARAMS ; short data file?');
end

% Load up a hashtable with the parameters.
params = XDPHYSreadParams(fid);

% Get the file.version param, and check it for
% compatibility.
[fileVersion,success]=hashFind(params,'file.version','num');
if ~success
    fileVersion=0;
end
if fileVersion > XDPHYS_FILE_VERSION
    error('Unsupported XDPHYS file format');
end

% Save some extra stuff in the hashtable.
params = hashReplace(params,'bedsProg',progname);
params = hashReplace(params,'bedsMaj',major_version);
params = hashReplace(params,'bedsMin',minor_version);
params = hashReplace(params,'bedsRev',revision);

% Look for an external analog file
% We do this here instead of above, in the "check version" if/then,
% because we need to look at some hash entries in 'params'
[anafid,params,ana_filename,ana_iscompressed]= ...
    XDPHYSopenExternalAnafile(filename,params);

% I'm going to blatantly ignore spike sorting.  This is
% perfectly acceptable, since it doesn't nor has it ever
% worked.
ssmode = hashFind(params,'detect.mode');
if (strcmp(ssmode,'ss'))
    error(['Who are you kidding? Spike sorting not' ...
        ' supported.']);
end

% Some backwards compatibility issues in the params.
% Apparently, dowl and some old version of xdphys stored
% the D->A, A->D, and event frequency sampling rate in
% kHz, instead of Hz.

% The version dowl 2.51 was a rogue version which
% should be largely equivalent to 2.47
if (((minor_version < 45) | (minor_version == 51)) &  ...
        (strncmp(progname,'dowl',4)))
    fc = hashFind(params,'adFc','num');
    params = hashReplace(params,'adFc',fc * 1000);
    fc = hashFind(params,'daFc','num');
    params = hashReplace(params,'daFc',fc * 1000);
    fc = hashFind(params,'evtFc','num');
    params = hashReplace(params,'evtFc',fc * 1000);
end

% Get some miscellaneous parameters and initialize a few
% variables before the real work starts.
fc = hashFind(params,'adFc','num');

[animal,unit,filenum,stimnum]=XDPHYSprocessFilename(filename);

[discrim_chan,success]=hashFind(params, ...
    'detect.discrim_channel','num');
if (~success)
    discrim_chan = 1;
end

% Jump down to the data
junk = skipTo(fid,'nrasters=');
if isempty(junk)
    error('Could not find nrasters ; short data file?');
end

% Raster read loop
% -----------------------------------------------------------

% Get the number of rasters and start reading them in.
nrasters = sscanf(junk,'nrasters=%f');

% Clicks need special processing...
[click_mono,success]=hashFind(params,'click.Side','str');
if success
    clickfile = 1;
else
    clickfile = 0;
end

% ... So do beats...
[beat_iid,success]=hashFind(params,'beats.iid','num');
if success
    beatfile = 1;
    [beat_abi,success1]=hashFind(params,'beats.abi','num');
    [beat_carrier,success2]=hashFind(params,'beats.carrier','num');
    [beat_signal,success3]=hashFind(params,'beats.signal','num');
    if ~(success1&success2&success3)
        error('Only a partial beat file? Couldn''t find all relevant params!');
    end
else
    beatfile = 0;
end

% ... As do the now-obsolete rover files.
%
% A note:  Rover files have odd depvars, which can be
% determined by:
%DepvarsITD = StartITD:StepITD:EndITD;
%DepvarsIID = StartIID:StepIID:EndIID;
%for i=0:length(DepvarsIID)-1
%  for j=1:length(DepvarsITD)
%    depvar = DepvarsITD(j)+(maxITD + 1 - minITD)*DepvarsIID(i+1);
%  end
%end
%
[rover_abi,success]=hashFind(params,'rover.abi','num');
if success
    roverfile=1;
    rover_stim=hashFind(params,'rover.stim','num');
    if rover_stim
        rover_type=BEDSstimulusType('STIM_TONE');
        rover_signal=rover_stim;
    else
        rover_type=BEDSstimulusType('STIM_BB');
        rover_signal=[];
    end
else
    roverfile=0;
end

% Dowl kept non-depvar params in the hash-table.
if IS_DOWL
    dowlparams=parseDOWLparams(params,modulename);
end

% Some versions of xdphys did not have two-tone or
% monaural info in the depvar line.  For them, we need to
% check the params.
[old_twosnd,success]=hashFind(params,'ts.on/off','num');
[old_mono,success]=hashFind(params,sprintf('%s.mono',modulename),'str');
if ~success
    old_mono='B';
end

for n = 1:nrasters
    stimparam = skipTo(fid,'depvar=');
    if isempty(stimparam)
        disp(sprintf('Incomplete file: only %d of %d rasters present.', ...
            n-1,nrasters));
        break;
    end
    depvar = sscanf(stimparam,'depvar=%f');
    % Parse the stimulus paramaters.  Note that the
    % spontaneous case really sucks, since we need to have
    % *some* non-empty value.
    if clickfile
        if (depvar == -6667)
            stim_type = BEDSstimulusType('STIM_SPONT');
            stim_signal=[];
            stim_abi=0; stim_itd=0; stim_iid=0; stim_bc=0;
            stim_mono='S';
            stim_twosnd=0;
        else
            stim_type = BEDSstimulusType('STIM_BB');
            stim_signal=[];
            stim_abi=0; stim_itd=depvar; stim_iid=0; stim_bc=100;
            stim_mono=click_mono;
            stim_twosnd=0;
        end
    elseif beatfile
        if (depvar == -6667)
            stim_type = BEDSstimulusType('STIM_SPONT');
            stim_signal=[];
            stim_abi=0; stim_itd=0; stim_iid=0; stim_bc=0;
            stim_mono='S';
            stim_twosnd=0;
        else
            stim_type = BEDSstimulusType('STIM_BEAT');
            stim_signal = [beat_carrier beat_signal];
            stim_abi=beat_abi;stim_iid=beat_iid;
            stim_itd=0;stim_bc=0;
            stim_mono='B';
            stim_twosnd=0;
        end
    elseif (depvar == -6666)
        stim_type = BEDSstimulusType('STIM_SPONT');
        stim_signal=[];
        stim_abi=0; stim_itd=0; stim_iid=0; stim_bc=0;
        stim_mono='S';
        stim_twosnd=0;
    elseif roverfile
        stim_type=rover_type;
        stim_signal=rover_signal;
        stim_abi=rover_abi; stim_mono='B'; stim_bc=100;
        stim_twosnd=0;
        stim_begin=findstr(stimparam,'<');
        stim_end=findstr(stimparam,'>');
        temp=sscanf(stimparam(stim_begin:stim_end),'<%d us, %d db>');
        stim_itd=temp(1); stim_iid=temp(2);
    elseif IS_DOWL
        [stim_twosnd,stim_bc,stim_itd,stim_iid, ...
            stim_abi,stim_mono,stim_type,stim_signal]= ...
            assignDOWLparams(dowlparams,modulename,depvar);
    else
        stim_begin = findstr(stimparam,'<');
        stim_end = findstr(stimparam,'>');
        stim_seps = findstr(stimparam,';');
        % For a while, xdphys ver 2.47, like dowl, did not
        % put all of the paramaters in the 'depvar=' line.
        % If this happens to be one of those files, we'll
        % treat it as if it were DOWL.
        if isempty(stim_seps)
            IS_DOWL=1;
            dowlparams=parseDOWLparams(params,modulename);
            [stim_twosnd,stim_bc,stim_itd,stim_iid, ...
                stim_abi,stim_mono,stim_type,stim_signal]= ...
                assignDOWLparams(dowlparams,modulename,depvar);
            %Maybe we're in the old, minus two-tone and mono,
            %depvar line version of xdphys.
        elseif(length(stim_seps)==4)
            stim_abi = str2num(stimparam(stim_begin+1:stim_seps(1)-1));
            stim_itd = str2num(stimparam(stim_seps(1)+2:stim_seps(2)-1));
            stim_iid = str2num(stimparam(stim_seps(2)+2:stim_seps(3)-1));
            stim_bc = str2num(stimparam(stim_seps(3)+2:stim_seps(4)-1));
            stim_freq = stimparam(stim_seps(4)+2:stim_end-1);
            [stim_type,stim_signal]=processXDPHYSfreqParam(stim_freq);
            stim_mono=old_mono;
            stim_twosnd=old_twosnd;
        else
            stim_abi = str2num(stimparam(stim_begin+1:stim_seps(1)-1));
            stim_itd = str2num(stimparam(stim_seps(1)+2:stim_seps(2)-1));
            stim_iid = str2num(stimparam(stim_seps(2)+2:stim_seps(3)-1));
            stim_bc = str2num(stimparam(stim_seps(3)+2:stim_seps(4)-1));
            stim_freq = stimparam(stim_seps(4)+2:stim_seps(5)-1);
            [stim_type,stim_signal]=processXDPHYSfreqParam(stim_freq);
            stim_mono = stimparam(stim_seps(5)+2:stim_seps(6)-1);
            stim_twosnd = str2num(stimparam(stim_seps(6)+2:stim_end-1));
        end
    end
    % Delay creating the BEDS Stimulus struct until after we know if
    % there are saved stimuli waveforms for this record

    % Read in the discrete event counting data.
    [spikebuf,toms] = XDPHYSreadRaster(fid,params,analog);

    % Deal with analog data and make the channel structure.
    [waveform,chan_ids,tomv,offset]=XDPHYSreadAnalog(fid,anafid,params,analog);
    chan=BEDSmakeChannel; % Reset to empty
    if (isempty(waveform))
        chan=BEDSmakeChannel(BEDSchannelType('CHAN_SPIKES'), ...
            spikebuf,toms,0,0,[],0,'');
    else
        spike_chan = find(chan_ids == discrim_chan);
        other_chan = find(chan_ids ~= discrim_chan);
        if isempty(spike_chan)
            chan(1)=BEDSmakeChannel(BEDSchannelType('CHAN_SPIKES'), ...
                spikebuf,toms,0,0,[],0,'');
        else
            chan(1) = BEDSmakeChannel(BEDSchannelType('CHAN_SPIKES_ANALOG'),...
                spikebuf,toms,waveform{spike_chan},...
                tomv(spike_chan),offset(spike_chan),fc,...
                sprintf('chan_id:%d',chan_ids(spike_chan)));
        end

        for mchan = 1:length(other_chan)
            chan(mchan+1)=BEDSmakeChannel(BEDSchannelType('CHAN_ANALOG'),[],0, ...
                waveform{other_chan(mchan)}, ...
                tomv(other_chan(mchan)),...
                offset(other_chan(mchan)), ...
                fc,sprintf('chan_id:%d',...
                other_chan(mchan)));
        end
    end

    % Deal with potential saved stimuli
    [stimuli,stim_chan_ids]=XDPHYSreadStimulus(fid,params,analog);
    if (~isempty(stimuli))
        numDataChans=length(chan);
        daFc = hashFind(params,'daFc','num');
        for i=1:length(stim_chan_ids)
            chan(numDataChans+i)= ...
                BEDSmakeChannel(BEDSchannelType('CHAN_STIM'), ...
                [], 0,stimuli{i}, 1, 0, daFc, ...
                stim_chan_ids(i));
        end
        stim_saved = 1;
    else
        stim_saved = 0;
    end

    % Now make the Stimulus record
    stimparam = BEDSmakeStimulus(stim_type, stim_signal, stim_abi, ...
        stim_itd, stim_iid, stim_bc, ...
        stim_mono, stim_twosnd, stim_saved);

    trial(n) = BEDSmakeTrial(animal,unit,filenum,n,modulename, ...
        depvar,stimparam,chan);

end % of trial processing.

% -----------------------------------------------------------

% Wrap everything up in a neat little package.
timestamp = hashFind(params,'time','str');
data = BEDSinit(Bversion,animal,unit,filenum,timestamp, ...
    params,'',trial);

% Close the external anafile and recompress if needed.
if (anafid ~= -1)
    failed = fclose(anafid);
    if (failed)
        error(sprintf('Could not close %s?!',ana_filename));
    end
    if ana_iscompressed
        XDPHYScompress(ana_filename);
    end
end

% Close the file and recompress if needed.
failed = fclose(fid);
if (failed)
    error(sprintf('Could not close %s?!',filename));
end
if iscompressed
    XDPHYScompress(filename);
end

% -----------------------------------------------------------

% The two DOWLparams functions are called from at least
% two different places, so we compartmentalize them to
% simplify any updates we might have to make.
function dowlparams=parseDOWLparams(params,modulename)
dowlparams=struct('stimtype',NaN,'signal',NaN,'itd',NaN,'iid', ...
    NaN,'abi',NaN,'mono','B','twosnd',NaN);
if strncmp('bf',modulename,2)
    disp('bf')
    [dowlparams.itd,success1]=hashFind(params,'bf.itd','num');
    [dowlparams.iid,success2]=hashFind(params,'bf.iid','num');
    [dowlparams.abi,success3]=hashFind(params,'bf.abi','num');
    [dowlparams.mono,success4]=hashFind(params,'bf.mono','str');
    success=success1&success2&success3&success4;
elseif strncmp('itd',modulename,3)
    [dowlparams.iid,success1]=hashFind(params,'itd.iid','num');
    [dowlparams.abi,success2]=hashFind(params,'itd.abi','num');
    % DOWL encoded frequency as either itd.stim=BB or
    % itd.stim=tone=<single frequency>.
    [temp,success3]=hashFind(params,'itd.stim','str');
    if success3
        [dowlparams.stimtype,dowlparams.signal]=parseDOWLfreqParam(temp);
    end
    success=success1&success2&success3;
elseif strncmp('iid',modulename,3)
    [dowlparams.itd,success1]=hashFind(params,'iid.itd','num');
    [dowlparams.abi,success2]=hashFind(params,'iid.abi','num');
    [temp,success3]=hashFind(params,'iid.stim','str')
    if success3
        [dowlparams.stimtype,dowlparams.signal]=parseDOWLfreqParam(temp);
    end
    success=success1&success2&success3;
elseif strncmp('abi',modulename,3)
    [dowlparams.itd,success1]=hashFind(params,'abi.itd','num');
    [dowlparams.iid,success2]=hashFind(params,'abi.iid','num');
    [temp,success3]=hashFind(params,'abi.stim','str')
    if success3
        [dowlparams.stimtype,dowlparams.signal]=parseDOWLfreqParam(temp);
    end
    success=success1&success2&success3;
elseif strncmp('fiid',modulename,4)
    [dowlparams.itd,success1]=hashFind(params,'fiid.itd','num');
    [temp1,success2]=hashFind(params,'fiid.side','str');
    [temp2,success3]=hashFind(params,'fiid.fixed','str');
    if success2&success3
        dowlparams.iid=strcat(temp1,temp2);
        success2=success2*success3;
    end
    [temp,success3]=hashFind(params,'fiid.stim','str');
    if success3
        [dowlparams.stimtype,dowlparams.signal]=parseDOWLfreqParam(temp);
    end
    success=success1&success2;
elseif strncmp('freq',modulename,4)
    dowlparams.stimtype=BEDSstimulusType('STIM_TONE');
    [dowlparams.itd,success1]=hashFind(params,'freq.itd','num');
    [dowlparams.iid,success2]=hashFind(params,'freq.iid','num');
    [dowlparams.abi,success3]=hashFind(params,'freq.abi','num');
    [dowlparams.mono,success4]=hashFind(params,'freq.mono','str');
    dowlparams.twosnd=[];
    success=success1&success2&success3&success4;
else
    error('Unsupported protocol for DOWL files.');
end
if ~success
    error('Unable to find other stimulus paramters!');
end

function [stimtype,signal]=parseDOWLfreqParam(rawstring)
gibber=findstr('stack=',rawstring);
if ~isempty(gibber)
    stimtype=BEDSstimulusType('STIM_STACK');
    signal=XDPHYSprocessRange(rawstring(7:end));
else
    gibber=findstr('tone=',rawstring);
    if ~isempty(gibber)
        stimtype=BEDSstimulusType('STIM_TONE');
        signal=str2num(rawstring(6:end));
    else
        if strncmp('BB',rawstring,2)
            stimtype=BEDSstimulusType('STIM_BB');
            signal=[];
        else
            stimtype=BEDSstimulusType('STIM_TONE');
            signal=str2num(rawstring);
        end
    end
end

function [stim_twosnd,stim_bc,stim_itd,stim_iid,stim_abi,stim_mono,stim_type,stim_signal]=assignDOWLparams(dowlparams,modulename,depvar)
stim_twosnd=0; stim_bc=100;
stim_itd=dowlparams.itd; stim_iid=dowlparams.iid;
stim_abi=dowlparams.abi; stim_mono=dowlparams.mono;
stim_type=dowlparams.stimtype; stim_signal=dowlparams.signal;
if strncmp('bf',modulename,2)
    stim_signal=depvar;
elseif strncmp('itd',modulename,3)
    stim_itd=depvar;
elseif strncmp('iid',modulename,3)
    stim_iid=depvar;
elseif strncmp('abi',modulename,3)
    stim_abi=depvar;
elseif strncmp('fiid',modulename,4)
    side=stim_iid(1);
    temp=str2num(stim_iid(2:end));
    if strncmp(side,'L',1)
        stim_iid=depvar-temp;
    elseif strncmp(side,'R',1)
        stim_iid=temp-depvar;
    else
        error('Unknown side value for FIID protocol');
    end
elseif strncmp('freq',modulename,4)
    stim_signal=depvar;
end

function [stim_type,stim_signal]=processXDPHYSfreqParam(stim_freq)
% Note that there are two equivalent ways to specify a
% narrowband noise: either by giving the extremes, or
% specifying the center frequency and the bandwidth.
if strncmp('/',stim_freq,1) | strncmp('.',stim_freq,1) ...
        | strncmp('~',stim_freq,1)
    stim_type=BEDSstimulusType('STIM_FILE');
    stim_signal=stim_freq;
elseif ~strncmp('BB',stim_freq,2)
    if strncmp('BP=',stim_freq,3)
        g=findstr('-',stim_freq);
        lower=str2num(stim_freq(4:g-1));
        upper=str2num(stim_freq(g+1:end));
        stim_type=BEDSstimulusType('STIM_NB');
        stim_signal=[min([lower upper]) max([lower upper])];
    elseif strncmp('CF',stim_freq,2)
        bwinit=strfind(stim_freq,'BW=');
        comma=findstr(stim_freq,',');
        cf=str2num(stim_freq(4:comma-1));
        bw=str2num(stim_freq(bwinit+3:end));
        stim_type=BEDSstimulusType('STIM_NB');
        stim_signal=[(cf-bw/2) (cf+bw/2)];
    elseif strncmp('NOSND',stim_freq,5)
        stim_type=BEDSstimulusType('STIM_SPONT');
        stim_signal=[];
    elseif strncmp('STACK=',stim_freq,6)
        stim_type=BEDSstimulusType('STIM_STACK');
        stim_signal=XDPHYSprocessRange(stim_freq(7:end));
    else
        stim_type=BEDSstimulusType('STIM_TONE');
        stim_signal=str2num(stim_freq);
    end
else
    stim_type=BEDSstimulusType('STIM_BB');
    stim_signal=[];
end
