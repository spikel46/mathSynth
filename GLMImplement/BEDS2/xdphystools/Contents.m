%  Tools for processing Xdphys files
%
%  readXDPHYSfile      Read an Xdphys file.
%  convertXDPHYSfiles  Batch convert Xdphys files to matfiles.
%  numspikes           Get the number of spikes in an XDPHYS file
%  XDPHYSprocessRange  Evaluate a XDPHYS-style range string

% RCS: $Id: Contents.m,v 1.4 2003/05/30 17:36:10 bjorn Exp $
