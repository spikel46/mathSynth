function true = structcmp(x,y)
% STRUCTCMP   Check that two structs are the same.
%
% STRUCTCMP(X,Y) will return 1 if X and Y are the same structure
% type (ie they can be put in the same array), and zero else.
  
% Written by GBC, 29 May 2000.
% RCS: $Id: structcmp.m,v 1.2 2001/03/20 05:13:48 bjorn Exp $

  if (~isstruct(x) | ~isstruct(y))
    true = 0;
    return;
  end
  
  namesX = fieldnames(x);
  namesY = fieldnames(y);
  if (length(namesX) ~= length(namesY))
    true = 0;
    return;
  end
  
  if strcmp(namesX,namesY)
    true = 1;
  else
    true = 0;
  end
  