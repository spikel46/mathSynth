function endString=skipTo(fid,targetString);
% SKIPTO        Skip through a file until reaching the
%               target line.
%
% E = SKIPTO(FID,TARGET) will advance through the text file
% pointed to by FID until a line beginning with the text
% in TARGET is found.  The matching line is returned in
% E.
%
% Note that advancing through the file is done by fgetl,
% so this will not work properly with binary files.

% Written by GBC, 01 Mar 2001 
% RCS: $Id: skipTo.m,v 1.2 2001/03/20 05:13:48 bjorn Exp $

len = length(targetString);
endString = fgetl(fid);
while (ischar(endString) & ~strncmp(endString, ...
				     targetString,len))
  endString = fgetl(fid);
end

if ~ischar(endString)
  endString = [];
end


