function bool = isint(x)
% ISINT    True if integer
%
% ISINT(X) returns 1 if X is an integer scalar and 0 else.

% RCS: $Id: isint.m,v 1.2 2001/03/20 05:13:48 bjorn Exp $

  if ischar(x)
    bool = 0;
  elseif (length(x) ~= length(1))
    bool = 0;
  elseif (round(x) ~= x)
    bool = 0;
  else 
    bool = 1;
  end
  