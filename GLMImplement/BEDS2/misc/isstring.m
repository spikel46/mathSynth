function true=isstring(x)
% ISSTRING   Check if a value is a scalar string.
%
% ISSTRING(X) returns 1 if X is a scalar string and 0 else. 
  
% Written by GBC, 29 May 2000.
% RCS: $Id: isstring.m,v 1.2 2001/03/20 05:13:48 bjorn Exp $

  if (~ischar(x))
    true = 0;
  else
    t = size(x);
    if (t(1) > 1)
      true = 0;
    else
      true = 1;
    end
  end
  
      
