% Miscellaneous Matlab tools.
%
% Type checking and relational operators.
%
%    isint          Check if a value is integer.
%    isstring       Check if a value is a string.
%    structcmp      See if two structures are the same type.
%
% File processing commands.
%
%    skipTo         Advance to given text in a file.

% RCS: $Id: Contents.m,v 1.2 2001/03/20 05:13:48 bjorn Exp $
