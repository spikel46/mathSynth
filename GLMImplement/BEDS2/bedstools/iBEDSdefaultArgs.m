function argvalue=iBEDSdefaultArgs(argname)

% THIS IS AN INTERNAL FUNCTION.  DO NOT CALL DIRECTLY.

% For consistency, use the same set of default arguments
% as much as possible.
  
% Written by GBC, 8 Jul 2003.
% RCS: $Id: iBEDSdefaultArgs.m,v 1.1 2003/07/08 22:55:20 bjorn Exp $

if strncmp(argname,'chan',4)
  argvalue='search';
elseif strncmp(argname,'trial',5)
  argvalue='all';
elseif strncmp(argname,'timewin',7)
  argvalue='default';
end
