function version = BEDSgetver
% BEDSgetver     Return the current version of the BEDS format.
%
% BEDSGETVER will return the current version of the BEDS data
% format.
  
% Written by GBC, 29 May 2000.
% RCS: $Id: BEDSgetver.m,v 1.4 2002/11/18 10:18:13 bjorn Exp $
  
% Version 2 was the initial release.  
%version = 2;  

% Version 3 replaced a raw string Stimulus field in the
% Trial structure with the Stimulus structure.
%version = 3;

% Version 4 added reading and storing saved stimuli 
% waveforms from xdphys file version 4 files.
version = 4;
