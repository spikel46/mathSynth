function stimulusType = BEDSstimulusType(typestr,pukeOnError)
% BEDSstimulusType Translate the stimulus type string to a stimulus type
%                  number.
%
%                  The stimulus type strings are these
%                    STIM_SPONT   (spontaneous, ie, no stimulus)
%                    STIM_TONE    (single tone)
%                    STIM_BEAT    (tones of different frequencies in each
%                                  ear)
%                    STIM_BB      (white noise)
%                    STIM_NB      (narrowband noise)
%                    STIM_STACK   (multiple tones)
%                    STIM_FILE    (stimulus read from a file)
%
% N = BEDSSTIMULUSTYPE('<type>') will return the associated number.
%
% TYPE = BEDSSTIMULUSTYPE(N) will return the string label
% associated with that number.
%
% ... = BEDSSTIMULUSTYPE(...,ERROR) does the same, but if
% ERROR is zero, then the function will return an empty
% array if the input is invalid, rather than quitting
% with an error.
%
% The necessary associated parameters for each type are:
%
%         STIM_SPONT:  None.  (empty vector)
%         STIM_TONE:   Frequency in Hz.  (scalar)
%         STIM_TONE:   Carrier and Envelope in Hz (2-element vector)
%         STIM_BB:     None.  (empty vector)
%         STIM_NB:     Bounds of the frequency range (2-element vector)
%         STIM_STACK:  Frequencies (in Hz) of all tones (n-tuple)
%         STIM_FILE:   Filename of stimulus (string)
%
% See also:  BEDS, BEDSmakeStimulus.

% Written by GBC, 13 Nov 2002.
% RCS: $Id: BEDSstimulusType.m,v 1.5 2005/02/03 02:08:37 bjorn Exp $

if nargin < 2; pukeOnError=1; end

% For safety's sake, check the type strings from
% longest to shortest, which means they won't be in
% numerical order.  The other half of the function (ie,
% number to type string) is in numerical order.
if ~isnumeric(typestr)
    if strcmp(typestr,'STIM_SPONT')
        stimulusType=0;
        return;
    end
    if strcmp(typestr,'STIM_STACK')
        stimulusType=4;
        return;
    end
    if strcmp(typestr,'STIM_TONE')
        stimulusType=1;
        return;
    end
    if strcmp(typestr,'STIM_FILE')
        stimulusType=5;
        return;
    end
    if strcmp(typestr,'STIM_BEAT')
        stimulusType=6;
        return
    end
    if strcmp(typestr,'STIM_BB')
        stimulusType=2;
        return;
    end
    if strcmp(typestr,'STIM_NB')
        stimulusType=3;
        return;
    end
    if pukeOnError
        error('unknown stimulus type');
    else
        stimulusType=[];
    end
    return;
else
    if typestr==0
        stimulusType='STIM_SPONT';
        return;
    end
    if typestr==1
        stimulusType='STIM_TONE';
        return;
    end
    if typestr==2
        stimulusType='STIM_BB';
        return;
    end
    if typestr==3
        stimulusType='STIM_NB';
    end
    if typestr==4
        stimulusType='STIM_STACK';
    end
    if typestr==5
        stimulusType='STIM_FILE';
    end
    if typestr==6
        stimulusType='STIM_BEAT';
    end
    if pukeOnError
        error('not a valid stimulus type')
    else
        stimulusType=[];
    end
end

