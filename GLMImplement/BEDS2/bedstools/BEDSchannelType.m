function channelType = BEDSchannelType(typestr)
% BEDSchannelType  Translate the channel type string to a channel type
%                  number.
%                  
%                  The channel type strings are these:
%                  CHAN_EMPTY  
%                  CHAN_SPIKES 
%                  CHAN_ANALOG 
%                  CHAN_SPIKES_ANALOG (= bitor(CHAN_SPIKES,CHAN_ANALOG))
%                  CHAN_STIM   
%
% N = BEDSCHANNELTYPE('<type>') will return the associated number.
%
% TYPE = BEDSCHANNELTYPE(N) will return the associated
% type string.
%
% See also:  BEDS.

% Written by cmalek, Aug 2002.
% RCS: $Id: BEDSchannelType.m,v 1.1 2002/11/18 10:18:12 bjorn Exp $

  if ~isnumeric(typestr)
	if strcmp(typestr,'CHAN_EMPTY')
	  channelType=0;
	  return;
	end
	if strcmp(typestr,'CHAN_SPIKES')
	  channelType=1;
	  return;
	end
	if strcmp(typestr,'CHAN_ANALOG')
	  channelType=2;
	  return;
	end
	if strcmp(typestr,'CHAN_SPIKES_ANALOG')
	  channelType=3;
	  return;
	end
	if strcmp(typestr,'CHAN_STIM')
	  channelType=4;
	  return;
	end
	error('unknown channel type');
  else
	if typestr==0
	  channelType='CHAN_EMPTY';
	  return
	end
	if typestr==1
	  channelType='CHAN_SPIKES';
	  return
	end
	if typestr==2
	  channelType='CHAN_ANALOG';
	  return
	end
	if typestr==3
	  channelType='CHAN_SPIKES_ANALOG';
	  return
	end
	if typestr==4
	  channelType='CHAN_STIM';
	  return
	end
	error('not a valid channel type')
  end
  

