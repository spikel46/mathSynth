function newdata=BEDSkeepTrials(olddata,trials)
% BEDSkeepTrials       Keep only listed trials
%
% NEW=BEDSKEEPTRIALS(DATA,TR) will return a data
% structure identical to DATA but preserving only the
% trials listed in TR.
%
% See also: BEDS, BEDSstripTrials.

% RCS: $Id: BEDSkeepTrials.m,v 1.1 2003/10/16 22:45:11 bjorn Exp $
% Written by GBC, 16 Oct 2003.

% Life don't get much easier than this.
newdata=olddata;
newdata.trial=newdata.trial(trials);