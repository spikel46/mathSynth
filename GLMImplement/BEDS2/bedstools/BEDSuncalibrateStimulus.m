function outdata=BEDSuncalibrateStimulus(data,calib,trial)
  
% BEDSuncalibrateStimulus    Remove calibration of saved stimuli
%
% FLAT=BEDSUNCALIBRATESTIMULUS(DATA,CAL) will remove
% compensation for speakers as described by the
% calibration CAL for the stimuli in the BEDS data
% structure DATA.  CAL can be either the calibration data
% itself, or a string containing the name of the
% calibration file to be used.  FLAT is a BEDS data
% structure identical to DATA, but with the compensated
% stimuli traces.
%
% FLAT=BEDSUNCALIBRATESTIMULUS(DATA,CAL,TR) allows you to
% specify a subset of trials to be processed.  FLAT will
% then only contain the subset of trials specified.
%
% Seee also: BEDS, XDPHYSremoveCalib.
  
% Written by GBC, 21 July 2003
% RCS: $Id: BEDSuncalibrateStimulus.m,v 1.2 2003/08/04 16:53:52 bjorn Exp $
% Depends on:  bedstools/isBEDS.m
%              bedstools/BEDSupdateToCurrent.m
%              bedstools/iBEDStrialChanSetup.m
%              misc/isstring.m
%              xdphystools/readXDPHYScalib.m
  
BEDSCOMPAT=4;
  
% Make sure we have a BEDS structure.
if ~isBEDS(data)
  error('DATA must be a BEDS protocol structure.');
end

if data.BEDSversion<BEDSCOMPAT
  data=BEDSupdateToCurrent(data);
end

% Set default argument values.
if (nargin < 2);error('Need CAL!');end;
if (nargin < 3);trial=iBEDSdefaultArgs('trial');end;
trial=iBEDStrialChanSetup(data,trial);

if isstring(calib)
  calib=readXDPHYScalib(calib);
end

% We need these values, to figure out what the "real"
% stimulus is (ie, delay:delay+duration), and what the
% bandpass limits of the generated signal were.
[epoch,duration,delay]=BEDSgetEpoch(data);
bandpass=[hashFind(data.params,'noise.low','num'), ...
		  hashFind(data.params,'noise.high','num')];

% Technically, you can vectorize the calibration removal,
% but it's not all that time efficient, it can crash if
% you have too many trials, missed samples mess
% everything up, and we need this loop anyways to lump
% everything into the output data structure.
%
% Because we need to modify/replace the channel
% structures containing the stimuli here, it's easiest
% not to bother with BEDSgetStimulus, even if it's
% technically bad behaviour.
outdata=data;
haveWarned=0;
for t=1:length(trial)
  % Find the channels with stimuli.
  stimchan=find([data.trial(t).channel.datatype]== ...
				BEDSchannelType('CHAN_STIM'));
  nonstim=find([data.trial(t).channel.datatype]~= ...
			   BEDSchannelType('CHAN_STIM'));
  outdata.trial(t).channel= ...
	  data.trial(t).channel(nonstim);
  numchannels=length(nonstim);
  for s=1:length(stimchan)
	this=data.trial(t).channel(stimchan(s));
	side=this.note;
	% If side isn't L or R, we don't know how to
    % calibrate it anyways.
	if strncmp(side,'?',1)
	  outdata.trial(t).channel(numchannels+s)=this;
	else
	  % Pick out the 'real' stimulus, calibrate it, and
      % then save the new channel
	  sr=this.waveformhz;
	  startIdx=round(delay/1000*sr);
	  stopIdx=round((duration+delay)/1000*sr);
	  stim=this.waveform;
	  if length(stim)<stopIdx
		if ~haveWarned
		  haveWarned=1;
		  warning('Some stimuli are too short');
		end
		stopIdx=length(stim);
	  end
	  temp=XDPHYSremoveCalib(calib,stim(startIdx:stopIdx), ...
							 sr,side,bandpass);
	  flatstim=[stim(1:startIdx-1)' temp' stim(stopIdx+1:end)'];
	  outdata.trial(t).channel(numchannels+s) = ...
		  BEDSmakeChannel(BEDSchannelType('CHAN_STIM'), [], ...
						  [], flatstim, this.tomv, this.offset, ...
						  sr,side);
	end
  end
end
