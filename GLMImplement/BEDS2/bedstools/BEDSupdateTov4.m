function convdata = BEDSupdateTov4(data);

% BEDSupdateTov4         Update a file to BEDS version 4.
%
% OUT = BEDSUPDATETOV3(DATA) will convert a BEDS format data
% file to BEDS version 4.  DATA can either be a BEDS data
% structure, or a pattern glob matching mat files containing
% a BEDS structure (in which case the file will be replaced
% with the new data).  OUT will return the converted data in
% the latter case, and the number of converted files in the
% former case.
%
% See also:  BEDS

% Written by GBC, 30 Sep 2001.
% RCS: $ID$

if isstruct(data)
  % Can't use isBEDS here, because we've changed what a Trial struct
  % looks like, and the new isBEDS fails on older BEDS format structs.
  % So we're just going to trust the user to give us a BEDS struct.
  convdata = do_conversion(data);
else
  filelist = dir(data);
  for n = 1:length(filelist);
    contents = load(filelist(n).name);
    dataname = fieldnames(contents);
    dataname = dataname{1};
    tempdata = getfield(contents,dataname);
    tempdata = do_conversion(tempdata);
    eval(sprintf('%s=tempdata;',dataname));
    save(filelist(n).name,dataname);
  end
  convdata = length(filelist);
end

function new = do_conversion(old)
if old.BEDSversion < 3
  old=BEDSupdateTov3(old);
elseif old.BEDSversion == 4
  return
elseif old.BEDSversion > 4
  error('More recent version than 4!')
end

new = old;
new.BEDSversion = 4;

for n=1:length(old.trial)
  oldstim=old.trial(n).stim;
  newstim=BEDSmakeStimulus;
  if oldstim.spont | oldstim.freq==-1
	new_type=BEDSstimulusType('STIM_SPONT');
	new_signal=[];
  elseif oldstim.freq==0
	new_type=BEDSstimulusType('STIM_BB');
	new_signal=[];
  else
	new_type=BEDSstimulusType('STIM_TONE');
	new_signal=oldstim.freq;
  end
  new.trial(n).stim=BEDSmakeStimulus(new_type, new_signal, oldstim.abi, ...
						   oldstim.itd, oldstim.iid, ...
						   oldstim.bc, oldstim.mono, ...
						   oldstim.twosnd, 0);
  
  oldchan=new.trial(n).channel;
  newchan=BEDSmakeChannel;
  for nn=1:length(oldchan)
	datatype=0;
	if ~isempty(oldchan(nn).spikes)
	  datatype=bitor(datatype,BEDSchannelType('CHAN_SPIKES'));
	end
	if ~isempty(oldchan(nn).waveform)
	  datatype=bitor(datatype,BEDSchannelType('CHAN_ANALOG'));
	end
	newchan(nn)=BEDSmakeChannel(datatype,oldchan(nn).spikes, ...
								oldchan(nn).spikestoms, ...
								oldchan(nn).waveform, ...
								oldchan(nn).tomv, ...
								oldchan(nn).offset, ...
								oldchan(nn).waveformhz, ...
								oldchan(nn).note);
  end
  new.trial(n).channel=newchan;
end
