function convdata = BEDSupdateTov3(data);

% BEDSupdateTov3         Update a file to BEDS version 3.
%
% OUT = BEDSUPDATETOV3(DATA) will convert a BEDS format data
% file to BEDS version 3.  DATA can either be a BEDS data
% structure, or a pattern glob matching mat files containing
% a BEDS structure (in which case the file will be replaced
% with the new data).  OUT will return the converted data in
% the latter case, and the number of converted files in the
% former case.
%
% See also:  BEDS

% Written by GBC, 30 Sep 2001.
% RCS: $ID$

if isstruct(data)
  convdata = do_conversion(data);
else
  filelist = dir(data);
  for n = 1:length(filelist);
    contents = load(filelist(n).name);
    dataname = fieldnames(contents);
    dataname = dataname{1};
    tempdata = getfield(contents,dataname);
    tempdata = do_conversion(tempdata);
    eval(sprintf('%s=tempdata;',dataname));
    save(filelist(n).name,dataname);
  end
  convdata = length(filelist);
end

function new = do_conversion(old);
new = old;
new.BEDSversion = 3;
if old.BEDSversion ~= 2
  error('This function for updating v2 to v3 only');
end
% Clicks need special processing.
[click_mono,success]=hashFind(new.params,'click.Side','str');
if success
  clickfile = 1;
else
  clickfile = 0;
end

for n=1:length(old.trial)
  if clickfile
    if (depvar == -6667)
      stim_spont = 1;
      stim_abi=0; stim_itd=0; stim_iid=0; stim_bc=0;
      stim_freq=0; stim_mono='S'; stim_twosnd=0;
    else
      stim_spont = 0;
      stim_abi=0; stim_itd=depvar; stim_iid=0; stim_bc=0;
      stim_freq=0; stim_mono=click_mono; stim_twosnd=0;
    end
  elseif (old.trial(n).depvar == -6666)
    stim_spont = 1;
    stim_abi=0; stim_itd=0; stim_iid=0; stim_bc=0;
    stim_freq=0; stim_mono='S'; stim_twosnd=0;
  else
    stimparam = old.trial(n).stim;
    stim_spont = 0;
    stim_begin = findstr(stimparam,'<');
    stim_end = findstr(stimparam,'>');
    stim_seps = findstr(stimparam,';');
    stim_abi = str2num(stimparam(stim_begin+1:stim_seps(1)-1));
    stim_itd = str2num(stimparam(stim_seps(1)+2:stim_seps(2)-1));
    stim_iid = str2num(stimparam(stim_seps(2)+2:stim_seps(3)-1));  
    stim_bc = str2num(stimparam(stim_seps(3)+2:stim_seps(4)-1));
    stim_freq = stimparam(stim_seps(4)+2:stim_seps(5)-1);
    if ~strncmp('BB',stim_freq,2)
      stim_freq=str2num(stim_freq);
      if strncmp('NOSND',stim_freq,5)
	stim_freq=-1;
      else
	stim_freq=str2num(stim_freq);
      end
    else
      stim_freq=0;
    end
    stim_mono = stimparam(stim_seps(5)+2:stim_seps(6)-1);
    stim_twosnd = str2num(stimparam(stim_seps(6)+2:stim_end-1));
  end
  new.trial(n).stim = BEDSmakeStimulusv3(stim_abi, stim_itd, ...
				       stim_iid, stim_bc, ...
				       stim_freq, stim_mono, ...
				       stim_twosnd, stim_spont);
  new.trial(n).BEDSversion = 3;
end
