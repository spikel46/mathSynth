function convdata = BEDSupdateToCurrent(data);

% BEDSupdateTovCurrent     Update a file to the most recent BEDS version.
%
% OUT = BEDSUPDATETOCURRENT(DATA) will convert a BEDS format
% data file to the most recent BEDS version.  DATA can
% either be a BEDS data structure, or a pattern glob
% matching mat files containing a BEDS structure (in which
% case the file will be replaced with the new data).  OUT
% will return the converted data in the latter case, and the
% number of converted files in the former case.
%
% See also:  BEDS

% Written by GBC, 16 Nov 2002.
% RCS: $ID$
% Depends on: bedstools/BEDSupdateTov4.m

%This is just a wrapper script.
  
  convdata=BEDSupdateTov4(data);
