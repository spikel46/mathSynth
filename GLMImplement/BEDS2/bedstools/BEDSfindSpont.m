function trials = BEDSfindSpont(data)

% BEDSfindSpont         Find spontaneous activity trials.
%
% TR= BEDSFINDSPONT(DATA) will return the indices of all
% spontaneous trials in the BEDS data file DATA.
%
% See also:  BEDS, BEDSfindStimParam

% RCS: $Id: BEDSfindSpont.m,v 1.1 2001/10/01 02:54:11 bjorn Exp $
% Written by GBC, 30 Sep 2001.
% Depends on:  bedstools/BEDSfindStimParam.m

trials = BEDSfindStimParam(data,'spont',1);