function depvar=BEDSgetDepvar(data,trial)

% BEDSgetDepvar    get depvar from a BEDS protocol
%                  structure.
%
% E = BEDSGETDEPVAR(DATA) will return the depvars of all
% the trials in the BEDS protocol structure DATA.
%
% E = BEDSGETDEPVAR(DATA,TRIAL) will return the depvars
% of the subset of trials specified in TRIAL.
%
% See also:  BEDS, BEDSfindDepvar

% Written by GBC, 22 Mar 2001.
% RCS: $Id: BEDSgetDepvar.m,v 1.2 2003/07/08 22:55:19 bjorn Exp $
% Depends on:  bedstools/isBEDS.m
%              bedstools/iBEDSdefaultArgs.m
%              bedstools/iBEDStrialChanSetup.m

% Verify we have a BEDS structure.
if (~isBEDS(data))
  error('DATA must be BEDS protocol structure');
end

% Make sure trial has a value and is one-dimensional.
if (nargin < 2); trial=iBEDSdefaultArgs('trial');end;
trial=iBEDStrialChanSetup(data,trial);

% Ain't this easy?
depvar = [data.trial(trial).depvar];
