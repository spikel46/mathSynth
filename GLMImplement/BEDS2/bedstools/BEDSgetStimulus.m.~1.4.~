function [waveform,saved,wavehz,side]=BEDSgetStimulus(data,trial)
% BEDSgetStimulus   get stimulus data from a BEDS protocol
%                   structure
%
% S=BEDSGETSTIMULUS(DATA) will retrieve the stimulus waveform 
% data of all trials and return it in a cell array.
%
% S=BEDSGETSTIMULUS(DATA,TRIAL) will retrieve stimulus waveform 
% data from the subset of trials specified in TRIAL.  Data
% from all trials will be returned if TRIAL is set to 'all'.
% Two channels will be returned per trial even if it is a monaural 
% trial.
%
% *NOTE* There is no relationship between the channel
% index of the stimuli and whether it was right or left
% stimuli.  BEDS/XDPHYS does not currently allow for that
% information to be stored.  
%
% [S,SAVED]=BEDSGETSTIMULUS(DATA)
% [S,SAVED]=BEDSGETSTIMULUS(DATA,TRIAL): S is the same cell array as
% in the examples above, while SAVED is a Nx1 boolean array, each element 
% indicating whether or not a saved stimulus was found for that trial.
%
% [S,SAVED,HZ]=BEDSGETSTIMULUS(DATA)
% [S,SAVED,HZ]=BEDSGETSTIMULUS(DATA,TRIAL): As above,
% with an NxM matrix HZ of the sampling rates for the
% stimuli.  (Sampling rate is defined as 0 if there was
% no stimuli).
%
% [S,SAVED,HZ,SIDE]=BEDSGETSTIMULUS(DATA)
% [S,SAVED,HZ,SIDE]=BEDSGETSTIMULUS(DATA,TRIAL): As above,
% with an NxM matrix HZ of the channel labels of the
% stimuli (i.e., either 'L', 'R', or '?').
%
% See also:  BEDS

% Written by GBC, 23 Mar 2001.
% RCS: $Id: BEDSgetStimulus.m,v 1.4 2003/08/04 16:53:52 bjorn Exp $
% Depends on:  bedstools/isBEDS.m
%              bedstools/iBEDSdefaultArgs.m
%              bedstools/iBEDStrialChanSetup.m
%              bedstools/BEDSchannelType.m
  
% Make sure we have a BEDS structure.
if ~isBEDS(data)
  error('DATA must be a BEDS protocol structure.');
end

% Make sure trial has a value and is one-dimensional.
if (nargin < 2); trial=iBEDSdefaultArgs('trial');end;
trial=iBEDStrialChanSetup(data,trial);

% Get all the bloody data.  I wish there was some way to
% do this other than a for loop.
waveform = {};
wavehz=[];
saved = [];
side=[];
for tr = 1:length(trial)
  saved(tr)=data.trial(trial(tr)).stim.saved;
  if saved(tr)
	ch = find([data.trial(trial(tr)).channel.datatype]== ...
				BEDSchannelType('CHAN_STIM'));
    for c=1:length(ch)
	  waveform{tr,c}=data.trial(trial(tr)).channel(ch(c)).waveform;
	  wavehz(tr,c)=data.trial(trial(tr)).channel(ch(c)).waveformhz;
	  side(tr,c)=data.trial(trial(tr)).channel(ch(c)).note;
	end
  else
	waveform{tr,:}=[];
	wavehz(tr,:)=0;
  end
end

