function [trial,channel]=iBEDStrialChanSetup(data,trial,channel,chantype)
  
% THIS IS AN INTERNAL FUNCTION.  DO NOT CALL DIRECTLY.

% Many functions have basically the same checking and
% processing of the 'trial' and 'chan' arguments.  This
% puts all that crap in one place. Note that not all
% functions care about channels, so if nargin < 3, we
% just skip all that.

% Written by GBC, 8 Jul 2003.
% RCS: $Id: iBEDStrialChanSetup.m,v 1.2 2003/09/23 00:18:10 bjorn Exp $
% Depends on: bedstools/isBEDS.m
%             bedstools/BEDSupdateToCurrent.m 
%             misc/isstring.m
  
BEDSCOMPAT=4;  
  
% Make sure we have a BEDS structure.
if ~isBEDS(data)
  error('DATA must be a BEDS protocol structure.');
end

if data.BEDSversion < BEDSCOMPAT
  data=BEDSupdateToCurrent(data);
end

% Provide cop-out for trials.
if isstring(trial)
  trial = 1:length(data.trial);
end

% Verify argument dimensions.
if (min(size(trial))~=1)
  error('TRIAL must be one-dimensional.');
end

if nargin > 2
  % If channel is 'search' (or any string, cuz I'm lazy),
  % then find all channels anywhere that store spikes.
  if isstring(channel)
	channel=[];
	for n=1:length(trial)
	  channel=unique([channel ...
					  find(bitand([data.trial(trial(n)).channel.datatype], ...
								  chantype))]); 
	  end
  end

  % Verify length of channel.
  if (min(size(channel))~=1)
	error('CHAN must be one-dimensional.');
  end
  if isempty(channel)
	error('CHAN cannot be empty.');
  end
end

