function channel = BEDSmakeChannel(datatype,spikes,spikestoms,waveform,...
                                   tomv, offset, waveformhz, note)
% BEDSmakeChannel   Make a channel entry for the BEDS format.
%
% C = BEDSMAKECHANNEL will return an electrode struture for
% use in the BEDS format with all values [].
%
% C = BEDSMAKECHANNEL(DATATYPE,SPIKE,SPIKESTOMS,WAVE,TOMV,OFFSET,WAVEFORMHZ,
% NOTE) will create a electrode structure for BEDS with spike train
% SPIKE (and a unit to ms conversion factor SPIKESTOMS), waveform WAVE (with
% sampling rate WAVEFORMHZ (in Hz!)), and note NOTE.
%
% The DATATYPE field describes what kind of data is saved in this channel
% struct, and is an integer.
%
% 0       No data
% 1       Spikes only
% 2       Analog data only
% 3       Spikes and Analog data ( 1 | 2 = 3 )
% 4       Stimulus 
%
% BEDSMAKECHANNEL(X) will return 1 if X is a BEDS channel
% structure and 0 else.  (Same as isBEDchannel.)
  
% Written by GBC,  26 May 2000.
% RCS: $Id: BEDSmakeChannel.m,v 1.5 2003/07/23 20:21:25 bjorn Exp $
% Depends on misc/structcmp.m
%            misc/isstring.m
  
% Current bugs:  
%    + in type checking part, we don't check for dimensionality,
%    and only check type for first element if input is an array.
  
  channel = struct('datatype',0,'spikes',[],'spikestoms',0,'waveform', ...
		   [],'tomv',0,'offset',0,'waveformhz',0,'note','');

  % Raw initialize.
  if (nargin == 8)
    channel.datatype=datatype;
	
	if (bitand(channel.datatype,BEDSchannelType('CHAN_SPIKES'))) 
      if (isnumeric(spikes))
        if (min(size(spikes)) > 1)
		  error('SPIKE must be one-dimensional.');
        else
		  channel.spikes = spikes;
        end
	  else
        error('SPIKE must be numeric.');
      end
  
      if (isnumeric(spikestoms))
        if (length(spikestoms) ~= 1)
		  error('SPIKESTOMS must be scalar.');
        else
		  channel.spikestoms = spikestoms;
        end
      else
        error('SPIKESTOMS must be numeric.')
      end
    end

    if (bitand(channel.datatype,BEDSchannelType('CHAN_ANALOG')) ...
		| bitand(channel.datatype,BEDSchannelType('CHAN_STIM')))  
      if (isnumeric(waveform))
        if (min(size(waveform)) > 1)
		  error('WAVE must be one-dimensional.');
        else
		  channel.waveform = waveform;
        end
      else
        error('WAVE must be numeric.');
      end
      
	  if (isnumeric(waveformhz))
        if (length(waveformhz) ~= 1)
		  error('WAVEFORMHZ must be scalar.');
        else
		  channel.waveformhz = waveformhz;
        end
      else
        error('WAVEFORMHZ must be numeric.');
      end
    end      

    if (bitand(channel.datatype,BEDSchannelType('CHAN_ANALOG')))
      if (isnumeric(tomv))
        if (min(size(tomv)) > 1)
		  error('TOMV must be one-dimensional.');
        else
		  channel.tomv = tomv;
        end
      else
        error('TOMV must be numeric.');
      end
  
      if (isnumeric(offset))
        if (min(size(offset)) > 1)
		  error('OFFSET must be one-dimensional.');
        else
		  channel.offset = offset;
        end
      else
        error('OFFSET must be numeric.');
      end
    else
      if bitand(channel.datatype,BEDSchannelType('CHAN_STIM'))
        channel.tomv = 1;
        channel.offset = 0;
      end
    end

    if (isstring(note))
      channel.note = note;
    else
      error('NOTE must be a string');
    end
    
  
  % Type checking.
  elseif (nargin == 1)
    x = datatype;
    if (~structcmp(x,channel))
      channel = 0;
      return
    end
	if (~isnumeric(x(1).datatype))
	  channel=0;
    elseif (~isnumeric(x(1).spikes))
      channel = 0;
    elseif (~isnumeric(x(1).waveform))
      channel = 0;
    elseif (~isnumeric(x(1).spikestoms))
      channel = 0;
    elseif (length(x(1).spikestoms) ~= 1)
      channel = 0;
    elseif (~isnumeric(x(1).waveformhz))
      channel = 0;
    elseif (length(x(1).waveformhz) ~= 1)
      channel = 0;
    elseif (~isstring(x.note))
      channel = 0;
    else
      channel = 1;
    end
    
  elseif (nargin ~= 0)
    error('Invalid number of parameters.');
  end
  
