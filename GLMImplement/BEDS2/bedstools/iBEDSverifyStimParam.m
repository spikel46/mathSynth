function valid = iBEDSverifyStimParam(data,param)

% iBEDSverifyStimParam       Check if stimulus parameter is valid
%
% This function is for internal use only. Do not call
% directly.

% RCS: $Id: iBEDSverifyStimParam.m,v 1.2 2003/09/23 00:18:10 bjorn Exp $
% Written by GBC, 1 Oct 2001.
% Depends on:  bedstools/isBEDS.m

% Very simple.  Just need to check if the given parameter
% matches one of the allowed fields.  The reason we take
% 'data' as an input is for possible version checking in
% the future.

if ~isBEDS(data)
  error('DATA must be BEDS structure');
end

% If param is numeric, then it should be one of the
% stimulus types.
if isnumeric(param)
  if isempty(BEDSstimulusType(param,0))
	valid = 0;
  else
	valid = 1;
  end
else
  % If param begins with STIM_, it might be a stimulus type.
  if strncmp(param,'STIM_',5)
	if isempty(BEDSstimulusType(param,0))
	  valid = 0;
	else
	  valid = 1;
	end
  % You can search on any of the fieldnames of the
  % stimulus structure
  elseif any(strcmp(fieldnames(BEDSmakeStimulus),param))
	valid=1;
  % The only other valid option is 'spont' or 'freq'.
  elseif strcmp(param,'spont') | strcmp(param,'freq')
	valid=1;
  else
	valid=0;
  end
end


