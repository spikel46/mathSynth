function note = BEDSgetNote(data);
% BEDSgetNote      Get a note from a BEDS protocol
%                  structure.
%
% N = BEDSGETNOTE(D) will get the notes from the BEDS
% protocol structure D.
%
% See also:  BEDS, BEDSaddNote

% Written by GBC, 22 Mar 2001.
% Depends on:  bedstools/isBEDS.m
% RCS: $Id: BEDSgetNote.m,v 1.1 2001/03/22 20:05:24 bjorn Exp $

if (~isBEDS(data))
  error('D must be a BEDS protocol structure.')
end

note = data.notes;