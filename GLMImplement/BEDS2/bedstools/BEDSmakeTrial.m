function trial = BEDSmakeTrial(animal,unit,filenum, ...
			       trialnum,protocol,depvar,stim, ...
			       channel)
  
% BEDSmakeTrial   Make a trial structure for BEDS.
%
% E = BEDSMAKETRIAL will return an empty trial structure
% for the BEDS data format.
%
% E = BEDSMAKETRIAL(A,C,F,T,PROT,DEPVAR,STIM,CHAN) will
% initialize a BEDS trial structure with animal ID A, cell
% number C, file number F, trial number T, protocol PORT,
% dependent variable DEPVAR, stimulus parameters STIM, 
% and channel data CHAN.
%
% E = BEDSMAKETRIAL(X) will return 1 if X is a valid BEDS
% trial structure and 0 else.  (Same as ISBEDSTRIAL.)
%
% See also BEDS, BEDSinit, BEDSmakeChannel, isBEDStrial.

% Written by GBC, 19 Mar 2001.
% RCS: $Id: BEDSmakeTrial.m,v 1.5 2002/11/18 10:18:13 bjorn Exp $
% Depends on bedstools/BEDSgetver.m
%            bedstools/BEDSmakeChannel.m
%            bedstools/BEDSmakeStimulus.m
%            misc/isint.m
%            misc/isstring.m

% This program is a big hefty brute, and is really two
% different programs (and in fact, it has two names:
% BEDSmakeTrial and isBEDStrial).  I've put it all in a
% single file so that when the format is changed, everything
% that needs to be changed is in a single place.

  % Make an "empty" structure.  If we don't enter into any
  % of the clauses below (ie, there were no parameters
  % passed to BEDSmakeTrial) then this is what will be
  % returned.
  trial = struct('BEDSversion',BEDSgetver,'animal',-1, ...
		 'unit',-1,'filenum',-1,'trialnum',-1, ...
		 'protocol','','depvar',0, ...
		 'stim',BEDSmakeStimulus,'channel', BEDSmakeChannel);
  
  numfields = length(fieldnames(trial))-1;
  
  % Doing an initialize with parameters.
  if (nargin == numfields)
    if (isint(animal))
      trial.animal = animal;
    else
      error('A must be an integer.');
    end
    if (isint(unit))
      trial.unit = unit;
    else
      error('C must be an integer.');
    end
    if (isint(filenum))
      trial.filenum = filenum;
    else
      error('F must be an integer.');
    end
    if (isint(trialnum))
      trial.trialnum = trialnum;
    else
      error('T must be an integer.');
    end
    if (isstring(protocol))
      trial.protocol = protocol;
    else
      error('PROT must be a string.');
    end
    if (isnumeric(depvar))
      trial.depvar = depvar;
    else
      error('DEPVAR must be numeric.')
    end
    if ~isempty(stim) & (length(stim)==1)
      if (BEDSmakeStimulus(stim))
		trial.stim = stim;
      else
		error('STIM must be a BEDS stimulus structure.');
      end
    else
      error('STIM must be a single, non-empty structure.');
    end
    if (min(size(channel))==1)
      if (BEDSmakeChannel(channel(1)))
		trial.channel = channel;
      else
		error('CHAN must be a BEDS channel structure.')
      end
    else
      error('CHAN must be a 1-D array and non-empty.')
    end
    
  % Doing type checking.
  elseif (nargin == 1)
    x = animal(1); %Might be more than one trial in array.
    if (~structcmp(x,trial))
      trial = 0;
      return
    end
    if (~isint(x.BEDSversion))
      trial = 0;
      return
    end
    if (~isint(x.animal))
      trial = 0;
      return
    end
    if (~isint(x.unit))
      trial = 0;
      return
    end
    if (~isint(x.filenum))
      trial = 0;
      return
    end
    if (~isint(x.trialnum))
      trial = 0;
      return
    end
    if (~isstring(x.protocol))
      trial = 0;
      return
    end
    if (~isnumeric(x.depvar))
      trial = 0;
      return
    end
    if (x.BEDSversion == 2)
      if (~isstring(x.stim))
	trial = 0;
	return
      end
    else
      if ~BEDSmakeStimulus(x.stim)
	trial = 0;
	return
      end
    end
    if (~BEDSmakeChannel(x.channel(1)))
      trial = 0;
      return
    end
    trial = 1;
    
    elseif (nargin ~= 0)
      error('Invalid number of parameters');
    end
  
      
  



