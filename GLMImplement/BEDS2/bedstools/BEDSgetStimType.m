function [type,params]=BEDSgetStimType(data,trial)
% BEDSgetStimType        Get the BEDS stimulus type from selected trials
%
% [T,P]=BEDSGETSTIMTYPE(DATA) will retrieve the stimulus
% types of all trials of the BEDS data structure DATA and
% place them in T, with the associated cell array P of
% parameters.
%
% [T,P]=BEDSGETSTIMTYPE(DATA,TRIAL) will do the same
% thing, but for the subset of trials TRIAL.
%
% For a definition of the types of stimuli, and the
% associated parameters, see the help for
% BEDSstimulusType.
%
% See also: BEDSstimulusType, BEDSfindStimType, BEDS.
 
% RCS: $Id: BEDSgetStimType.m,v 1.2 2003/07/08 22:55:20 bjorn Exp $
% Written by GBC, 30 Sep 2001.
% Depends on:  bedstools/BEDSupdateTov4.m
%              bedstools/iBEDSdefaultArgs.m
%              bedstools/iBEDStrialChanSetup.m
%              bedstools/isBEDS.m

% Minimum version of BEDS that will work with this
% script. 
BEDSCOMPAT = 4;

if ~isBEDS(data)
  error('DATA must be a valid BEDS structure');
end

% If we're below the current version, update for the
% meantime. 
if data.BEDSversion < BEDSCOMPAT
  data = BEDSupdateTov4(data);
end

% Make sure trial has a value and is one-dimensional.
if (nargin < 2); trial=iBEDSdefaultArgs('trial');end;
trial=iBEDStrialChanSetup(data,trial);

% The case of getting frequencies requires special
% handling.  Everything else is the same.
for tr = 1:length(trial)
  type(tr)=data.trial(trial(tr)).stim.type;
  params{tr}=data.trial(trial(tr)).stim.signal;
end
