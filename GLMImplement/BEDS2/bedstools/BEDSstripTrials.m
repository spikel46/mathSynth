function newdata=BEDSstripTrials(data,trials)
% BEDSstripTrials       Remove listed trials
%
% NEW=BEDSSTRIPTRIALS(DATA,TR) will return a data
% structure identical to DATA but preserving only the
% trials which are not listed in TR.
%
% See also: BEDS, BEDSkeepTrials.

% RCS: $Id: BEDSstripTrials.m,v 1.1 2003/10/16 22:45:12 bjorn Exp $
% Written by GBC, 16 Oct 2003.
% Depends on: bedstools/BEDSkeepTrials

% By definition, this function is the same as calling
% BEDSkeepTrials on the complement of trials.  So that's
% what we do -- that way, if other checks, changes,
% etc. need to be made, they only have to be made to
% BEDSkeepTrials.
  
keeptrials=setdiff(1:length(data.trial),trials);
newdata=BEDSkeepTrials(data,keeptrials);
