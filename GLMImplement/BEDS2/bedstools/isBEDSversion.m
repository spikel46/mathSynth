function true = isBEDSversion(x)
% isBEDSversion    Check to see if a value is a BEDS version structure.
%
% ISBEDSVERSION(X) will return 1 if X is a BEDS version structure and
% 0 else.
  
% Written by GBC, 29 May 2000.
% RCS: $Id: isBEDSversion.m,v 1.2 2001/03/20 04:59:54 bjorn Exp $
% Depends on bedstools/BEDSmakeVersion.m
  
  if (nargin == 1)
    true = BEDSmakeVersion(x);
  else
    error('Incorrect number of arguments')
  end
  