%  Tools for Bjorn's Excellent Data Structure.
%
%  Initialization functions:
%    BEDSinit          Create a BEDS protocol structure.
%    BEDSmakeVersion   Create a BEDS version structure.
%    BEDSmakeTrial     Create a BEDS trial structure.
%    BEDSmakeChannel   Create a BEDS channel structure.
%    BEDSmakeStimulus  Create a BEDs stimulus structure.
%
%  Access functions:
%    BEDSgetNote       Get notes in BEDS protocol structure.
%    BEDSstatus        Get basic info on BEDs data file.
%    BEDSaddNote       Add notes to a BEDS protocol stucture.
%    BEDSgetDepvar     Get the depvar of trials.
%    BEDSfindDepvar    Find trials matching depvar.
%    BEDSgetSpikes     Get spiking data from trials.
%    BEDSgetWaveform   Get waveform data from trials.
%    BEDSgetStimulus   Get saved stimuli waveforms from trials.
%    BEDSgetEpoch      Get epoch information.
%    BEDSfindSpont     Find spontaneous trials.
%    BEDSfindNonspont  Find non-spontaneous trials.
%    BEDSfindStimParam Find trials with certain stimulus paramater.
%    BEDSgetStimParam  Get stimulus parameter of trials.
%    BEDSfindStimType  Find trials with a certain stimulus type.
%    BEDSgetStimType   Get stimulus type of trials.
%
%  Miscellaneous tools:
%    BEDSload          Load file outputted by convertXDPHYSfiles.
%    BEDSgetver        Get current BEDS version.
%    BEDSchannelType   Handle channel type definitions.
%    BEDSstimulusType  Handle stimulus type definitions.
%    isBEDS            Check if a value is a BEDS structure.
%    isBEDSversion     Check if a value is a BEDS version structure.
%    isBEDStrial       Check if a value is a BEDS trial structure.
%    isBEDSchannel     Check if a value is a BEDS channel structure.
%    isBEDSstimulus    Check if a value is a BEDs stimulus structure.
%
%  Update routines:
%    BEDSupdateTov3    Convert v2 BEDS to v3.
%    BEDSupdateTov4    Converts to v4 BEDS.

% RCS: $Id: Contents.m,v 1.12 2003/06/25 18:34:30 bjorn Exp $
