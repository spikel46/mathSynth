function trials = BEDSfindNonspont(data)

% BEDSfindNonspont         Find data trials.
%
% TR= BEDSFINDSPONT(DATA) will return the indices of all
% non-spontaneous trials in the BEDS data file DATA.
%
% See also:  BEDS, BEDSfindSpont, BEDSfindStimParam

% RCS: $Id: BEDSfindNonspont.m,v 1.1 2001/10/04 00:58:13 bjorn Exp $
% Written by GBC, 30 Sep 2001.
% Depends on:  bedstools/BEDSfindStimParam.m

trials = BEDSfindStimParam(data,'spont',0);