function stimulus = BEDSmakeStimulusv3(abi,itd,iid,bc,freq,mono,twosnd,spont);
  
% THIS IS OLD AND OBSOLETE.  It is *only* present for
% compatability with BEDSupdateTov3.  NEVER EVER USE THIS.   
  
% BEDSmakeStimulus  Make a stimulus structure for BEDS.
%
% E = BEDSMAKESTIMULUS will return an empty trial structure
% for the BEDS data format.
%
% E = BEDSMAKESTIMULUS(A,T,I,B,F,M,Tw,S) will
% initialize a BEDS stimulus structure with abi A, itd T,
% iid I, bc B, freq F, mono M, twosnd Tw, and spontaneous S.
%
% E = BEDSMAKESTIMULUS(X) will return 1 if X is a valid BEDS
% trial structure and 0 else.  (Same as ISBEDSstimulus.)
%
  
% See also BEDS, BEDSinit, BEDSmakeTrial, isBEDSstimulus.

% Written by GBC, 30 Sep 2001.
% RCS: $Id: BEDSmakeStimulusv3.m,v 1.1 2003/03/08 09:03:15 bjorn Exp $
% Depends on:  misc/isstring.m

% This program is a big hefty brute, and is really two
% different programs (and in fact, it has two names:
% BEDSmakeStimulus and isBEDSstimulus).  I've put it all in a
% single file so that when the format is changed, everything
% that needs to be changed is in a single place.

  % Make an "empty" structure.  If we don't enter into any
  % of the clauses below (ie, there were no parameters
  % passed to BEDSmakeStimulus) then this is what will be
  % returned.
  stimulus = struct('abi',[],'itd',[],'iid',[],'bc',[], ...
		    'freq',[], 'mono','','twosnd',[],'spont',[]);
  
  numfields = length(fieldnames(stimulus));

  % Doing an initialize with parameters.
  if (nargin == numfields)
    if isnumeric(spont);
      if (max(size(spont)) == 1 & ~isempty(spont))
	stimulus.spont = spont;
      else
	error('SPONT must be scalar and non-empty');
      end
    else
      error('SPONT must be numeric');
    end
    if isnumeric(abi);
      if (~(max(size(abi)) > 1) & (~isempty(abi) | spont))
	stimulus.abi = abi;
      else
	error('ABI must be scalar and non-empty');
      end
    else
      error('ABI must be numeric');
    end
    if isnumeric(itd);
      if (~(max(size(itd)) > 1) & (~isempty(itd) | spont))
	stimulus.itd = itd;
      else
	error('ITD must be scalar and non-empty');
      end
    else
      error('ITD must be numeric');
    end
    if isnumeric(iid);
      if (~(max(size(iid)) > 1) & (~isempty(iid) | spont))
	stimulus.iid = iid;
      else
	error('IID must be scalar and non-empty');
      end
    else
      error('IID must be numeric');
    end
    if isnumeric(bc);
      if (~(max(size(bc)) > 1) & (~isempty(bc) | spont))
	stimulus.bc = bc;
      else
	error('BC must be scalar and non-empty');
      end
    else
      error('BC must be numeric');
    end
    if isnumeric(freq);
      if (~(max(size(freq)) > 1) & (~isempty(freq) | spont))
	stimulus.freq = freq;
      else
	error('FREQ must be scalar and non-empty');
      end
    else
      error('FREQ must be numeric.');
    end
    if isstring(mono)
      stimulus.mono = mono;
    else
      error('MONO must be a string');
    end
    if isnumeric(twosnd);
      if (~(max(size(twosnd)) > 1) & (~isempty(twosnd) | spont))
	stimulus.twosnd = twosnd;
      else
	error('TWOSND must be scalar and non-empty');
      end
    else
      error('TWOSND must be numeric');
    end
      
    
  % Doing type checking.
  elseif (nargin == 1)
    x = abi;
    if (~structcmp(x,stimulus))
      stimulus = 0;
      return
    end
    if (~isnumeric(x.abi))
      stimulus = 0;
      return
    end
    if (~isnumeric(x.itd))
      stimulus = 0;
      return
    end
    if (~isnumeric(x.iid))
      stimulus = 0;
      return
    end
    if (~isnumeric(x.bc))
      stimulus = 0;
      return
    end
    if (~isnumeric(x.freq))
      if (~isstring(x.freq))
	stimulus = 0;
	return
      end
    end
    if (~isstring(x.mono))
      stimulus = 0;
      return
    end
    if (~isnumeric(x.twosnd))
      stimulus = 0;
      return
    end
    if (~isnumeric(x.spont))
      stimulus = 0;
      return
    end
    stimulus = 1;
    
    elseif (nargin ~= 0)
      error('Invalid number of parameters');
    end
  
      
  



