function [epoch,duration,delay]=BEDSgetEpoch(data)
% BEDSgetEpoch          get epoch information
%
% [E,DU,DL]=BEDSGETEPOCH(DATA) will return the epoch
% duration E, the stimulus duration DU, and the stimulus
% onset DL, as listed in the BEDS datafile DATA.
%
% See also: BEDS.

% RCS: $Id: BEDSgetEpoch.m,v 1.1 2001/08/29 23:58:09 bjorn Exp $
% Written by GBC, 29 Aug 2001.
% Depends on:  hashtable/hashFind.m

[epoch,success1] = hashFind(data.params,'epoch','num');
[duration,success2] = hashFind(data.params,'dur','num');
[delay,success3] = hashFind(data.params,'delay','num');

if ~(success1 & success2 & success3)
  error('Unable to find stimulus time parameters in DATA');
end
