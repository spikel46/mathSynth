function timewin=iBEDScheckTimewin(data,timewin,default)

% THIS IS AN INTERNAL FUNCTION.  DO NOT CALL DIRECTLY.

% Sanity checks for time window values.  Note that there
% are two possibilities for a 'default' case.

% Written by GBC, 8 Jul 2003.
% RCS: $Id: iBEDScheckTimewin.m,v 1.1 2003/07/08 22:55:20 bjorn Exp $
% Depends on: bedstools/BEDSgetEpoch.m
%             misc/isstring.m
  

[epoch,duration,delay]=BEDSgetEpoch(data);

if (isstring(timewin))
  if strcmp(default,'fulltrial')
	timewin = [0 epoch];
  elseif strcmp(default,'dataonly')
	timewin = [delay delay+duration];
  elseif strcmp(default,'spont')
	timewin = [0 delay];
  else
	error('Undefined default timewindow');
  end
end
if (length(timewin) == 1)
  if ((delay + timewin) > epoch)
	error('Specified duration exceeds epoch')
  else
	timewin = [delay delay+timewin];
  end
elseif ((length(timewin)==2)&(min(size(timewin))==1))
  if timewin(1) < 0
	error(['Specified start of timewindow must be' ...
		   ' non-negative.'])
  elseif timewin(2) > epoch
	error('Specified timewindow exceeds epoch');
  end
else
  error('TIME must be [starttime endtime]');
end
