function trials = BEDSfindStimType(data,type,param);

% BEDSfindStimType   Find trials with given stimulus type
%
% T = BEDSFINDSTIMTYPE(DATA,TYPE) will return the
% indices of trials with stimulus type TYPE.  TYPE can be
% either in the numerical or string format.  (See
% BEDSstimulusType.)
%
% T = BEDSFINDSTIMTYPE(DATA,TYPE,PARAM) will additionally
% filter to match all trials who have a type of TYPE and
% corresponding parameters PARAM.  Note that if TYPE is
% one that has no parameters, then all trials matching
% TYPE will be returned without considering PARAM.
%
% See also:  BEDS, BEDSstimulusType, BEDSgetStimType.

% RCS: $Id: BEDSfindStimType.m,v 1.1 2003/06/25 18:37:08 bjorn Exp $
% Written by GBC, 30 Sep 2001.
% Depends on:  bedstools/BEDSfindStimParam.m
%              bedstools/BEDSstimulusType.m
%              misc/isstring.m
  
  if isstring(type)
	type=BEDSstimulusType(type);
  end
  
  %  BEDSfindStimParam can do most of the (easy) work for us.
  trials=BEDSfindStimParam(data,'type',type);
  
  % If the stim type is SPONT or BB, then there are no
  % params, and so we declare a trivial match.
  % Otherwise, check all the parameters.  We have to use
  % a loop (rather than find) because of the nested
  % structures, and we explicitly check the lengths, in
  % case the number of tones in a stack ever varies.
  if nargin == 3
	param=sort(param);
	if type == BEDSstimulusType('STIM_SPONT') | ...
	   type == BEDSstimulusType('STIM_BB');
	  return;
	else
	  paramSize=size(param);
	  paramLen=max(paramSize);
	  if min(paramSize)~=1
		error('Matrix or empty PARAMs are not valid');
	  else
		newtrials=[];
		for n=1:length(trials)
		  if (length(data.trial(trials(n)).stim.signal)) == paramLen & ...
				(prod(sort(data.trial(trials(n)).stim.signal)==param))
			newtrials=[newtrials trials(n)];
		  end
		end
		trials=newtrials;
	  end
	end
  end
  
  
  