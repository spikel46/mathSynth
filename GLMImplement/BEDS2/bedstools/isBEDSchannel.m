function true = isBEDSchannel(x)
% isBEDSchannel    Check to see if a value is a BEDS channel structure.
%
% isBEDSchannel(X) will return 1 if X is a BEDS channel structure
% and 0 else.
  
% Written by GBC, 29 May 2000.
% RCS: $Id: isBEDSchannel.m,v 1.1 2001/03/22 03:05:04 bjorn Exp $

% Depends on bedstools/BEDSmakeChannel.m
  
  if (nargin == 1)
    true = BEDSmakeChannel(x);
  else
    error('Incorrect number of arguments');
  end
  