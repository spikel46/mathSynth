function version = BEDSmakeVersion(program,number,major,minor)
% BEDSmakeVersion   Make a version entry for the BEDS format.
%
% VERSION = BEDSMAKEVERSION will return a VERSION struture for
% use in the BEDS format with all values either 0 or ''.
%
% VERSION = BEDSMAKEVERSION(PROG,NUM,MAJ,MIN) will create a
% version structure for BEDS with program name PROG, version number
% NUM, major number NUM, and minor number MIN.
%
% BEDSMAKEVERSION(X) will return 1 if X is a BEDS version structure
% and 0 else.  (Same as isBEDSversion.)
  
% Written by GBC,  26 May 2000.
% RCS:  $Id: BEDSmakeVersion.m,v 1.2 2001/03/20 04:59:54 bjorn Exp $
% Depends on:  misc/isint.m
%              misc/isstring.m
%              misc/structcmp.m
  
  version = struct('program','','number',0,'major',0,'minor',0);
  
  % Raw initialize.
  if (nargin == 4)
    if (isstring(program))
      version.program = program;
    else
      error('PROG must be a string.');
    end
    if (isint(number))
      version.number = number;
    else
      error('NUM must be integer.');
    end
    if (isint(major))
      version.major = major;
    else
      error('MAJ must be integer.');
    end
    if (isint(minor))
      version.minor = minor;
    else
      error('MIN must be integer.');
    end
    
  % Type checking.
  elseif (nargin == 1)
    x = program;
    if (~structcmp(version,x))
      version = 0;
      return
    end
    if (~isstring(x.program))
      version = 0;
      return
    end
    if (~isint(x.number))
      version = 0;
      return
    end
    if (~isint(x.major))
      version = 0;
      return
    end
    if (~isint(x.minor))
      version = 0;
      return
    end
    version = 1;
  elseif (nargin ~= 0)
    error('Invalid number of parameters.');
  end
  