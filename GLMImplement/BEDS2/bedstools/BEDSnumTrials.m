function numTrials = BEDSnumTrials(data)
% BEDSnumTrials	   Return the number of trials in a BEDS data file
%
% N = BEDSNUMTRIALS(D) will return the number of trials N
% in a BEDS data file D.
%
% See also:  BEDS.

% Written by GBC, 12 Sep 2001.
% RCS: $Id: BEDSnumTrials.m,v 1.1 2001/09/12 23:19:19 bjorn Exp $

numTrials = length(data.trial);
