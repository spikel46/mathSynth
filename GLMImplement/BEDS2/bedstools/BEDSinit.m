function element = BEDSinit(version,animal,unit,filenum, ...
			    timestamp,params,notes,trial)
  
% BEDSinit   Make a protocol data structure for BEDS.
%
% E = BEDSINIT will return a BEDS protocol structure with all values
% empty.
%
% E = BEDSINIT(VER,A,C,F,TIME,PARAMS,NOTES,TRIAL) will
% return a BEDS element with version VER, animal ID A, cell
% numer C, file number F, timestamp TIME, parameters PARAMS,
% general notes NOTES, and trial array TRIAL.
%
% BEDSINIT(X) will return 1 if X is a BEDS structure and 0
% else.  (Same as isBEDS.)
%
% See also BEDS, BEDSmakeVersion, BEDSmakeTrial.

% Written by GBC, 26 May 2000.
% RCS: $Id: BEDSinit.m,v 1.3 2002/11/18 10:18:13 bjorn Exp $
% Depends on bedstools/BEDSmakeVersion.m
%            bedstools/BEDSmakeTrial.m
%            bedstools/BEDSgetver.m
%            misc/isint.m
%            misc/isstring.m

% This program is a big hefty brute, and is really two
% different programs (and in fact, it has two names:
% BEDSinit and isBEDS).  I've put it all in a single file so
% that when the format is changed, everything that needs to
% be changed is in a single place.

  % Make an "empty" structure.  If we don't enter into any
  % of the clauses below (ie, there were no parameters
  % passed to BEDSinit) then this is what will be returned.
  element = struct('BEDSversion',BEDSgetver,'version', ...
		   BEDSmakeVersion, 'animal',-1,'unit',- ...
		   1,'filenum',-1,'timestamp','', 'params',[], ...
		   'notes','','trial', BEDSmakeTrial);
		   
  
  numfields = length(fieldnames(element))-1;
  
  % Doing an initialize with parameters.
  if (nargin == numfields)
    if ((length(version)==1) & (BEDSmakeVersion(version)))
      element.version = version;
    else
      error('VER must be a scalar BEDS version structure.');
    end
    if (isint(animal))
      element.animal = animal;
    else
      error('A must be an integer.');
    end
    if (isint(unit))
      element.unit = unit;
    else
      error('C must be an integer.');
    end
    if (isint(filenum))
      element.filenum = filenum;
    else
      error('F must be an integer.');
    end
    if (isstring(timestamp))
      element.timestamp = timestamp;
    else
      error('TIME must be a string.');
    end
    % We do no type checking on params.  However people
    % want to store it is fine by us.
    element.params = params;
    if (isstring(notes))
      element.notes = notes;
    else
      error('NOTES must be a string.');
    end
    if (BEDSmakeTrial(trial))
      element.trial = trial;
    else
      error('TRIAL must be a BEDS trial structure array.');
    end
    
  % Doing type checking.
  elseif (nargin == 1)
    x = version;
    if (~structcmp(x,element))
      element = 0;
      return
    end
    if (~isint(x.BEDSversion))
      element = 0;
      return
    end
    if ((length(x.version)~=1) | (~BEDSmakeVersion(x.version)))
      element = 0;
      return
    end
    if (~isint(x.animal))
      element = 0;
      return
    end
    if (~isint(x.unit))
      element = 0;
      return
    end
    if (~isint(x.filenum))
      element = 0;
      return
    end
    if (~isstring(x.timestamp))
      element = 0;
      return
    end
    % No type checking on params.
    if (~isstring(x.notes))
      element = 0;
      return
    end
    if (~BEDSmakeTrial(x.trial))
      element = 0;
      return
    end
    element = 1;
    
  elseif (nargin ~= 0)
    error('Invalid number of parameters');
  end
  
      
  



