function true = isBEDS(x)
% isBEDS    Check to see if a value is a BEDS structure.
%
% ISBEDS(X) will return 1 if X is a BEDS structure and 0 else.
  
% Written by GBC, 29 May 2000.
% RCS: $Id: isBEDS.m,v 1.3 2002/11/18 10:18:14 bjorn Exp $
% Depends on bedstools/BEDSinit.m
%            bedstools/BEDSupdateToCurrent.m
  
% A failure might just mean that the data file in
% question is an older version of BEDS.  If that's the
% case, then try updating it to the most recent version
% before declaring failure.
  
  if (nargin == 1)
    true = BEDSinit(x);
	if ~true
	  true=BEDSinit(BEDSupdateToCurrent(x));
	end
  else
    error('Incorrect of arguments.')
  end
  