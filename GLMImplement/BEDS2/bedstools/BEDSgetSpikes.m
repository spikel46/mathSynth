function spikes=BEDSgetSpikes(data,trial,channel,timewin,noconv)
% BEDSgetSpikes   get spike data from a BEDS protocol
%                 structure
%
% S=BEDSGETSPIKES(DATA) will retrieve the spike data (in any
% channel, during the stimulus period) of all trials and
% return it in a cell array.
%
% S=BEDSGETSPIKES(DATA,TRIAL) will retrieve spike data
% from the subset of trials specified in TRIAL.  Data
% from all trials will be returned if TRIAL is set to
% 'all'. 
%
% S=BEDSGETSPIKES(DATA,TRIAL,CHAN) will do the same, but
% retrieving from channels CHAN, where CHAN is a vector
% list of desired channels.  If CHAN is non-scalar, then
% the data will be returned in a two-dimensional cell
% array, with the first index being trial and the second
% being channel.  If CHAN is 'search', then all
% spike-bearing channels for each trial will be returned
% automatically (this is the default behaviour).
%
% S=BEDSGETSPIKES(...,TIMEWIN) will do the same, but only
% return spikes in the time window TIMEWIN.  TIMEWIN can be
% scalar, in which case the spikes between stimulus onset
% and stimulus onset plus TIMEWIN will be considered; or a
% vector of the form [lowertime uppertime].
%
% Spike times are returned in milliseconds.
%
% See also:  BEDS

% Written by GBC, 22 Mar 2001.
% RCS: $Id: BEDSgetSpikes.m,v 1.18 2005/01/06 07:11:30 bjorn Exp $
% Depends on:  bedstools/isBEDS.m
%              bedstools/iBEDSdefaultArgs.m
%              bedstools/iBEDStrialChanSetup.m
%              bedstools/iBEDScheckTimewin.m
%              bedstools/BEDSgetEpoch.m
%              bedstools/BEDSupdateToCurrent.m
%              bedstools/BEDSchannelType.m
%              misc/isstring.m
  
BEDSCOMPAT=4;  
  
% Make sure we have a BEDS structure.
if ~isBEDS(data)
  error('DATA must be a BEDS protocol structure.');
end

if data.BEDSversion<BEDSCOMPAT
  data=BEDSupdateToCurrent(data);
end

% Set default argument values.
if (nargin < 2);trial=iBEDSdefaultArgs('trial');end;
if (nargin < 3);channel=iBEDSdefaultArgs('chan');end;
if (nargin < 4);timewin=iBEDSdefaultArgs('timewin');end;
if (nargin < 5);noconv=0;end;

% Sanity checks.
[trial,channel]=iBEDStrialChanSetup(data,trial,channel,...
									BEDSchannelType('CHAN_SPIKES'));
timewin=iBEDScheckTimewin(data,timewin,'dataonly');

% Get all the bloody data.  I wish there was some way to
% do this other than a for loop.
spikes = {};
sconv = [];
n = 0;
for tr = trial
  nn = 0;
  n = n + 1;
  for ch = channel
    nn = nn + 1;
	if bitand(data.trial(tr).channel(ch).datatype, ...
			  BEDSchannelType('CHAN_SPIKES'))
	  spikes{n,nn}=data.trial(tr).channel(ch).spikes;
	  conv = data.trial(tr).channel(ch).spikestoms;
	  if noconv
		temp = timewin;
		timewin = timewin/conv;
	  else
		spikes{n,nn}=spikes{n,nn}.*conv;
	  end
	  spikes{n,nn} = spikes{n,nn}(find((timewin(1) < spikes{n,nn}) ...
									   & (timewin(2) > spikes{n,nn})));
	  if noconv
		timewin = temp;
	  end
	else
	  spikes{n,nn}=[];
	  end
  end
end








