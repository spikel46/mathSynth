function [waveform,wavehz,waveconv]=BEDSgetWaveform(data,trial,channel,unitconv)
% BEDSgetWaveform   get waveform data from a BEDS protocol
%                   structure
%
% S=BEDSGETWAVEFORM(DATA) will retrieve the waveform data
% from all analog channels of all trials and return it in a
% cell array.
%
% S=BEDSGETWAVEFORM(DATA,TRIAL) will retrieve waveform data
% from the subset of trials specified in TRIAL.  Data
% from all trials will be returned if TRIAL is set to 'all'.
%
% S=BEDSGETWAVEFORM(DATA,TRIAL,CHAN) will do the same, but
% retrieving from channels CHAN, where CHAN is a vector
% list of desired channels.  If CHAN is non-scalar, then
% the data will be returned in a two-dimensional cell
% array, with the first index being trial and the second
% being channel.  If CHAN is 'search', then all
% spike-bearing channels for each trial will be returned
% automatically (this is the default behaviour).
%
% S=BEDSGETWAVEFORM(...,UNITCONV) will do the same, but
% allows the user to specify unit conversion.  If
% UNITCONV is 1, the returned waveforms will
% automatically be converted into mV; if it is 0, it will
% be returned in whatever the internally stored format is
% (probably ticks, but no guarantees).  The unit
% conversion can be very expensive computationally, so be
% warned.  By default, UNITCONV is 1.
%
% [S,SRATE]=BEDSGETWAVEFORM(...) will do the same, but also
% return the sampling rate SRATE, one for each waveform
% returned. 
%
% [S,SRATE,SCONV]=BEDSGETWAVEFORM(...) will do the same,
% but the multiplicative factor and offset the internally
% stored format to mV (ie, mV = a * tic + offset) will be
% returned in the matrix SCONV, one row per waveform,
% with first column a and second column offset.
%
% See also:  BEDS

% Written by GBC, 23 Mar 2001.
% RCS: $Id: BEDSgetWaveform.m,v 1.10 2003/08/04 16:53:52 bjorn Exp $
% Depends on:  bedstools/isBEDS.m
%              bedstools/iBEDSdefaultArgs.m
%              bedstools/iBEDStrialChanSetup.m
%              bedstools/BEDSupdateToCurrent.m
%              bedstools/BEDSchannelType.m
%              misc/isstring.m
  
BEDSCOMPAT=4;  
  
% Make sure we have a BEDS structure.
if ~isBEDS(data)
  error('DATA must be a BEDS protocol structure.');
end

if data.BEDSversion < BEDSCOMPAT
  data=BEDSupdateToCurrent(data)
end

% Set default argument values.
if (nargin < 2);trial=iBEDSdefaultArgs('trial');end;
if (nargin < 3);channel=iBEDSdefaultArgs('chan');end;
if (nargin < 4);unitconv=1;end;

[trial,channel]=iBEDStrialChanSetup(data,trial,channel,...
									BEDSchannelType('CHAN_ANALOG'));


% Get all the bloody data.  I wish there was some way to
% do this other than a for loop.
waveform = {};
wavehz = [];
waveconv = zeros(length(trial),2);
n = 0;
for tr = trial
  nn = 0;
  n = n + 1;
  for ch = channel
    nn = nn + 1;
	if bitand(data.trial(tr).channel(ch).datatype, ...
			  BEDSchannelType('CHAN_ANALOG'))
	  waveform{n,nn}=data.trial(tr).channel(ch).waveform;
	  waveconv(n,1,nn)=data.trial(tr).channel(ch).tomv;
	  waveconv(n,2,nn)=data.trial(tr).channel(ch).offset;
	  if (unitconv)
		waveform{n,nn}=waveform{n,nn}*waveconv(n,1,nn)+waveconv(n,2,nn);
	  end
	  wavehz(n,nn)=data.trial(tr).channel(ch).waveformhz;
	else
	  waveform{n,nn}=[];
	end
  end
end

% XDPHYS allows a software reduction of sampling rate.
% For simplicity of the user, compensate for that here.
[anaDecimate,success]=hashFind(data.params,'ana-decimate','num');
if success
  wavehz=wavehz./anaDecimate;
end
