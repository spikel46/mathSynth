function data = BEDSaddNote(data,note) 

% BEDSgetNote         Add a note to a BEDS protocol structure.
%
% E = BEDSGETNOTE(D,NOTE) will add the note NOTE to the
% BEDS protocol structure D, replacing whatever note was
% already in D.
%
% See also:  BEDS, BEDSgetNote

% Written by GBC, 22 Mar 2001.
% Depends on: misc/isstring.m
%             bedstools/isBEDS.m
% RCS: $Id: BEDSaddNote.m,v 1.2 2002/11/18 10:18:12 bjorn Exp $

  
if (~isBEDS(data)
  error('D must be a BEDS protocol structure.');
end
if (~isstring(note))
  error('NOTE must be a string.')
end

data.notes = note;


