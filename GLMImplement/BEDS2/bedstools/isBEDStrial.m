function true = isBEDStrial(x)
% isBEDStrial    Check to see if a value is a BEDS trial structure.
%
% isBEDStrial(X) will return 1 if X is a BEDS trial structure
% and 0 else.
  
% Written by GBC, 29 May 2000.
% RCS: $Id: isBEDStrial.m,v 1.2 2001/03/20 04:59:54 bjorn Exp $

% Depends on bedstools/BEDSmakeTrial.m
  
  if (nargin == 1)
    true = BEDSmakeTrial(x);
  else
    error('Incorrect number of arguments');
  end
  