function trials = BEDSfindStimParam(data,param,value);

% BEDSfindStimParam   Find trials with given stimulus parameter
%
% T = BEDSFINDSTIMPARAM(DATA,PARAM,VAL) will return the
% indices of trials with stimulus paramter PARAM matching
% value VAL in the BEDS data file DATA.  PARAM can be any
% of the fields of the BEDS stimulus structure (see the
% BEDS help file), 'freq', 'spont', or one of the valid
% stimulus types (see BEDSstimulusType).
%
% A couple of important notes:
%    1)  If the param is 'freq', then legitimate values
%    are a positive number (corresponding to a frequency
%    of a tone), 'BB' (for broadband noise), or 'NB' (for
%    narrowband noise).
%
%    2)  If the param is 'type', then legitimate values
%    are those produced by BEDSstimulusType (either
%    string or numerical format).
%
%    3)  If the param is a stimulus type (ie, STIM_TONE),
%    then VAL should be an appropriate parameter for that
%    stimulus type (see BEDSstimulusType).  This option
%    is useful for when you have signals with different
%    bandpasses used in a single data-set, for example.
%    
%    4)  If the trial was spontaneous (ie 'spont' is 1),
%    then no guarantees are made as to the settings of
%    the other parameters.
%
%    5)  If the stimulus was NOSND, the trial will be
%    treated as spontaneous.  This is a difference from
%    the behaviour of BEDS prior to version 4.
%
% Note that there is some overlap -- there are, for
% example, multiple ways to get both broadband noise
% trials or spontaneous trials.
%
% See also:  BEDS, BEDSgetStimParam.

% RCS: $Id: BEDSfindStimParam.m,v 1.8 2003/09/23 00:18:09 bjorn Exp $
% Written by GBC, 30 Sep 2001.
% Depends on:  bedstools/BEDSupdateTov4.m
%              bedstools/isBEDS.m
%              bedstools/iBEDSverifyStimParam.m
%              misc/isstring.m

% "There is some overlap" -- hah.  This function is
% horribly overloaded.  To find all trials with a 5000Hz
% tone, for example, you could use:
%      a) param='freq', value=5000;
%      b) param='STIM_TONE', value=5000;
%      c) param='signal', value=5000;
% The worst is broadband, where you could do:
%      a) param='type', value='STIM_BB' (which is preferred!)
%      b) param='STIM_BB', value=[];
%      c) param='freq', value=0;  (legacy reasons)
%      d) param='freq', value='BB'; (legacy reasons)
% In short, inconsistent.  Bleah.  
  
% Minimum version of BEDS that will work with this
% script. 
BEDSCOMPAT = 4;

if ~isBEDS(data)
  error('DATA must be valid BEDS structure');
end

%  Technically, there are a couple of values for 'param' that don't
%  require anything for 'value'.  
if nargin < 3; value=[]; end;

% If we're below the current version, update for the
% meantime. 
if data.BEDSversion < BEDSCOMPAT
  data = BEDSupdateTov4(data);
end

% Make sure it's a valid param setting.  For a list of
% all valid param settings, see iBEDSverifyStimParam.
if ~iBEDSverifyStimParam(data,param)
  error('Invalid stimulus parameter!');
end

% Do some conversions.  Looking for a 'freq' of 'BB' or 0 is
% the same as searching on 'type' for STIM_BB; similarly
% with a 'freq' of 'NB'.  Searching for 'freq' with any
% other value is the same as searching for
% 'STIM_TONE'. 
if strcmp(param,'freq')
  if (strcmp(value,'BB') | value==0)
	param='type';
	value=BEDSstimulusType('STIM_BB');
  elseif strcmp(value,'NB')
	param='type';
	value=BEDSstimulusType('STIM_NB');
  else
	param='STIM_TONE';
  end
end

% Searching for spontaneous trials is the same as searching
% for a 'type' of STIM_SPONT, but searching for 'spont' of
% '0' (non-spontaneous trials) will require special
% handling.
if strncmp(param,'spont',5) & value~=0
  param='type';
  value=BEDSstimulusType('STIM_SPONT');
end

% If the string stimulus type was passed, convert it to
% the numerical format.
if strncmp(param,'type',4) & isstring(value)
  value=BEDSstimulusType(value);
end

% If we're given a stimulus type as the parameter, and
% it's in the string format, convert it to numerical format.
if strncmp(param,'STIM_',5)
  param=BEDSstimulusType(param);
end

% Some stimulus types have no associated values.  If
% we're searching for one of these, then set a flag, and
% if the user did give us a value, issue a little warning. 
if param==BEDSstimulusType('STIM_SPONT') | param == ...
		 BEDSstimulusType('STIM_BB')
  noValue=1;
  if ~isempty(value)
	warning(sprintf(['Non-empty VAL for PARAM %s;' ...
					 ' discarding.'],BEDSstimulusType(param)))
  end
else
  noValue=0;
end  

% Whee. Bit of trouble because of finicky nested
% structure shit.
stims = [data.trial(:).stim];

% Two cases needs special handling.  If the param was one of
% the STIM_*, then it will be a number now, and we need to
% first find the trials that match the STIM_*, and then
% which of those trials matches the value.  Also, if we want
% 'spont' of '0' (non-spontaneous trials, which is the only
% time that param should still be 'spont'), then we need to
% search for all trials with a 'type' that is *not*
% STIM_SPONT.  Otherwise, it's easy.
if isnumeric(param)
  tmptrials=find([stims.type]==param);
  % if the STIM_* has no values, then we're done.
  if noValue
	trials=tmptrials;
  else
	trials=[];
	value=sort(value); %Make sure it's in the right order!
	% Because sometimes the signal field can be an array,
    % we need that all() call in there.
	for n=1:length(tmptrials)
	  if all(stims(tmptrials(n)).signal==value)
		trials=[trials tmptrials(n)];
	  end
	end
  end
elseif strncmp(param,'spont',5)
  trials=find([stims.type]~=BEDSstimulusType('STIM_SPONT'));
else
  eval(sprintf('trials = find([stims.%s]==value);',param));
end


    