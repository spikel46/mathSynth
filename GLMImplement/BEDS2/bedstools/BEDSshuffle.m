function newdata = BEDSshuffle(data,varargin)
% BEDSshuffle	   Shuffle data for Monte Carlo analysis
%
% N = BEDSNUMTRIALS(D) will return the number of trials N
% in a BEDS data file D.
%
% See also:  BEDS.

% Written by GBC, 27 Oct 2003
% RCS: $Id: BEDSnumTrials.m,v 1.1 2001/09/12 23:19:19 bjorn Exp $

% Minimum version of BEDS that will work with this
% script.
BEDSCOMPAT = 4;

if ~isBEDS(data)
  error('DATA must be valid BEDS structure');
end

% If we're below the current version, update for the
% meantime.
if data.BEDSversion < BEDSCOMPAT
  data = BEDSupdateToCurrent(data);
end

numTrials=length(data.trial);

shuffleStims=0;
shuffleWaves=0;
shuffleISILocal=0;
shuffleISIGlobal=0;

for n=1:length(varargin)
  if strcmp('stim',lower(varargin{n}))
	shuffleStims=1;
	stimOrder=randperm(numTrials);
  elseif strcmp('analog',lower(varargin{n}))
	shuffleWaves=1;
	anaOrder=randperm(numTrials);
  elseif strcmp('local',lower(varargin{n}))
	shuffleISILocal=1;
  elseif strcmp('global',lower(varargin{n}))
	shuffleISIGlobal=1;
  else
	error('Unknown shuffle option')
  end
end

if shuffleISILocal & shuffleISIGlobal
  shuffleISILocal=1;
end