function data=BEDSload(filename)

% BEDSload           Load the output of convertXDPHYSfiles.
%
% DATA = BEDSLOAD(FILE) will load the contents of the
% file FILE into DATA.  FILE is a string containing the
% name of a file (usually one created by
% convertXDPHYSfiles, but this is not necessary).
%
% (Basically, the problem is that convertXDPHYSfiles
% gives its output a unique name.  While convenient on
% some cases, this presents obstacles to writing
% scripts.) 
%
% See also:  BEDS, convertXDPHYSfiles.

% Written by GBC, 24 May 2001.
% RCS: $Id: BEDSload.m,v 1.8 2005/01/15 01:30:45 bjorn Exp $

d=load(filename);
datanames=fieldnames(d);
if length(d)>1
  error('Too much data in file!');
else
  eval(sprintf('data=d.%s;',datanames{1}));
end

