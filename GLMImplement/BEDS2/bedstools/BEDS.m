%  BEDS        Bjorn's Excellent Data Structure.
%
%  BEDS is a format for storing electrophysiological data in
%  Matlab, along with a set of tools for manipulating the data.
%  A single protocol execution is stored in the BEDS
%  protocol structure:
%
%      BEDSversion:    the version of BEDS for this element.
%      version:        a structure storing the name and version of
%                      the software used to collect the data, with
%                      elements 'program' (the program name, string),
%                      'number', 'major', and 'minor' (for version
%                      information, int).
%      animal:         an integer identifier for the animal.
%      unit:           an integer identifier for the unit.
%      filenum:        an integer identifier for the file number.
%      timestamp:      a string holding the date and time the data
%                      was collected.
%      params:         the parameters of the protocol
%                      run.  For example, in xdphys, this
%                      is a hash table (using the
%                      hashtools suite). 
%      notes:          a string for miscellaneous notes.
%      trial:          the trial specific data.  This is
%                      an array, one structure per trial.
%
%  The trial specific data is stored in an array of BEDS
%  trial structures, having the following form:
%
%      BEDSversion:    the version of BEDS used.
%      animal:         an integer identifier for the animal.
%      unit:           an integer identifier for the unit.
%      filenum:        an integer identifier for the file number.
%      trialnum:       an integer identifier for the trial number.
%      protocol:       a string storing the protocol.
%      stim:           a single structure (not an array)
%                      storing the information about the
%                      stimulus used in the trial.
%      channel:        The actual data, which is stored
%                      in an array of structures.  As the
%                      name implies, each element of the
%                      array should correspond to a
%                      single "channel" of data, which
%                      may consist of both analog and
%                      spike data.
%
%  The stimulus data is stored in a single BEDS stimulus
%  structure, which has the following form:
%
%      type:           an integer value (valid values are
%                      given by BEDSstimulusType), which
%                      sets the type of stimulus used.
%      signal:         the parameters of the signal
%                      type.  These will vary based on
%                      the value of 'type', and are
%                      described in the documentation for
%                      BEDSstimulusType.
%      abi:            the average binaural intensity of
%                      the signal (this will be NaN for
%                      FIID files).
%      itd:            the interaural time difference of
%                      the signal.
%      iid:            the interaural intensity
%                      difference of the signal.
%      bc:             the binaural correlation of the
%                      signal (in percent).
%      mono:           a string (either 'L', 'R', or 'B')
%                      describing to which ears the
%                      stimulus was presented.
%      twosnd:         an xdphys-specific boolean
%                      indicating whether a second sound
%                      was also presented.  If this is
%                      true, then you need to consult the
%                      params hash table to determine
%                      what that sound was.
%      saved:          a boolean indicating whether a
%                      copy of the stimulus was saved in
%                      the channels array. 
%
%  The channel data is saved in an array of BEDS channel
%  structures, which have the following form:
%
%      datatype:       An integer whose bits reflect the
%                      type of data present in the
%                      channel structure.  The value of
%                      various bits is given by
%                      BEDSchannelType.
%      spikes:         An array of spike times.
%      spikestoms:     The multiplicitave factor that
%                      will convert the values in
%                      'spikes' to milliseconds.
%      waveform:       An array containing the analog
%                      data.
%      tomv:           Converts 'waveform' into units of
%                      millivolts.
%      offset:         The DC shift of the analog data,
%                      in millivolts.
%      waveformhz:     The sampling rate of the analog data.
%      note:           A string that can contain a note
%                      about the channel.  While its
%                      contents are not defined, they are
%                      used currently to identify the
%                      XDPHYS chan_id.
%
%  BEDS elements should not be written.  The following tools
%  should be used instead:
%
%      BEDSinit:           create a protocol structure.
%      BEDSmakeVersion:    create a version structure.
%      BEDSmakeTrial:      create a trial structure.
%      BEDSmakeStimulus:   create a stimulus structure.
%      BEDSmakeChannel:    create a channel structure.
%
%  BEDS strucutres should also not be accessed directly.
%  Instead, use the following access methods:
%  
%      BEDSgetNote:        Get the note in a protocol structure.
%      BEDSaddNote:        Add a note to a protocol structure.
%      BEDSgetDepvar:      Get the depvar of trials.
%      BEDSgetSpikes:      Get spikes from trials.
%      BEDSgetWaveform:    Get waveforms from trials.
%      BEDSgetStimulus:    Retrieve saves stimuli from trials.
%      BEDSgetStimType:    Get the type of stimulus.
%      BEDSgetStimParam:   Get the parameters of the stimulus
%
%  Note that many 'get' methods have a matching 'find'
%  method (mainly those having to do with stimulus
%  parameters, rather than the actual data).
%
%  There are also the following miscellaneous tools.
%
%      BEDSgetver:         returns the current version of
%                          BEDS.
%      BEDSstimulusType:   handle stimulus type definitions.
%      BEDSchannelType:    handle channel type definitions.
%      isBEDS:             checks if is a valid BEDS
%                          protocol structure.
%      isBEDSversion:      checks if is a valid BEDS version
%                          structure. 
%      isBEDStrial:        checks if is a valid BEDS trial
%                          structure. 
%      isBEDSchannel:      checks if is a valid BEDS channel
%                          structure. 
%
%  Functions beginning with 'iBEDS' are internal BEDS functions.
%  They should be imbedded in other BEDS functions only.

% RCS: $Id: BEDS.m,v 1.12 2004/09/27 05:18:53 bjorn Exp $
