function [Onumtrials,Onumchans,Owaveforms,Ospikes] = BEDSstatus(data,verbose);
% BEDSstatus             Get basic information on a BEDS data file.
%
% [T,C,A,S] = BEDSSTATUS(DATA) will return the number of
% trials T, the number of channels C, the channels that
% have analog data A, and the channels with spike data S,
% in the BEDS data file DATA.  By default, this
% information will also be echoed to the terminal.
%
% [...] = BEDSSTATUS(DATA,V) will cause the terminal
% output to be surpressed if V is 0.  By default, V is
% 1. 
%
% See also:  BEDS.

% RCS: $Id: BEDSstatus.m,v 1.3 2003/03/18 01:28:41 bjorn Exp $
% Written by GBC, 25 Sep 2001.
% Depends on:  bedstools/BEDSfindNonspont.m
%              bedstools/BEDSchannelType.m
  
% Argument defaults and checking.
if nargin < 2
  verbose = 1;
end

if ~BEDSinit(data)
  error('DATA must be a valid BEDS file');
end


numtrials = length(data.trial);
numchans = length(data.trial(1).channel);

nonspontTrials=BEDSfindNonspont(data);

numsponttrials=numtrials-length(nonspontTrials);

waveforms = [];
spikes = [];

% Go through channels looking for data.
temp=data;
temp.trial=data.trial(nonspontTrials);
for tr=1:length(temp.trial)
  for chan=1:numchans
	if bitand(temp.trial(tr).channel(chan).datatype, ...
			  BEDSchannelType('CHAN_SPIKES'))
	  spikes = unique([spikes chan]);
	end
	
	if bitand(temp.trial(tr).channel(chan).datatype, ...
			  BEDSchannelType('CHAN_ANALOG'))
	  waveforms = unique([waveforms chan]);
	end
  end
end

% Echo data.
if verbose
  disp(sprintf('Number of trials: %d',numtrials));
  disp(sprintf('Number of channels: %d',numchans));
  disp(sprintf('Number of spontaneous trials: %d',numsponttrials));
  if isempty(spikes);
    disp('No spike data.');
  else
    disp('Spikes in channels:');
    disp(spikes);
  end
  if isempty(waveforms);
    disp('No analog data.');
  else
    disp('Analog data in channels:');
    disp(waveforms);
  end
end

% Assign outputs.
if nargout > 0
  Onumtrials = numtrials;
  Onumchans = numchans;
  Owaveforms = waveforms;
  Ospikes = spikes;
end

  
