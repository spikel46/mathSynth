function true = isBEDSstimulus(x)
% isBEDSstimulus    Check to see if a value is a BEDS stimulus structure.
%
% isBEDSstimulus(X) will return 1 if X is a BEDS stimulus structure
% and 0 else.
  
% Written by GBC, 30 Sep 2001.
% RCS: $Id: isBEDSstimulus.m,v 1.1 2001/10/01 02:54:12 bjorn Exp $

% Depends on bedstools/BEDSmakeStimulus.m
  
  if (nargin == 1)
    true = BEDSmakeStimulus(x);
  else
    error('Incorrect number of arguments');
  end
  
