function value = BEDSgetStimParam(data,param,trial);

% BEDSgetStimParam   Get given stimulus parameter
%
% V = BEDSGETSTIMPARAM(DATA,PARAM) will get the value of
% the stimulus parameter PARAM in all trials of DATA and
% return it in the vector V.  PARAM can be any
% of the fields of the BEDS stimulus structure (see the
% BEDS help file), 'freq', 'spont', or one of the valid
% stimulus types (see BEDSstimulusType).
%
% V = BEDSGETSTIMPARAM(...,TRIAL) does the same but
% allows you to specify a subset of trials to examine.
%
% A couple of important notes:
%    1) If the stimulus was broadband, then 'freq' will
%    be zero.  If the stimulus was NOSND, then 'freq'
%    will be -1.
%
%    2) If the trial was spontaneous (ie 'spont' is 1),
%    then 'mono' will be 'S' and all other values will be
%    0.   
%
%    3) If PARAM is a stimulus type that has no
%    associated parameters, then V will be empty.  If it
%    is a type that has multiple parameters (e.g.,
%    STIM_NB), then V will be a cell array.  This sucks,
%    but hey, what can you do?
%
% See also:  BEDS, BEDSfindStimParam.

% RCS: $Id: BEDSgetStimParam.m,v 1.8 2003/09/23 00:18:09 bjorn Exp $
% Written by GBC, 30 Sep 2001.
% Depends on:  bedstools/BEDSupdateTov3.m
%              bedstools/isBEDS.m
%              bedstools/iBEDSdefaultArgs.m
%              bedstools/iBEDStrialChanSetup.m
%              bedstools/iBEDSverifyStimParam.m

% Minimum version of BEDS that will work with this
% script. 
BEDSCOMPAT = 4;

if ~isBEDS(data)
  error('DATA must be a valid BEDS structure');
end

% If we're below the current version, update for the
% meantime. 
if data.BEDSversion < BEDSCOMPAT
  data = BEDSupdateTov4(data);
end

% Make sure it's a valid param setting.
if ~iBEDSverifyStimParam(data,param)
  error('Invalid stimulus parameter!');
end

% Make sure trial has a value and is one-dimensional.
if (nargin < 3); trial=iBEDSdefaultArgs('trial');end;
trial=iBEDStrialChanSetup(data,trial);

if strcmp(param,'freq')
  param='STIM_TONE';
end
if strncmp(param,'STIM_',5)
  param=BEDSstimulusType(param);
end

% If it's numeric, then it's a STIM_*.  If it's one with
% multiple parameters, then we need cell array outputs.
% If it's one with no parameters, then there's no work to
% do.  Otherwise, we're okay.
if isnumeric(param)
  if param==BEDSstimulusType('STIM_NB') | param== ...
		   BEDSstimulusType('STIM_STACK')
	cellOutput=1;
	value={};
  elseif param==BEDSstimulusType('STIM_BB') | param== ...
		BEDSstimulusType('STIM_SPONT')
	value=[];
	return
  else
	cellOutput=0;
  end
end

% The case of getting specific STIM_* requires special
% handling.  Everything else is done the same (easy) way.
if isnumeric(param)
  for tr = 1:length(trial)
	if cellOutput
	  if (data.trial(trial(tr)).stim.type == param)
		value{tr}=data.trial(trial(tr)).stim.signal;
	  else
		value{tr}=[];
	  end
	else
	  if (data.trial(trial(tr)).stim.type == param)
		value(tr)=data.trial(trial(tr)).stim.signal;
	  else
		value(tr)=0;
	  end
	end
  end
else
  for tr = 1:length(trial)
	value(tr) = getfield(data.trial(trial(tr)).stim,param);
  end
end

    