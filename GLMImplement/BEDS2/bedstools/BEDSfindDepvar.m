function trials = BEDSfindDepvar(data,depvar)

% BEDSfindDepvar    find trials matching depvar in BEDS
%                   protocol structure
%
% E = BEDSFINDDEPVAR(DATA,DEPVAR) will find all trials in
% the BEDS protocol structure DATA whose depvar matches
% DEPVAR. 
%
% See also:  BEDS, BEDSgetDepvar

% Written by GBC, 22 Mar 2001.
% RCS: $Id: BEDSfindDepvar.m,v 1.1 2001/03/23 01:13:02 bjorn Exp $
% Depends on:  bedstools/isBEDS.m

if ~(isBEDS(data))
  error('DATA must be a BEDS protocol structure.');
end

if nargin < 2
  error('DEPVAR must be specified.');
end

if (length(depvar) ~= length(1))
  error('DEPVAR must be scalar.');
end

% Only tricky part is the square brackets.
trials = find([data.trial.depvar]==depvar);