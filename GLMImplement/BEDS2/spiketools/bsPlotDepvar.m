function [depvars,meanout,stdout,numspk]=bsPlotDepvar(data,timewin,channel,plotflag)
% bsPlotDepvar          Plot mean spike count against depvar
%
% BSPLOTDEPVAR(DATA) will plot the mean spike count
% (during the stimulus period) against the depvar.  DATA
% must be a BEDS data structure.
%
% BSPLOTDEPVAR(DATA,TIMEWIN) does the same but also
% allows you to specify the timewindow of analysis.
% TIMEWIN can be a range [lower upper], a scalar which
% will be interpreted as an offset relative to the
% beginning of stimulus, or the string 'default', which
% will force default behaviour.
% 
% BSPLOTDEPVAR(DATA,TIMEWIN,CHAN) lets you specify the
% channel CHAN (as in BEDSgetSpikes).
%
% BSPLOTDEPVAR(DATA,TIMEWIN,CHAN,PLOTFLAG) does the same, but
% the graph will not be plotted if PLOTFLAG is 0, and
% will be if PLOTFLAG is 1.
%
% [D,M,S,N] = BSPLOTDEPVAR(...) will do the same, but will
% return values.  D will be a sorted list of depvars, while
% M and S are the mean and standard deviation of the spike
% counts, in the same order as D.  N is a [iters x depvar]
% matrix, whose (i,j)th entry is the number of spikes in the
% ith repetition of the jth depvar. By default, if output
% arguments are specified, PLOTFLAG is 0.
%
% Note that the output matrix N will always be of size
% (specified iterations) x (# depvars).  If the file was
% data-light, the matrix will be padded with NaN.  This
% does not affect outputs M or S.
%
% If the data file is of BEDS version 3 or higher,
% spontaneous trials will automatically be ignored.
%
% This program is meant to supersede both 'nspikes' and 'numspikes'.
% It also currently assumes the data was collected with
% xdphys. 
%
% See also:  BEDS.

% Written by GBC, 22 May 2001.
% RCS: $Id: bsPlotDepvar.m,v 1.11 2003/07/08 23:00:47 bjorn Exp $
% Depends on bedstools/BEDSgetSpikes.m
%            bedstools/iBEDSdefaultArgs.m
%            bedstools/iBEDStrialChanSetup.m
%            bedstools/iBEDScheckTimewin.m
%            bedstools/BEDSchannelType.m
%            bedstools/BEDSgetDepvar.m
%            bedstools/BEDSfindDepvar.m
%            bedstools/BEDSfindStimParam.m
%            bedstools/BEDSgetEpoch.m
%            hashtable/hashFind.m
%            misc/isstring.m

BEDSCOMPAT = 4;

if ~isBEDS(data)
  error('DATA must be a BEDS structure');
else
  if data.BEDSversion < BEDSCOMPAT
    data = BEDSupdateToCurrent(data);
  end
end

% Set default argument values.
if (nargin < 2);timewin=iBEDSdefaultArgs('timewin');end;
if (nargin < 3);channel=iBEDSdefaultArgs('chan');end;
if nargin < 4
  if nargout == 0
    plotflag = 1;
  else
    plotflag = 0;
  end
end

% Sanity checks.
[trial,channel]=iBEDStrialChanSetup(data,'all',channel,...
									BEDSchannelType('CHAN_SPIKES'));
timewin=iBEDScheckTimewin(data,timewin,'dataonly');

% Get the number of spikes.  Ain't this elegant?  Now go
% take a look at numspikes to see the puke way to do it.
[iters,success] = hashFind(data.params,'reps','num');
if ~success
  error('Unable to find number of repetitions in DATA');
end

depvar = unique(BEDSgetDepvar(data));
spikeholder = NaN*ones([iters length(depvar)]);
trueIters=0;
for n = 1:length(depvar)
  spikes = BEDSgetSpikes(data,BEDSfindDepvar(data,depvar(n)),channel,timewin);
  nspikes=[];
  if length(spikes) < iters & length(spikes) > trueIters
	trueIters=length(spikes);
  end
  for nn = 1:length(spikes)
    nspikes(nn) = length(spikes{nn});
	spikeholder(nn,n)=nspikes(nn);
  end
  smean(n)=mean(nspikes);
  sstd(n)=std(nspikes);
end

if plotflag
  errorbar(depvar,smean,sstd);
end

if nargout > 0
  depvars = depvar;
  meanout = smean;
  stdout = sstd;
  if trueIters > 0 
	spikeholder=spikeholder(1:trueIters,:);
  end
  numspk = spikeholder;
end
