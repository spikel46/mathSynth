function [sgram,freqs,times]=bsSonogram(data,trial,chan,win,overlap,nfft,plotflag)
% bsSonogram             Plot the sonogram of a neural  waveform.
%
% BSSONOGRAM(DATA) will plot the sonogram of the waveform
% of the first trial of the first channel.
%
% BSSONOGRAM(DATA,TR,CH,WIN,OVERLAP,NFFT,PLOTFLAG) lets
% you specifiy the trials to plot TR (default = 1),
% channels to plot CH (default = 1), the window size WIN
% (default = 350 samples), the window overlap OVERLAP
% (default = 330 samples), the zero-padded length NFFT
% (default = 2^nextpow2(win)), and whether or not to plot
% (PLOTFLAG; default is 1 if no output arguments, 0
% otherwise).
%
% [S,F,T] = BSSONOGRAM(...) does the same, but returns
% the results of the specgram call.  This will not work
% if more than one waveform is specified.  Plotting is
% disabled by default.
%
% See also:  BEDS, BEDSgetWaveform.

% RCS: $Id: bsSonogram.m,v 1.1 2001/06/12 00:38:29 bjorn Exp $.
% Written by GBC, 11 June 2001.
% Depends on:  bedstools/BEDSgetWaveform.m

if nargin < 2; trial = 1;                end;
if nargin < 3; chan = 1;                 end;
if nargin < 4; win = 350;                end;
if nargin < 5; overlap = 330;            end;
if nargin < 6; nfft  = 2^nextpow2(win);  end; 
if nargin < 7
  if nargout > 0
    plotflag = 0;
  else
    plotflag = 1;
  end
end

if nargout > 0
  if length(trial)*length(chan) > 1
    error(['Unable to return values for more than one' ...
	   ' waveform']);
  end
end
   

[waves,sr] = BEDSgetWaveform(data,trial,chan);

for tr = 1:length(trial)
  for ch = 1:length(chan)
    wave = 2^15.*waves{tr,ch}./max(abs(waves{tr,ch}));
    [b,f,t] = specgram(wave,nfft,sr(tr,ch),win,overlap);
    t = t.*1000;
    if plotflag
      figure;
      imagesc (t,f,20*log10(abs(b)));
      axis ('xy');
      colormap ('pink');
      axis ([0 max(t) 0 15000]);
      caxis([45 115]); 
      brighten(-0.6);
    end
  end
end

if nargout > 0
  sgram = b;
  freqs = f;
  times = t;
end

