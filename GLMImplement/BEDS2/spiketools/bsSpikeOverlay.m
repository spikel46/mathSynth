function outwaves = bsSpikeOverlay(data,trial,chan,windowsize,plotflag)
% bsSpikeOverlay          Overlay neural waveforms from spikes
%
% BSSPIKEOVERLAY(DATA,TRIAL,CHAN,WIN) will plot the
% neural waveforms immediately surrounding each of the
% spikes.  You can specify the trials TRIAL (default =
% all), the channels CHAN (default = 1), and the
% size of the window to use WIN (in seconds, default =
% .001).  
%
% OUT = BSSPIKEOVERLAY(...) does the same, but will returns
% the spike shapes in a cell array.  Plotting is disabled by
% default.
%
% OUT = BSSPIKEOVERLAY(...,PLOT) does the same, but
% allows the user to override default plotting
% behaviour.  Plotting will be forced if PLOT is 1, and
% disabled if PLOT is 0.
%
% See also:  BEDS.

% RCS: $Id: bsSpikeOverlay.m,v 1.3 2003/07/08 23:00:47 bjorn Exp $.
% Written by GBC, 11 June 2001.
% Depends on:  bedstools/BEDSgetSpikes.m
%              bedstools/iBEDSdefaultArgs.m
%              bedstools/iBEDStrialChanSetup.m
%              bedstools/BEDSgetWaveform.m
%              hashtable/hashFind.m

% Set defaults.
if nargin < 2; trial = iBEDSdefaultArgs('trial');end;
if nargin < 3; chan = iBEDSdefaultArgs('chan');  end;
if nargin < 4; windowsize = .001;                end;
if nargin < 5
  if nargout > 0 
    plotflag = 0;
  else
    plotflag = 1;
  end
end

% Sanity checks.
[trial,channel]=iBEDStrialChanSetup(data,trial,channel,...
									BEDSchannelType('CHAN_SPIKES'));

% Get the epoch length, in seconds.
epoch = BEDSgetEpoch(data);
epoch = epoch/1000;


% Get spikes, waveforms, and sampling rates.
spikes = BEDSgetSpikes(data,trial,chan);
[waves,sr] = BEDSgetWaveform(data,trial,chan);

if plotflag
  figure;
  hold on;
end

% Initialize some variables., and get the half window in
% samples. 
winsamps = sr*windowsize/2;
spikewaves = {};
miniwaves = {};
for tr = 1:length(trial)
  for ch = 1:length(chan)
    spike = spikes{tr,ch};
    for sp = 1:length(spike)
      occursamp = spike(sp)*sr(tr,ch)/1000;
      % Have to check boundaries!
      lowbound = max(1,round(occursamp-winsamps(tr,ch)));
      hibound = min(round(epoch*sr(tr,ch)), ...
		    round(occursamp+winsamps(tr,ch)));
      miniwaves{sp} = waves{tr,ch}(lowbound:hibound);
      if plotflag
	plot(miniwaves{sp});
      end
    end
    spikewaves{tr,ch}=miniwaves;
  end
end

% Return values.
if nargout > 0
  outwaves = spikewaves;
end






