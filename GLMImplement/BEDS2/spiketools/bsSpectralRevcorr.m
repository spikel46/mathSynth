function [revcorr,errbars,spectralPESE,samprate,F]=bsSpectralRevcorr(data,window,trial,chan,timewin,specWindow,NFFT,sidedness)

% bsRevcorr                Compute a reverse correlation
% blahddidblahblah

% Written by GBC, 8 Jul 2003.
% RCS: $Id: bsSpectralRevcorr.m,v 1.1 2003/07/23 20:21:26 bjorn Exp $
% Depends on:  bedstools/isBEDS.m
%              bedstools/BEDSupdateTov4.m
%              bedstools/iBEDSdefaultArgs.m
%              bedstools/iBEDStrialChanSetup.m
%              bedstools/iBEDScheckTimewin.m
%              bedstools/BEDSchannelType.m
%              spiketools/bsGetPESE.m  
BEDSCOMPAT = 4;

if ~isBEDS(data)
  error('DATA must be a BEDS structure');
else
  if data.BEDSversion < BEDSCOMPAT
    data = BEDSupdateTov4(data);
  end
end

% Set default argument values.
if (nargin < 3);trial=iBEDSdefaultArgs('trial');end;
if (nargin < 4);chan=iBEDSdefaultArgs('chan');end;
if (nargin < 5);timewin=iBEDSdefaultArgs('timewin');end;
if (nargin < 6);specWindow=[];end;
if (nargin < 7);NFFT=[];end;
if (nargin < 8);sidedness='onesided';end;

% Sanity checks.
[trial,channel]=iBEDStrialChanSetup(data,trial,chan,...
									BEDSchannelType('CHAN_SPIKES'));
timewin=iBEDScheckTimewin(data,timewin,'dataonly');

% The hard part is done by bsGetPESE.
[PESE,samprate]=bsGetPESE(data,window,trial,chan,timewin);

numPESE=size(PESE{1});
for nn=1:length(PESE)
  for n=1:numPESE(1)
  [spectralPESE{nn}(n,:),F]=periodogram(PESE{nn}(n),specWindow, ...
								   NFFT,samprate,sidedness);
  end
end
for n=1:length(spectralPESE)
  revcorr(n,:)=mean(spectralPESE{n});
  errbars(n,:)=std(spectralPESE{n});
end
