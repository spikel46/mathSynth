function [depvars,meanout,stdout,numspk]=bsPlotStimParam(data,param,timewin,trial,chan,plotflag)
% bsPlotStimParam          Plot mean spike count against stim param
%
% BSPLOTSTIMPARAM(DATA,PARAM) will plot the mean spike count
% (during the stimulus period) against the stim param PARAM.  DATA
% must be a BEDS data structure; PARAM must be one of the
% legitimate stim param fields (see BEDSfindStimParam).
%
% BSPLOTSTIMPARAM(...,TIMEWIN) does the same but also
% allows you to specify the timewindow of analysis.
% TIMEWIN can be a range [lower upper], a scalar which
% will be interpreted as an offset relative to the
% beginning of stimulus, or the string 'default', which
% will force default behaviour.
% 
% BSPLOTSTIMPARAM(...,TR,CHAN) lets you specify the
% trials TR and channels CHAN to consider (as in BEDSgetSpikes).
%
% BSPLOTSTIMPARAM(...,PLOTFLAG) does the same, but
% the graph will not be plotted if PLOTFLAG is 0, and
% will be if PLOTFLAG is 1.
%
% [D,M,S,N] = BSPLOTSTIMPARAM(...) will do the same, but
% will return values.  D will be a sorted list of
% stim params, while N will be a (iters x #depvar matrix),
% where the (i,j)'th entry will be the number of spikes
% in the i'th repitition of the depvar D(j).  M and S are
% the mean and standard deviation of the spike counts, in
% the same order as D.  By default, if output arguments
% are specified, PLOTFLAG is 0.
%
% See also:  BEDS, bsPlotDepvar.

% Written by GBC, 10 Oct 2001.
% RCS: $Id: bsPlotStimParam.m,v 1.4 2003/07/08 23:00:47 bjorn Exp $
% Depends on bedstools/BEDSgetSpikes.m
%            bedstools/iBEDSdefaultArgs.m
%            bedstools/iBEDStrialChanSetup.m
%            bedstools/iBEDScheckTimewin.m
%            bedstools/BEDSchannelType.m
%            bedstools/BEDSgetEpoch.m
%            bedstools/BEDSupdateToV4.m
%            bedstools/BEDSverifyStimParam.m
%            bedstools/BEDSfindStimParam.m
%            bedstools/BEDSgetStimParam.m
%            bedstools/isBEDS.m
%            misc/isstring.m

% Minimum compatible version of BEDS.
BEDSCOMPAT = 4;

if ~isBEDS(data)
  error('DATA must be a BEDS structure.');
end

if data.BEDSversion < BEDSCOMPAT
  data = BEDSupdateTov4(data);
end

if ~BEDSverifyStimParam(data,param)
  error('Invalid stimulus paramter!')
end

% Set default argument values.
if (nargin < 3);timewin=iBEDSdefaultArgs('timewin');end;
if (nargin < 4);trial=iBEDSdefaultArgs('trial');end;
if (nargin < 5);channel=iBEDSdefaultArgs('chan');end;
if (nargin < 5);noconv=0;end;
% By default, if there's no assigned outputs, we plot. 
if nargin < 6
  if nargout == 0
    plotflag = 1;
  else
    plotflag = 0;
  end
end

% Sanity checks.
[trial,channel]=iBEDStrialChanSetup(data,trial,channel,...
									BEDSchannelType('CHAN_SPIKES'));
timewin=iBEDScheckTimewin(data,timewin,'dataonly');

% We need to do two BEDSgetSpikes calls -- to get a
% subset of the user-specified subset.  So watch this
% trick:
temp = data;
temp.trial = data.trial(trial);

% We can ignore any spontaneous trials there may be --
% unless, of course, we're trying to plot spont trials.
if ~strcmp(param,'spont')
  temp.trial = temp.trial(BEDSfindStimParam(temp,'spont',0));
end

% Get the number of spikes.  Ain't this elegant?  Now go
% take a look at numspikes to see the puke way to do it.
depvar = sort(unique(BEDSgetStimParam(temp,param)));
for n = 1:length(depvar)
  reltrials = BEDSfindStimParam(temp,param,depvar(n));
  spikes = BEDSgetSpikes(temp,reltrials,chan,timewin);
  nspikes=[];
  for nn = 1:length(spikes)
    nspikes(nn) = length(spikes{nn});
  end
  smean(n)=mean(nspikes);
  sstd(n)=std(nspikes);
end

if plotflag
  errorbar(depvar,smean,sstd);
end

if nargout > 0
  depvars = depvar;
  meanout = smean;
  stdout = sstd;
  numspk = nspikes;
end
