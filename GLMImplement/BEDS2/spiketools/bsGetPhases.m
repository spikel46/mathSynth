function [phases,cycles] = bsGetPhases(data,trial,chan,timewin,noconv);

% bsGetPhases     get phase data from a BEDS protocol
%                 structure
%
% [P,C]=BSGETPHASES(DATA) will retrieve the phase P and
% cycle C data with reference to the appropriate stimulus
% for each spike in channel 1 of all trials and return it in
% a cell array.  The phases and cycles will be set to -1 and
% 0, respectively, if the stimulus is broadband rather than
% a tone.
%
% [...]=BSGETPHASES(DATA,TRIAL) will retrieve phase and
% cycle data from the subset of trials specified in TRIAL.
% Data from all trials will be returned if TRIAL is set to
% 'all'.
%
% [...]=BSGETPHASES(DATA,TRIAL,CHAN) will do the same, but
% retrieving from channels CHAN, where CHAN is a vector list
% of desired channels.  If CHAN is non-scalar, then the data
% will be returned in a two-dimensional cell array, with the
% first index being trial and the second being channel.
%
% [...]=BEDSGETPHASES(...,TIMEWIN) will do the same, but
% only return data on spikes in the time window TIMEWIN.
% TIMEWIN can be scalar, in which case the spikes between
% stimulus onset and stimulus onset plus TIMEWIN will be
% considered; or a vector of the form [lowertime uppertime].
%
% Note that cycles are defined with respect to "periods
% since stimulus onset," rather than with respect to
% "zero-crossings of the stimulus".  In other words, the
% first cycle is always of length (1/freq).
%
% See also:  BEDS, BEDSgetSpikes, bsVectorStrength.

% Written by GBC, 3 Oct 2001.
% RCS: $Id: bsGetPhases.m,v 1.17 2003/07/23 20:21:26 bjorn Exp $
% Depends on:  bedstools/isBEDS.m
%              bedstools/iBEDSdefaultArgs.m
%              bedstools/iBEDStrialChanSetup.m
%              bedstools/iBEDScheckTimewin.m
%              bedstools/BEDSchannelType.m
%              bedstools/BEDSgetEpoch.m
%              bedstools/BEDSgetSpikes.m
%              bedstools/BEDSupdateTov3.m
%              bedstools/BEDSgetStimParam.m
%              hashtools/hashFind.m
%              spiketools/bsCalculatePhase.m
%              misc/isstring.m

BEDSCOMPAT = 4;

if ~isBEDS(data)
  error('DATA must be a BEDS structure');
else
  if data.BEDSversion < BEDSCOMPAT
    data = BEDSupdateToCurrent(data);
  end
end

% Set default argument values.
if (nargin < 2);trial=iBEDSdefaultArgs('trial');end;
if (nargin < 3);chan=iBEDSdefaultArgs('chan');end;
if (nargin < 4);timewin=iBEDSdefaultArgs('timewin');end;
if (nargin < 5);noconv=0;end;

% Sanity checks.
[trial,chan]=iBEDStrialChanSetup(data,trial,chan,...
									BEDSchannelType('CHAN_SPIKES'));
timewin=iBEDScheckTimewin(data,timewin,'dataonly');

% Extract some parameters.
[epoch,duration,delay]=BEDSgetEpoch(data);
[radvary,success] = hashFind(data.params,'tone.rad_vary','num');
if ~success
  error('Unable to find RAD-VARY parameter.');
end
[iters,success] = hashFind(data.params,'reps','num');
if ~success
  error('Unable to find number of repetitions in DATA');
end

% Calculate this, rather than just measuring the length,
% in case the file is data light. 
numtrials=length(unique(BEDSgetDepvar(data)))*iters;

% Doodeedoodeedoo.
phases = {};
cycles = {};

for ch = 1:length(chan)
  for tr = 1:length(trial)
    freq = BEDSgetStimParam(data,'freq',trial(tr));
    spikes = BEDSgetSpikes(data,trial(tr),chan(ch),timewin,noconv);
    if freq == 0 | freq == -1
      phases{tr,ch} = -1*ones(size(spikes{1}));
      cycles{tr,ch} =  zeros(size(spikes{1}));
    else
      if radvary
		repnum=(data.trial(trial(tr)).trialnum-1)*iters/numtrials;
		initphase=2*pi*floor(repnum)/iters;
      else
		initphase = 0;
      end
	  [phases{tr,ch},cycles{tr,ch}]= ...
		  bsCalculatePhase(spikes,delay,freq,initphase);
    end
  end
end

