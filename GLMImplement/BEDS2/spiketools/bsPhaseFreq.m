function bsPhaseFreq(data,timewin,channel);
% bsPhaseFreq       Plot frequency versus phase of firing.
%
% BSPHASEFREQ(DATA) will plot the frequency of stimlus
% versus the mean phase of spike firing.  By default, only
% the spikes that fired during the stimulus will be
% considered.
%
% BSPHASEHIST(DATA,TIMEWIN) allows you to specify the
% window of analysis.  TIMEWIN can be scalar, in which
% case the spikes between stimulus onset and stimulus
% onset plus TIMEWIN will be considered; or a vector of
% the form [lowertime uppertime].
%
% BSPHASEHIST(DATA,TIMEWIN,CHAN) does the same thing, but
% allows you to specify the channels CHAN to consider (as
% in BEDSgetSpikes).
%
% See also: BEDS, bsPhaseHist.

% RCS: $Id: bsPhaseFreq.m,v 1.4 2003/07/08 23:00:46 bjorn Exp $
% Written by GBC, 23 May 2001.
% Depends on:  hashtable/hashFind.m
%              bedstools/iBEDSdefaultArgs.m
%              bedstools/iBEDStrialChanSetup.m
%              bedstools/iBEDScheckTimewin.m
%              bedstools/BEDSchannelType.m
%              bedstools/BEDSgetSpikes.m
%              bedstools/BEDSgetDepvar.m
%              bedstools/BEDSfindDepvar.m
%              bedstools/BEDSgetEpoch.m

% Do some sanity checks on timewin, plus set the default.
% Note that even though we *say* timewin reverts to the
% default if it is 'default', in fact, any string will
% do. 

BEDSCOMPAT = 4;

if ~isBEDS(data)
  error('DATA must be a BEDS structure');
else
  if data.BEDSversion < BEDSCOMPAT
    data = BEDSupdateToCurrent(data);
  end
end

% Set default argument values.
if (nargin < 2);timewin=iBEDSdefaultArgs('timewin');end;
if (nargin < 3);channel=iBEDSdefaultArgs('chan');end;

% Sanity checks.
[trial,channel]=iBEDStrialChanSetup(data,'all',channel,...
									BEDSchannelType('CHAN_SPIKES'));
timewin=iBEDScheckTimewin(data,timewin,'dataonly');

% Extract some parameters.
[epoch,duration,delay]=BEDSgetEpoch(data);
[radvary,success] = hashFind(data.params,'tone.rad_vary','num');
if ~success
  error('Unable to find RAD-VARY parameter.');
end
[iters,success] = hashFind(data.params,'reps','num');
if ~success
  error('Unable to find number of repetitions in DATA');
end

% We need to make sure this is bf file.
[junk,success]=hashFind(data.params,'bf.mono');
if ~success
  error('Need a BF file.');
end

% Get the depvars, and ignore spontaneous trials.
depvar = unique(BEDSgetDepvar(data));
if (depvar(1) == -6666)
  depvar = depvar(2:end);
end

for n = 1:length(depvar)
  if radvary
    phase = 2*pi*n/iters;
  else
    phase = 0;
  end
  phases = [];
  spikes = BEDSgetSpikes(data,BEDSfindDepvar(data,depvar(n)),channel,timewin);
  for nn = 1:length(spikes)
    phases = [phases mod(spikes / depvar(n) + phase,2*pi)];
  end
  meanphases(n) = mean(phases);
  stdphases(n) = std(phases);
end

% Plot the graph.
errorbar(depvar,meanphases,stdphases);
xlabel('Frequency');
ylabel('Mean phase at firing');






