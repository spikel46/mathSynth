function [phases,cycles] = bsCalculatePhase(spikes,delay,freq,initphase);

% bsCalculatePhases  Calculate phase data.
%
% [P,C]=BSGETPHASES(SPIKES,DELAY,FREQ,INITPHASE) will
% calculate the phase at which the spikes in the first
% element of cell array SPIKES occurred assuming the
% stimulus was a tone with stimulus onset DELAY,
% frequency FREQ, and initial phase INITPHASE.  It will
% return an array P of phases and C of cycles.
%
% Unless you for some reason need to manually specify
% stimulus parameters (for example, if you want to
% pretend the stimulus was something other than what it
% was), you should really be using bsGetPhases.
% 
% See also:  BEDS, BEDSgetSpikes, bsGetPhases, bsVectorStrength.

% Written by GBC, 24 Apr 2002.
% RCS: $Id: bsCalculatePhase.m,v 1.4 2004/09/27 05:18:54 bjorn Exp $

BEDSCOMPAT = 3;
if ~isempty(spikes{1})
  period = 1000/freq;
  timeInCycle = mod((spikes{1}-delay),period);
  phases=mod(((spikes{1}-delay)/period)*2*pi+initphase,2*pi)-pi;
  cycles = ((spikes{1}-delay)-timeInCycle)/period;
else
  phases=[];
  cycles=[];
end


