function [histcnt, histmean, histstd] = bsPsth(spikes,bins,stim)
% bsPsth             Plot a peristimulus time histogram.
%
% BSPSTH(SPIKES) plots an unnormalized bar histogram of the
% spikes in SPIKES, aligning with respect to 0 time and
% using the bins with edges 0:10:500 ms.  SPIKES is a cell
% column array, with each cell containing an array of spike
% times (in ms).  Rows in SPIKES correspond to different
% cells, which will be plotted in an overlapping manner.  In
% other words, SPIKES is to be consistent with the output of
% BEDSgetSpikes.
%
% BSPSTH(SPIKES,BINS) does the same, but uses the bin
% edges BINS.
%
% BSPSTH(...,STIM) does the same, but aligns at time
% STIM.  STIM can either be a scalar number, in which
% case all trials are aligned at the same time, or an
% array of the same length as a column in SPIKES.
%
% [COUNT,MEAN,STD] = BSPSTH(...) returns the raw
% count, mean, and standard deviation for each bin.  (The
% PSTH can be replotted using:  bar(BINS,COUNT,'histc').
% Note the values of "MEAN" and "STD" are different if
% only a single spike train is presented!
%
% See also:  BEDS, BEDSgetSpikes, histc.

% RCS:$Id: bsPsth.m,v 1.4 2001/09/20 21:39:52 bjorn Exp $
% Written by GBC, 21 May 2001.
% Based on code written by Maneesh Sahani and John Pezaris.

% default the arguments
if nargout == 0
  color = num2cell(get(gca, 'colororder'), 2);
end


if (nargin < 2) bins = 0:10:500;		end
if (nargin < 3) stim = 0;			end

% Make sure stim is the right size.
if length(stim) == 1
  stim = stim * ones(size(spikes(:,1)));
end

ymax = -inf;
ymin = inf;

if length(stim) > 0
  numtrials = length(spikes(:,1));
  numcells = length(spikes(1,:));
  numbins = length(bins);
 
  
  if ((numcells > 1) & (nargout > 0))
    error(['Only one cell can be specified with output' ...
	   ' arguments.']);
  end
  

  for c = 1:numcells
    
    % Initialize variables.
    h  = zeros (numtrials,numbins); % histogram of counts
    h2 = zeros (numbins,1);	% square of counts
    x  = bins(1:numbins-1) + diff(bins)/2;
  
    for t = 1:numtrials
      evt = spikes{t,c} - stim(t);
      if length(evt)
        [dh, x] = histc(evt', bins);
	% row/col orientation of dh is unpredictable.
        % Need to check it.
	dhsize=size(dh);
	if dhsize(1) > dhsize(2)
	  h(t,:) = dh';
	else
	  h(t,:) = dh;
	end
      end
    end
    
    if numtrials > 1
      histcount = sum(h);
    else
      histcount = h;
    end
      
    if nargout > 0
      histcnt = histcount;
      histmean = mean(h);
      histstd = std(h);
    else
      if (iscell(color))
	cell_color = color{1 + mod(c - 1, length(color))};
      else
	cell_color = color;
      end
      set(gca, 'ColorOrder', cell_color, 'NextPlot', 'add');
      set(gca, 'NextPlot', 'add');
      handle = bar(bins,histcount,'histc');
      set(handle,'FaceColor',cell_color);
      ymin = min(ymin, min(histcount));
      ymax = max(ymax, max(histcount));
    end
  
  end

end

if nargout == 0
  ylim = [ymin - 0.1 * (ymax - ymin), ymax + 0.1 * (ymax - ymin)];
  if (sum(isinf(ylim)) > 0) ylim = [0 1]; end;
  set(gca, 'ylim', ylim);
end



