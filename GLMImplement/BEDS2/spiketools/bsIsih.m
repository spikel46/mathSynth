function histcnt = bsIsih(spikes,bins,plotflag)
% bsIsih             Plot an inter-spike interval histogram.
%
% BSISIH(SPIKES) plots an unnormalized bar histogram of the
% inter-spike intervals in SPIKES, using bins 0:.1:5 ms.
% SPIKES is a cell column array, with each cell containing
% an array of spike times (in ms).  Rows in SPIKES
% correspond to different cells, which will be plotted in an
% overlapping manner.  In other words, SPIKES is to be
% consistent with the output of BEDSgetSpikes.
%
% BSISIH(SPIKES,BINS) does the same, but uses the bin
% edges BINS.  If BINS is 'default', the default bins are
% used. 
%
% BSISIH(SPIKES,BINS,PLOT) will not plot the histogram if
% PLOT=0.  The default is PLOT nonzero, unless there is a
% return value....
%
% COUNT = BSISIH(...) returns the raw count for each bin.
% (The ISIH can be replotted using: bar(BINS,COUNT,'histc').
% By default, the histogram will not be plotted.
%
% See also:  BEDS, BEDSgetSpikes, histc.

% RCS:$Id: bsIsih.m,v 1.6 2005/01/19 03:41:41 bjorn Exp $
% Written by GBC, 21 May 2001.
% Based on code written by Maneesh Sahani and John Pezaris.

% default the arguments
color = num2cell(get(gca, 'colororder'), 2);

if (nargin < 2) bins = 0:.1:5;   		end
if (nargin < 3)
  if nargout==0
	plotflag=1;
  else
	plotflag=0;
  end
end

% Because I'm lazy, any string in bins will result in the
% default binning behaviour.
if isstring(bins)
  bins = 0:.1:5;
end


ymax = -inf;
ymin = inf;

numtrials = length(spikes(:,1));
numcells = length(spikes(1,:));
numbins = length(bins);
 

if ((numcells > 1) & (nargout > 0))
  error(['Only one cell can be specified with output' ...
	 ' arguments.']);
end


for c = 1:numcells
  
  h  = zeros (numtrials,numbins); % histogram of counts

  for t = 1:numtrials
    evt = diff(spikes{t,c});
    if length(evt)
      [dh, x] = histc(evt', bins);
      % row/col orientation of dh is unpredictable for
      % small lengths of evt (ie, 2 or less).  So we need
      % to check explicitly.
      dhsize=size(dh);
      if dhsize(1) > dhsize(2)
	h(t,:)  = dh';
      else
	h(t,:) = dh;
      end
    end
  end
  
  histcount = sum(h);
  if nargout > 0
    histcnt = histcount;
  end
    
  if plotflag
	if (iscell(color))
	  cell_color = color{1 + mod(c - 1, length(color))};
	else
	  cell_color = color;
	end
	set(gca, 'ColorOrder', cell_color, 'NextPlot', ...
			 'add');
	set(gca, 'NextPlot', 'add');
	handle = bar(bins,histcount,'histc');
	set(handle,'FaceColor',cell_color);
	ymin = min(ymin, min(histcount));
	ymax = max(ymax, max(histcount));
  end

end

if plotflag
  ylim = [ymin - 0.1 * (ymax - ymin), ymax + 0.1 * (ymax - ymin)];
  if (sum(isinf(ylim)) > 0) ylim = [0 1]; end;
  set(gca, 'ylim', ylim);
end


