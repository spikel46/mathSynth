function [vspo,vsph,p] = bsVectorStrength(phases);

% bsVectorStrength          Compute trial-based vector strength.
%
% [PO,PH,P]=BSVECTORSTRENGTH(PHASES) will compute the vector
% strength power PO and phase PH of the phases in the cell
% array PHASES (as per the output of bsGetPhases), as well
% as the p-value of the power according to Rayleigh's Test.
% Each output array will have one entry per entry in PHASES.
%
% Note: to get the behaviour of XDPHYS' vector strength, you
% need to concatenate all the phases returned by bsGetPhases
% into a single trial, first (or use bsSumVectorStrength).
%
% See also: BEDS, bsGetPhases, bsRayleighTest, bsSumVectorStrength.

% Written by GBC, 5 Oct 2001.
% RCS: $Id: bsVectorStrength.m,v 1.7 2003/02/01 07:30:09 bjorn Exp $
% Depends on:  spiketools/bsRayleighTest.m

if nargout < 3
  rayleigh = 0;
else
  rayleigh = 1;
end

for n=1:length(phases)
  vs = sum(cos(phases{n})) + i*sum(sin(phases{n}));
  N = length(phases{n});
  vspo(n) = abs(vs) / N;
  vsph(n)=angle(vs);
  if rayleigh; p(n) = bsRayleighTest(N,vspo(n)); end;
end
