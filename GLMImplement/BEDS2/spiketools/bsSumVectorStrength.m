function [vspo,vsph,p] = bsSumVectorStrength(data,trial,channel,timewin,noconv);

% bsSumVectorStrength          Compute summed vector strength.
%
% [PO,PH,P]=BSSUMVECTORSTRENGTH(DATA) will compute the average
% vector strength power PO and phase PH over all trials in
% DATA, as well as the p-value of the power according to
% Rayleigh's Test.
%
% [...]=BSSUMVECTORSTRENGTH(DATA,TRIAL) will do the same for
% the subset of trials specified in TRIAL.  Data from all
% trials will be returned if TRIAL is set to 'all'.
%
% [...]=BSSUMVECTORSTRENGTH(DATA,TRIAL,CHAN) will do the same, but
% retrieving from channels CHAN, where CHAN is a vector list
% of desired channels.  If CHAN is non-scalar, then the data
% will be returned in a two-dimensional cell array, with the
% first index being trial and the second being channel.
%
% [...]=BSSUMVECTORSTRENGTH(...,TIMEWIN) will do the same, but
% only return data on spikes in the time window TIMEWIN.
% TIMEWIN can be scalar, in which case the spikes between
% stimulus onset and stimulus onset plus TIMEWIN will be
% considered; or a vector of the form [lowertime uppertime].
%
% Note that cycles are defined with respect to "periods
% since stimulus onset," rather than with respect to
% "zero-crossings of the stimulus".  In other words, the
% first cycle is always of length (1/freq).
%
% NOTE:  THIS FUNCTION DOES NOT CHECK THAT ALL TRIALS
% WERE DONE WITH RESPECT TO THE SAME FREQUENCY.  You must
% check this yourself, or your results may be
% meaningless.
%
% See also: BEDS, bsGetPhases, bsRayleighTest, bsVectorStrength..

% Written by GBC, 28 Sep 2002.
% RCS: $Id: bsSumVectorStrength.m,v 1.4 2003/07/23 20:21:26 bjorn Exp $
% Depends on:  spiketools/bsGetPhases.m
%              spiketools/bsVectorStrength.m
%              bedstools/iBEDSdefaultArgs.m
%              bedstools/iBEDStrialChanSetup.m
%              bedstools/iBEDScheckTimewin.m
%              bedstools/BEDSchannelType.m
%              bedstools/BEDSupdatedTov3.m

BEDSCOMPAT = 4;

if ~isBEDS(data)
  error('DATA must be a BEDS structure');
else
  if data.BEDSversion < BEDSCOMPAT
    data = BEDSupdateTov4(data);
  end
end

% Set default argument values.
if (nargin < 2);trial=iBEDSdefaultArgs('trial');end;
if (nargin < 3);channel=iBEDSdefaultArgs('chan');end;
if (nargin < 4);timewin=iBEDSdefaultArgs('timewin');end;
if (nargin < 5);noconv=0;end;

% Sanity checks.

[trial,channel]=iBEDStrialChanSetup(data,trial,channel,...
									BEDSchannelType('CHAN_SPIKES'));
timewin=iBEDScheckTimewin(data,timewin,'dataonly');

phases=bsGetPhases(data,trial,channel,timewin,noconv);
phases={[phases{:}]};

if nargout < 3
  [vspo,vsph]=bsVectorStrength(phases);
else
  [vspo,vsph,p]=bsVectorStrength(phases);
end
