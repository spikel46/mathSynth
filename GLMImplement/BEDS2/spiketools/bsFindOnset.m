function [onsetTime,onsetCount]=bsFindOnset(data,timewin,trial,channel,cumul)
% bsFindOnset               Find the response onset in a spike train.
%
% "Onset", in this case, is defined as the the first time
% bin during the stimulus in which the spike count exceeds
% the maximum spike count in the pre-stimulus period, and
% whose following time bin also exceeds that maximum.
%
% [T,C] = BSFINDONSET(DATA) will return the onset time T
% (relative to the beginning of the stimulus) and the count
% of spikes in the relevant bin C from the BEDS data file
% DATA for all trials in channel 1, using a time window of 5
% ms for the binning.  The value T is returned in
% milliseconds from beginning of the trial.
%
% [T,C] = BSFINDONSET(...,TW) does the same, but lets you
% specify the time window width TW in ms.
%
% [T,C] = BSFINDONSET(...,TW,TR) does the same, but lets
% you specify the trial subset to use.  If TR is 'all',
% then all trials will be used.
%
% [T,C] = BSFINDONSET(...,TW,TR,CH) also allows you to
% specify the channels CH.
%
% [T,C] = BSFINDONSET(...,TW,TR,CH,CU) does the same, but
% if CU is 1, then the onset will be computed using the
% cumulative histogram, rather than on a trial-by-trial
% basis.  By default, CU is 0.
%
% See also:  BEDS.

% RCS: $Id: bsFindOnset.m,v 1.6 2003/07/08 23:00:46 bjorn Exp $
% Written by GBC, 20 Sep 2001.
% Depends on:  bedstools/BEDSgetEpoch.m
%              bedstools/iBEDSdefaultArgs.m
%              bedstools/iBEDStrialChanSetup.m
%              bedstools/BEDSgetSpikes.m
%              spiketools/bsPsth.m
%              misc/isstring.m

% Set defaults.
if nargin < 2; timewin = 5;                     end;
if nargin < 3; trial=iBEDSdefaultArgs('trial'); end;
if nargin < 4; channel=iBEDSdefaultArgs('chan');end;
if nargin < 5; cumul = 0;                       end;

[trial,channel]=iBEDStrialChanSetup(data,trial,channel, ...
									BEDSchannelType('CHAN_SPIKES'));

% Extract the trial timing information, and get the
% pre-stimulus and stimulus spike counts.  We set up the
% bin edges at this point as well.  Note that click
% trials will have a duration of 0.  For the purposes of
% finding response onset, this is bollocks, so we skip
% around that problem.
[epoch,duration,delay] = BEDSgetEpoch(data);
[junk,success]=hashFind(data.params,'click.Side');
if success
  duration=epoch-delay;
end
prestim = BEDSgetSpikes(data,trial,channel,[0,delay]);
stim = BEDSgetSpikes(data,trial,channel,duration);
if cumul
  prestim = {sort(cat(2,prestim{:}))};
  stim = {sort(cat(2,stim{:}))};
  trial = 1;
  channel = 1;
end
prestimBins = 0:timewin:delay;
stimBins = delay:timewin:delay+duration;

onsetTime =  zeros(length(trial),length(channel));
onsetCount = zeros(length(trial),length(channel));

for ch = 1:length(channel)
  for tr = 1:length(trial)
    % Find maximum spike count in prestim.
    prestimCount = bsPsth(prestim(tr,ch),prestimBins);
    maxPrestim = max(prestimCount);
    
    % Get bin counts in stim, and create its shifted counterpart.
    stimCount = bsPsth(stim(tr,ch),stimBins);
    shiftedStimCount = [stimCount(2:end) 0];
    
    % Find onset.
    onsetBin = find((stimCount > maxPrestim) & ... 
                    (shiftedStimCount > maxPrestim));
    
    % Convert the onsetBin to a time value, and handle
    % the case where there was no response onset.
    if isempty(onsetBin)
      onsetTime(tr,ch) = 0;
      onsetCount(tr,ch) = 0;
    else
      onsetTime(tr,ch) = onsetBin(1)*timewin;
      onsetCount(tr,ch) = stimCount(onsetBin(1));
    end
  end
end
