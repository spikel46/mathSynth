function bsPhaseHist(data,binwidth,trial,channel,timewin,plottype);
% bsPhaseHist       Plot a histogram of spike phases.
%
% BSPHASEHIST(DATA) will plot a histogram of the number
% of spikes versus the phase of the tone at which they
% fired.  By default, only the spikes that fired during
% the stimulus will be considered, and the binwidth for
% the histogram is pi/10.
%
% BSPHASEHIST(DATA,BIN) does the same, but allows you to
% specify the bin width.
%
% BSPHASEHIST(DATA,BIN,TR,CHAN) does the same, but
% allows you to specify the trials and channel to be
% extracted (as in BEDSgetSpikes).  The default channel
% is 1; if TR is set to 'all', all trials will be used.
%
% BSPHASEHIST(DATA,BIN,TR,CH,TIMEWIN) allows you to specify
% the window of analysis.  TIMEWIN can be scalar, in which
% case the spikes between stimulus onset and stimulus onset
% plus TIMEWIN will be considered; or a vector of the form
% [lowertime uppertime].  The default time window is the
% stimulus period.
%
% BSPHASEHIST(...,PLOTTYPE) will plot a cumulative
% histogram if PLOTTYPE is 'cumul', a per-trial histogram
% if PLOTTYPE is 'trial', and a per-trial histogram with
% errorbars if PLOTTYPE is 'err'.
%
% See also: BEDS,bsGetPhases.

% RCS: $Id: bsPhaseHist.m,v 1.11 2003/07/08 23:00:47 bjorn Exp $
% Written by GBC, 23 May 2001.
% Depends on:  spiketools/bsGetPhases.m
%              bedstools/iBEDSdefaultArgs.m
%              bedstools/iBEDStrialChanSetup.m
%              bedstools/iBEDScheckTimewin.m
%              bedstools/BEDSchannelType.m
%              bedstools/BEDSgetEpoch.m
%              bedstools/BEDSupdateTov4.m
%              bedstools/BEDSfindNonspont.m
%              misc/isstring.m

BEDSCOMPAT = 4;

if ~isBEDS(data)
  error('DATA must be a BEDS structure');
else
  if data.BEDSversion < BEDSCOMPAT
    data = BEDSupdateTov4(data);
  end
end

% Set default argument values.
if (nargin < 2);binwidth=pi/10;end;
if (nargin < 3);trial=iBEDSdefaultArgs('trial');end;
if (nargin < 4);channel=iBEDSdefaultArgs('chan');end;
if (nargin < 5);timewin=iBEDSdefaultArgs('timewin');end;
if (nargin < 5);plottype='cumul';end;

% Sanity checks.
[trial,channel]=iBEDStrialChanSetup(data,trial,channel,...
									BEDSchannelType('CHAN_SPIKES'));
timewin=iBEDScheckTimewin(data,timewin,'dataonly');

if strncmp(plottype,'cu',2)
  plottype = 1;
elseif strncmp(plottype,'tr',2)
  plottype = 2;
elseif strncmp(plottype,'er',2)
  plottype = 3;
else
  error('Unknown value for PLOTTYPE');
end

phases = bsGetPhases(data,trial,channel,timewin);

% If cumulative histogram, take phases to a single
% array. 
if plottype == 1
  phases = cat(2,phases{:});
  count = histc(phases,0:binwidth:2*pi);
else
  tmpcount = [];
  for n = 1:length(phases)
    if ~isempty(phases{n})
      tmpcount(n,:)=histc(phases{n},0:binwidth:2*pi);
    else
      tmpcount(n,:) = zeros(1,length(0:binwidth:2*pi));
    end
  end
  count = mean(tmpcount);
end

bar(0:binwidth:2*pi,count,'histc');

if plottype == 3
  scount = std(tmpcount);
  hold on
  errorbar(binwidth/2:binwidth:2*pi-binwidth/2,count(1: ...
						  end-1),scount(1:end-1),'.'); 
  hold off
end

coord = axis;
axis([0 2*pi coord(3) coord(4)]);
xlabel('Phase');
if plottype == 1
  ylabel('Spike count');
else
  ylabel('Spikes per trial');
end






