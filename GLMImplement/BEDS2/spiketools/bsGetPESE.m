function [PESE,samprate]=bsGetPESE(data,window,trial,chan,timewin,shuffle)

% bsPESE           Get the Pre-Event Stimulus Ensemble.
%
% PESE=BSGETPESE(DATA,WIN) will calculate the Pre-Event
% Stimulus Ensemble PESE of the data in the BEDS data
% structure DATA, consisting of WIN milliseconds
% before each event.  PESE will be a cell array, one cell
% per trial, and each cell containing a NxM matrix, where
% N is the number of stimuli channels and M are the
% number of samples that make up WIN.
%
% [PESE,SR]=BSGETPESE(DATA,WIN) returns the sampling rate
% SR.  Currently, this function only supports a uniform
% sampling rate of all stimuli, so SR will be scalar.
%
% [...]=BSGETPESE(DATA,WIN,TR,CH,TW) will allow you to
% specify the trials TR, channels CH, and timewindow TW
% to use.
%
% See also: bsRevcorr

% Written by GBC, 8 Jul 2003.
% RCS: $Id: bsGetPESE.m,v 1.6 2004/09/27 05:18:54 bjorn Exp $
% Depends on:  bedstools/isBEDS.m
%              bedstools/BEDSupdateTov4.m
%              bedstools/iBEDSdefaultArgs.m
%              bedstools/iBEDStrialChanSetup.m
%              bedstools/iBEDScheckTimewin.m
%              bedstools/BEDSchannelType.m
%              bedstools/BEDSgetStimulus.m
%              bedstools/BEDSgetSpikes.m
  
BEDSCOMPAT = 4;

if ~isBEDS(data)
  error('DATA must be a BEDS structure');
else
  if data.BEDSversion < BEDSCOMPAT
    data = BEDSupdateTov4(data);
  end
end

% Set default argument values.
if (nargin < 2);error('Need a WIN value');end;
if (nargin < 3);trial=iBEDSdefaultArgs('trial');end;
if (nargin < 4);chan=iBEDSdefaultArgs('chan');end;
if (nargin < 5);timewin=iBEDSdefaultArgs('timewin');end;
if (nargin < 6);shuffle=0;end

% Sanity checks.
[trial,channel]=iBEDStrialChanSetup(data,trial,chan,...
									BEDSchannelType('CHAN_SPIKES'));
timewin=iBEDScheckTimewin(data,timewin,'dataonly');


% Get the saved stimuli.  If there are none, then there's
% nothing to do.
[stims,saved,wavehz]=BEDSgetStimulus(data,trial);
savedIdx=find(saved==1);
if isempty(savedIdx)
  error('No saved stimuli available');
end
numStimChans=size(stims,2);
if shuffle
  numStims=size(stims,1);
  stims(1:numStims,1:2)=stims(randperm(numStims),1:2);
end

% Just about what the error message says, yup.
if ~isempty(find(wavehz(savedIdx)~=wavehz(savedIdx(1))))
  error(['More than one sampling rate for saved stimuli!' ...
		 ' Programmer was lazy and I can''t do this!']);
else
  samprate=wavehz(savedIdx(1));
end

% Convert the window of integration into a number of samples.
sampleWin=round(window/1000*samprate);

spikes=BEDSgetSpikes(data,trial,'search',timewin);
spikesize=size(spikes);
if (spikesize(2)>1)
  error('Too many spike trains -- how can I choose?');
end

% One note here:  as it stands right now, always returns
% exactly two stimulus channels.  Therefore, we will
% always do reverse correlation on exactly two stimulus
% channels.  This is a bad habit, but what the hell.
PESE={};
N=ones(1,numStimChans);
% Note that it is conceptually possible to remove the
% outer for loop, but in doing so, the matrices involved
% become so large that time is not saved.
evtcnt=0;
for t=1:length(trial)
  if saved(t)==1
	% Convert from msec to sample number, and only
    % consider events where we can obtain the full window.
	event=round(spikes{t}/1000*samprate)';
	event=event(event>sampleWin);
	evtcnt=evtcnt+length(event);
	if ~isempty(event)
	  % The idea here is to create a matrix whose (i,j)th
	  % element is the index of the
      % (length(sampleWin)-j)th
	  % sample preceding the ith spike.  Then we collapse
	  % this into a vector, retrieve all the appropriate
	  % stimulus samples, and then reshape it into a
      % matrix
	  % with (#spikes) rows and length(samplewin)
      % columns.
	  sampleVector=[-sampleWin:0]';
	  sampleMatrix=sampleVector(:,ones(length(event),1));
	  eventMatrix=event(:,ones(length(sampleVector),1));
	  indices=eventMatrix'+sampleMatrix;
	  indices=indices(:);
	  for n=1:numStimChans
		stim=stims{t,n};
		if ~isempty(stim)
		  temp=reshape(stim(indices(:)), ...
					   length(sampleVector), ...
					   length(event));
		  PESE{n}(N(n):N(n)+length(event)-1,:)=temp';
		  N(n)=N(n)+length(event);
		end
	  end
	end
  end
end
