function [revcorr,errbars,PESE,samprate]=bsRevcorr(data,window,trial,chan,timewin)

% bsRevcorr                Compute a reverse correlation
% 
% [REV,ERR,PESE,SR]=BSGETPESE(DATA,WIN) will calculate
% the reverse correlation REV (with errorbars ERR) on the
% pre-event stimulus ensemple PESE for the data in the
% BEDS data structure DATA, using the WIN milliseconds
% before each event.  SR is the sampling rate of the
% stimuli, and hence REV.  REV will be a NxM matrix,
% where N is the number of stimuli channels, and M is the
% number of samples in WIN.
%
% [...]=BSREVCORR(DATA,WIN,TR,CH,TW) will allow you to
% specify the trials TR, channels CH, and timewindow TW
% to use.
%
% See also: bsGetPESE.


% Written by GBC, 8 Jul 2003.
% RCS: $Id: bsRevcorr.m,v 1.2 2003/08/04 16:53:53 bjorn Exp $
% Depends on:  bedstools/isBEDS.m
%              bedstools/BEDSupdateTov4.m
%              bedstools/iBEDSdefaultArgs.m
%              bedstools/iBEDStrialChanSetup.m
%              bedstools/iBEDScheckTimewin.m
%              bedstools/BEDSchannelType.m
%              spiketools/bsGetPESE.m  
BEDSCOMPAT = 4;

if ~isBEDS(data)
  error('DATA must be a BEDS structure');
else
  if data.BEDSversion < BEDSCOMPAT
    data = BEDSupdateTov4(data);
  end
end

% Set default argument values.
if (nargin < 3);trial=iBEDSdefaultArgs('trial');end;
if (nargin < 4);chan=iBEDSdefaultArgs('chan');end;
if (nargin < 5);timewin=iBEDSdefaultArgs('timewin');end;

% Sanity checks.
[trial,channel]=iBEDStrialChanSetup(data,trial,chan,...
									BEDSchannelType('CHAN_SPIKES'));
timewin=iBEDScheckTimewin(data,timewin,'dataonly');

% The hard part is done by bsGetPESE.
[PESE,samprate]=bsGetPESE(data,window,trial,chan,timewin);

revcorr=[];
errbars=[];
for n=1:length(PESE)
  revcorr(n,:)=mean(PESE{n});
  errbars(n,:)=std(PESE{n});
end