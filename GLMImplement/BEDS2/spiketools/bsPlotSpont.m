function [Omean,Ostd] = bsPlotSpont(data,spontHow,channel,plotflag,timewin)

% bsPlotSpont          Plot spontaneous firing rates.
%
% BSPLOTSPONT(DATA,HOW) will plot spontaneous firing
% rates for the data in the BEDS structure DATA.  HOW
% specifies how to obtain the firing rates:  "trial" will
% use spontaneous trials, 'pre' will use the delay
% interval; 'best' will use trials if available, or delay
% period otherwise; and 'win' will use a user-specified
% time window.  By default, HOW is 'best'.
%
% BSPLOTSPONT(DATA,HOW,CH) lets you specify which channels
% to use.  Note that if HOW is "trial," this parameter, if
% set, will be ignored.
%
% BSPLOTSPONT(DATA,HOW,CH,PLOT) will allow you to choose whether
% to plot or not.  By default, PLOT is 1 (ie, plot),
% unless there are output arguments, in which case the
% default is 0 (no plot).
%
% BSPLOTSPONT(DATA,HOW,CH,PLOT,WIN) does all of the
% above, but lets you specify the timewindow for option
% 'win'.
%
% [M,S] = BSPLOTSPONT(...) does the above, but returns
% the mean spontaneous rate M and the standard deviation
% S.
%
% Note that these are not normalized firing rates.
%
% See also:  BEDS, bsPlotDepvar, bsPlotStimParam.

% RCS: $Id: bsPlotSpont.m,v 1.5 2003/09/23 00:18:10 bjorn Exp $
% Written by GBC, 1 Oct 2001.
% Depends on:  bedstools/isBEDS.m
%              bedstools/iBEDSdefaultArgs.m
%              bedstools/iBEDStrialChanSetup.m
%              bedstools/iBEDScheckTimewin.m
%              bedstools/BEDSchannelType.m
%              bedstools/BEDSupdateTov3.m
%              bedstools/BEDSgetEpoch.m
%              bedstolls/BEDSfindSpont.m
%              spiketools/bsPlotDepvar.m
%              spiketools/bsPlotStimParam.m
%              misc/isstring.m

BEDSCOMPAT = 4;

if ~isBEDS(data)
  error('DATA must be a BEDS structure');
end

if BEDSCOMPAT > data.BEDSversion
  data = BEDSupdateTov4(data);
end

% Set default argument values.
if (nargin < 2);spontHow = 'best';end;
if (nargin < 3);channel=iBEDSdefaultArgs('chan');end;
if (nargin < 4)
  if nargout > 0
    plotflag = 0;
  else
    plotflag = 1;
  end
end

% Convert to numbers.
if isstring(spontHow)
  spontHow = lower(spontHow);
  if strncmp(spontHow,'bes',3)
    spontHow = 0;
  elseif strncmp(spontHow,'tri',3)
    spontHow = 1;
  elseif strncmp(spontHow,'pre',3)
    spontHow = 2;
  elseif strncmp(spontHow,'win',3)
    spontHow = 3;
  else
    error('Unknown setting for HOW');
  end
else
  error('HOW must be a string');
end

if (nargin < 5) & (spontHow == 3)
  error('Must provide timewindow!');
end

spontTrials = BEDSfindSpont(data);

% Decides which case 'best' falls through to.
if (spontHow == 0)
  if isempty(spontTrials)
    spontHow = 2;
  else
    spontHow = 1;
  end
end

% Argument checking.  We had to first resolve the 'best'
% case, as that changes the sort of channel we look for.
if (spontHow == 1)
  [junk,channel]=iBEDStrialChanSetup(data,'all',channel,...
									BEDSchannelType('CHAN_EMPTY'));
else
  [junk,channel]=iBEDStrialChanSetup(data,'all',channel,...
									BEDSchannelType('CHAN_SPIKES'));
end

% 'trial' case.
if (spontHow == 1)
  if isempty(spontTrials)
    error('No spontaneous trials!');
  end
  [junk,smean,sstd]=bsPlotStimParam(data,'spont',[0 epoch], ... 
				       spontTrials); 
end

% 'pre' case: set a timewindow and go to 'win' case.
if (spontHow == 2)
  timewin = 'default';
  spontHow = 3;
end

% 'win' case.
if (spontHow == 3);
  if (length(timewin) == 1)
	timewin = iBEDScheckTimewin(data,timewin,'spont');
  end
  [junk,smean,sstd,counts]=bsPlotDepvar(data,timewin,channel);
  cntsize = size(counts);
  counts = reshape(counts,cntsize(1)*cntsize(2),1);
  smean = mean(counts);
  sstd = std(counts);
end

if plotflag
  axisends = axis;
  hold on;
  errorbar([axisends(1) axisends(2)],[smean smean],[sstd sstd]);
  hold off;
end

if nargout
  Omean = smean;
  Ostd = sstd;
end
