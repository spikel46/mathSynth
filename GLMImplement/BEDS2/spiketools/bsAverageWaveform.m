function wave = bsAverageWaveform(data,trial,chan,avgtype)

% bsAverageWaveform average waveform data from a BEDS protocol
%                   structure
%
% W=BSAVERAGEWAVEFORM(DATA) will retrieve the waveform data
% (from all valid channels) of all trials and return the
% mean-averaged waveform in W.
%
% W=BSAVERAGEWAVEFORM(DATA,TRIAL) will average waveform data
% from the subset of trials specified in TRIAL.  Data
% from all trials will be returned if TRIAL is set to 'all'.
%
% W=BSAVERAGEWAVEFORM(DATA,TRIAL,CHAN) will do the same, but
% retrieving from channels CHAN, where CHAN is a vector
% list of desired channels.  If CHAN is non-scalar, then
% the data will be returned in a two-dimensional cell
% array, with the first index being trial and the second
% being channel.
%
% W=BSAVERAGEWAVEFORM(...,TYPE) will do the same, but also
% allows you to specify the type of averaging to be used:
% 'mean', 'median', or 'default' (which is the same as
% 'mean'.)
%
% See also:  BEDS, BEDSgetWaveform

% Written by GBC, 26 Apr 2002.
% RCS: $Id: bsAverageWaveform.m,v 1.4 2003/07/08 23:00:46 bjorn Exp $
% Depends on:  bedstools/isBEDS.m
%              bedstools/BEDSgetWaveform
%              bedstools/iBEDSdefaultArgs.m
%              bedstools/iBEDStrialChanSetup.m

% Current Bugs:
%   This script makes an assumption that the waveforms in
%   BEDS structures are always column vectors.  I can't
%   think of an easy/computationally inexpensive way to
%   work around this assumption yet, so instead we just
%   check if we've been bitten.
  
% Make sure we have a BEDS structure.
if ~isBEDS(data)
  error('DATA must be a BEDS protocol structure.');
end

% Set default argument values.
if (nargin < 2);trial=iBEDSdefaultArgs('trial');end;
if (nargin < 3);channel=iBEDSdefaultArgs('chan');end;
if (nargin < 4);avgtype='default';end;

[trial,chan]=iBEDStrialChanSetup(data,trial,chan, ...
								 BEDSchannelType('CHAN_ANALOG')); 


% Get waveforms.  We'll try to save time by doing the
% unitconversion ourself after the averaging -- need to
% check if we can do this first!
[waveforms,wavehz,waveconv]=BEDSgetWaveform(data,trial, ...
											chan,0);

% If this is the degenerate case of a single waveform,
% return without averaging.
wavesize=size(waveforms);
if max(wavesize)==1
  wave = waveforms{1}*waveconv(1,1,1)+waveconv(1,2,1);
  return;
end

if length(unique(wavehz))==1 & length(unique(waveconv))==1
  needconv = 1;
else
  needconv = 0;
  tr=wavesize(1);ch=wavesize(2);
  for n=1:tr
	for nn=1:ch
	  waveforms{n,nn}=waveforms{n,nn}*waveconv(n,1,nn)+waveconv(n,2,nn);
	end
  end
end

% Reshape into a column vector of cells, and then a
% matrix where each waveform is a single row.
waveforms=reshape(waveforms,wavesize(1)*wavesize(2),1);
waveforms=[waveforms{:}]';

% We've already made sure there's more than one waveform,
% so if we have a vector, rather than an array, then
% either all of the waveforms were of length one (a case
% which we'll assume never happens), or the waveforms
% were row vectors.
if min(size(waveforms))==1
  error(['Waveform dimension assumption violated -- contact' ...
		 ' script author']);
end

if strncmp(avgtype,'mea',3) | strncmp(avgtype,'def',3)
  wave=mean(waveforms);
elseif strncmp(avgtype,'med',3)
  wave=median(waveforms);
else
  error('Unsupported averaging type')
end

if needconv
  wave=wave*waveconv(1,1,1)+waveconv(1,2,1)
end
