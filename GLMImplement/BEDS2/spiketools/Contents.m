%Analysis functions for BEDS files
%
%  Spike analysis tools
%    bsAverageWaveform   Average analog waveforms.
%    bsPlotDepvar        Plot spike counts vs depvar.
%    bsPlotStimParam     Plot spike count vs. stim param.
%    bsPlotSpont         Plot spontaneous rates.
%    bsPsth              Plot a peristimulus time histogram.
%    bsIsih              Plot an inter-spike interval histogram.
%    bsRaster            Plot a raster.
%    bsPhaseFreq         Plot frequency vs. mean phase of firing.
%    bsPhaseHist         Plot histogram of mean phase of firing.
%    bsSpikeOverlay      Plot overlapping spike waveforms.
%    bsFindOnset         Find response onset.
%    bsRayleighTest      Compute Rayleigh's Test.
%    bsGetPhases         Get the phase of spikes.
%    bsVectorStrength    Compute vector strength.
%    bsSonogram          Plot a sonogram.