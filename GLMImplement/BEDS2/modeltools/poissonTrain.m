function train=poissonTrain(rate,interval,refract,ratecomp)
% poissonTrain         Generate a Poisson train.
%
% TR=POISSONTRAIN(MU,LE) will generate a Poisson train with
% rate paramter MU, over an interval LE.  LE can either be
% scalar, in which case it is the presumed length of the
% interval, or it can be of the form [BEGIN END].
%
% TR=POISSONTRAIN(...,REFRACT) will generate a spike
% train with a refractory period of length REFRACT.
%
% It is the job of the user to make sure all values have
% equivalent units!
  
% Written by GBC, 2 Nov 2001.
% RCS: $Id: poissonTrain.m,v 1.8 2001/11/07 08:16:27 bjorn Exp $
  
  if nargin < 3
    refract = 0;
  end
  if nargin < 4
    ratecomp = 1;
  end
  
  if rate == 0
    train=[];
    return;
  end
  
  if length(interval) == 1
    interval = [0 interval];
  elseif length(interval) ~= 2
    error('INTERVAL must be of length 1 or 2.');
  end
  
  intervallength = interval(2)-interval(1);
  
  if ratecomp
    if refract ~=0;
      rate=rate/(1-rate*refract);
    end
  end
  
  n=1;
  train(n)=exprnd(1/rate)+interval(1);
  while train(n) < interval(2)
    n=n+1;
    train(n)=exprnd(1/rate)+train(n-1)+refract;
  end
  train=train(1:n-1);
      
