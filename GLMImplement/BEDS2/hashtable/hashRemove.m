function [newtable,success]=hashRemove(table,key)
% hashRemove         Remove a value from a hash table.
%
% NTABLE = hashRemove(TABLE,KEY) will invalidate the
% entry KEY in table TABLE, and return the new table.  If
% KEY does not exist in the table, the old table is
% returned.  KEY must be a string.
%
% [NTABLE,SUCCESS]=hashRemove(...) does the same, but
% SUCCESS will be 0 if the removal failed (the key was
% not in the table).

% Written by GBC, 01 March 2001.
% RCS: $Id: hashRemove.m,v 1.2 2001/03/20 05:07:47 bjorn Exp $
% Depends on misc/isstring.m
%            hashtable/hashFunction.m

if ~isstring(key)
  error('KEY must be a string');
end

if ~structcmp(table,hashMake)
  error('TABLE is not a hash table.')
end

newtable = table;
key = upper(key);
hashValue = hashFunction(newtable,key);

% Find either the key in question, a blank entry, or wrap
% around the hash table.
niters = 0;
while ((table.entry(hashValue+1).wasvalid~=0) & ~ ...
       (strcmp(table.entry(hashValue+1).key, key)) & ...
       (niters < table.tablesize))
  hashValue = mod(hashValue + 1,table.tablesize);
  niters = niters+1;
end

% Either remove or mark failure.
if ((table.entry(hashValue+1).valid == 0) | (niters== ...
    table.tablesize))
  success = 0;
else
  success = 1;
  newtable.entry(hashValue+1).valid = 0;
  newtable.validEntries = newtable.validEntries - 1;
end

