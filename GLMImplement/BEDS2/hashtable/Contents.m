%  Tools for a simple hash table with string keys.
%
%  hashMake          Initialize a hash table.
%  hashFunction      Return a hash value.
%  hashInsert        Insert data in a hash table.
%  hashReplace       Replace data in a hash table.
%  hashFind          Find data in a hash table.
%  hashRemove        Remove data from a hash table.
%  hashListKeys      Get a listing of all valid keys.

% RCS: $Id: Contents.m,v 1.2 2001/04/25 18:08:09 bjorn Exp $
