function [newtable,success] = hashReplace(table,key,data)
% hashReplace       Replace an element into a hash table.
%
% NTABLE = hashReplace(TABLE,KEY,DATA) will replace the
% the current value of KEY with new value DATA.  If KEY
% does not currently exist in the table, it will be
% inserted. 
%
% [NTABLE,SUCCESS] = hashReplace(...) will do the same,
% but SUCCESS will be 0 if the attempt failed (if the
% table is full). 

% Written by GBC, 12 March 2001.
% RCS: $Id: hashReplace.m,v 1.2 2001/03/20 05:07:47 bjorn Exp $
% Depends on misc/isstring.m
%            hashtable/hashInsert.m
%            hashtable/hashRemove.m


if ~isstring(key)
  error('KEY must be a string')
end

if ~structcmp(table,hashMake)
  error('TABLE is not a hash table.')
end

key = upper(key);
[newtable,success] = hashInsert(table,key,data);

if (~success)
  if (newtable.tablesize ~= newtable.validEntries)
    [newtable,success]=hashRemove(table,key);
    if (~success)
      return;
    end
    [newtable,success]=hashInsert(newtable,key,data);
  end
end


