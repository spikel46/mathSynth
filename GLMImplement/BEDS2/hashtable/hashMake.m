function table = hashMake(initsize)
% hashMake        Initialize a hash table
%
% TABLE = hashMAKE(SIZE) will initialize a new hash table
% of size SIZE.  If size is not provided, it defaults to
% 691. 

% Written by GBC, 01 March 2001.
% RCS: $Id: hashMake.m,v 1.2 2001/03/20 05:07:47 bjorn Exp $

if nargin == 0
  initsize = 691;
end

% .valid is if the entry is currently used, .wasvalid is
% if the entry is in use or ever has been.  (So we don't
% lose entries when we delete keys.)
entryTemplate = struct('valid',0,'wasvalid',0,'key','', ...
		       'data',[]);

table = struct('tablesize',initsize,'validEntries',0,'entry', ...
	       repmat(entryTemplate,initsize,1)); 