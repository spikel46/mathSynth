function [newtable,success] = hashInsert(table,key,data)
% hashInsert       Insert an element into a hash table.
%
% NTABLE = hashInsert(TABLE,KEY,DATA) will insert a value
% DATA into location KEY in hash table TABLE, and return
% the new table.  KEY must be a string.
%
% [NTABLE,SUCCESS] = hashInsert(...) will do the same,
% but SUCCESS will be 0 if the attempt failed (if the
% table is full or if the key already exists in the
% table). 

% Written by GBC, 01 March 2001.
% RCS: $Id: hashInsert.m,v 1.2 2001/03/20 05:07:47 bjorn Exp $
% Depends on misc/isstring.m
%            hashtable/hashFind.m
%            hashTable/hashFunction.m

if ~isstring(key)
  error('KEY must be a string')
end

if ~structcmp(table,hashMake)
  error('TABLE is not a hash table.')
end

newtable = table;
key = upper(key);

if (newtable.validEntries == newtable.tablesize)
  % Table is full.
  success =  0;
else
  [junkdata,inTable] = hashFind(newtable,key);
  if (inTable)
    % Key already exists in table.
    success = 0;
  else
    hashValue = hashFunction(newtable,key);
    
    % If hashValue occupied, skip ahead to find first
    % available slot.
    while (newtable.entry(hashValue+1).valid ~= 0)
      hashValue = mod(hashValue + 1,newtable.tablesize);
    end
    
    newtable.entry(hashValue+1).valid = 1;
    newtable.entry(hashValue+1).wasvalid = 1;
    newtable.entry(hashValue+1).key = key;
    newtable.entry(hashValue+1).data = data;
    newtable.validEntries = table.validEntries + 1;
    success = 1;
  end
end

