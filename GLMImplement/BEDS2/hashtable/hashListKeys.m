function keys = hashListKeys(table,verbose);
% hashListKeys         List all valid keys in a hash table
%
% K = HASHLISTKEYS(TABLE) will return all valid keys in
% the table TABLE in a cell array.  At the same time, all
% the keys will be printed to console.
%
% K = HASHLISTKEYS(TABLE,VERBOSE) will do the same, but
% printing the keys will be disable if VERBOSE is 0.  By
% default, VERBOSE is 1.

% Written by GBC, 25 Apr 2001.
% RCS: $Id: hashListKeys.m,v 1.1 2001/04/25 18:07:36 bjorn Exp $
% Depends on hashtable/hashMake.m
%            misc/structcmp.m

if ~structcmp(table,hashMake)
  error('TABLE is not a valid hash table.');
end

if nargin == 1
  verbose = 1;
end

valid = 0;
keys = {};
for n = 1:table.tablesize
  if (table.entry(n).valid)
    valid = valid + 1;
    keys{valid} = table.entry(n).key;
    if verbose
      disp(table.entry(n).key);
    end
  end
end

    
  