function [data,success] = hashFind(table,key,castType)
% hashFind         Find an entry in the hash table
%
% DATA = hashFind(TABLE,KEY) will find the entry with key
% KEY in hash table TABLE and return its value DATA, with
% DATA empty if the key was not found.  KEY must be a
% string. 
%
% DATA = hashFind(TABLE,KEY,CAST) will convert the
% returned DATA to the specified data type.  CAST can be
% any of 'str' (cast data to string), 'num', (cast data
% to double), or 'whatever' (same as not specifiying
% CAST). 
% 
% [DATA,SUCCESS]=hashFIND(...) will do the same,
% but return 0 in SUCCESS if it failed.

% Written by GBC, 01 March 2001.
% RCS: $Id: hashFind.m,v 1.2 2001/03/20 05:07:46 bjorn Exp $
% Depends on  misc/isstring.m
%             misc/structcmp.m
%             hashtable/hashFunction.m

if ~isstring(key)
  error('KEY must be a string');
end

if ~structcmp(table,hashMake)
  error('TABLE is not a hash table.')
end

% Only use upper case letters.
key = upper(key);

hashValue = hashFunction(table,key);

% Find either the key in question, a blank entry, or wrap
% around the hash table.
niters = 0;
while ((table.entry(hashValue+1).wasvalid~=0) & ~ ...
       (strcmp(table.entry(hashValue+1).key, key)) & ...
       (niters < table.tablesize))
  hashValue = mod(hashValue + 1,table.tablesize);
  niters = niters+1;
end

% Either return or mark failure.
if ((table.entry(hashValue+1).valid == 0) | (niters== ...
    table.tablesize))
  data = [];
  success = 0;
else
  success = 1;
  data = table.entry(hashValue+1).data;
  % Handle data casting.
  if (nargin > 2)
    if ~isstring(castType)
      error('CAST must be a string.');
    elseif strncmp(castType,'str',3)
      if ~isstring(data)
	data = num2str(data);
      end
    elseif strncmp(castType,'num',3)
      if isstring(data)
	data = str2num(data);
      end
    elseif ~strncmp(castType,'whatever',8)
      error('Invalid value for CAST.')
    end
  end
end

