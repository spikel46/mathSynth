function hashValue = hashFunction(table,key)
% hashFunction      Return a hash value
%
% V = hashFunction(T,K) will return the correct hash value V
% for key K, given table T.  The key must be a string.
%
% This is an internal function for the hash routines and
% should not be called directly.

% Written by GBC, 01 March 2001.
% RCS: $Id: hashFunction.m,v 1.2 2001/03/20 05:07:47 bjorn Exp $
% Depends on misc/isstring.m

% Hash function taken from XDPhys/XDOwl, (c) 1994 Jamie
% Mazer.  

if ~isstring(key)
  error('KEY must be a string');
end

% Convert to upper case.  Doesn't really matter, I
% suppose. 
key = upper(key);

hashValue = 0;
for n = 1:length(key);
%  hashValue = hashValue + key(n)*n;
  hashValue = hashValue + key(n)*2^(n-1);
end

hashValue = mod(hashValue,table.tablesize);
