function [estfr] = estimfiringrate(spikes, time, width)

% estimates the firing rate at a certain time
% the estimate is based on data in spikes
% estimate with a width of time

%% look at that width around the time and evaluate number of spikes
numspikes = 0;

for k=1:length(spikes)
    for i=1:length(spikes{k})
        if spikes{k}(i) > time - 0.5*width
            if spikes{k}(i) < time + 0.5*width
                numspikes = numspikes + 1;
            end
        end
    end
end

%% estimate the firing rate

estfr = numspikes/(width*length(spikes));



