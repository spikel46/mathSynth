
Bsp=-exp(-.1*(0:J-1));

B0=[kls(:);Bsp(:)];

opts.w0=B0;

%% 

opts.family = 'poissexp';

%% Select quadractic penalty

%qf=ones(size(X,2),1)*.1;

%qf=diag(qf);


qf = blkdiag(qfsmooth1D(size(X,2)-1),.1);

%% Do GLM fit
results = glmfitqp(Y,X,qf,opts);

B=results.w;

%% Plot

strf=reshape(B(1:K*J),J,K);
xi=linspace(1,K,K*3);
yi=linspace(1,J,J*3);
[xi,yi]=meshgrid(xi,yi);
strf=interp2(strf,xi,yi);
cmax=max(abs(strf(:)));

figure(2);clf;
subplot(2,1,1)
imagesc(t,cf,strf.');axis xy;caxis([-cmax cmax]);colorbar
xlabel('Time (ms)','fontsize',15)
ylabel('Frequency (Hz)','fontsize',15)
title('GLM', 'fontsize', 20)

subplot(2,1,2);
t=linspace(0,Tstim,J);

plot(t,B(K*J+1:end),'linewidth',1.5)
xlabel('Time (ms)','fontsize',15)
ylabel('Spike history','fontsize',15)
