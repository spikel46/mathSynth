function [CV]=CofV(spikes, dl)

% [CV]=CofV(spikes, dl)
% assumes spikes is a cell array with a cell of spike times for each trial

%% Generate IID vector

IItimes = IIDvector(spikes, dl);

%% Get rid of negatives

newalldiffs = IItimes;
clear IItimes;
IItimes = [];

for i=1:length(newalldiffs)
    if(newalldiffs(i) > 0)
        IItimes = [IItimes newalldiffs(i)];
    end
end

%% Generate coefficient of variation

CV = std(IItimes)/mean(IItimes);