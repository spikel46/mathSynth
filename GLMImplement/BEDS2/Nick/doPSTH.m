function [PSTH,BINS]=doPSTH(spikes,Tmin,Tmax,BINSIZE)

%doPSTH(spikes,Tmin,Tmax,BINSIZE)
%Assumes spikes is a cell array with a cell of spike times for each trial

BINS=Tmin:BINSIZE:Tmax;

Ntrial=length(spikes);

PSTH=zeros(size(BINS));

for n=1:Ntrial
    sp=spikes{n};
    if ~isempty(sp)
    PSTH=PSTH+histc(sp,BINS);
    end 

    
%% sets up histogram assuming that bin width is 1ms (put space before the neuron unit string)
%bar(BINS,PSTH);
%title(titlename, 'Fontsize', 16)
%xlabel('Time (ms)')
%ylabel('Spike Count')
%xlim([-0.5, 120])
end





