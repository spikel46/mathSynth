function [f]=FanoFactor(spikes,dl)

% function [f]=FanoFactor(spikes,dl)
% generates fano factor

IItimes = IIDvector(spikes, dl);

newalldiffs = IItimes;
clear IItimes;
IItimes = [];

for i=1:length(newalldiffs)
    if(newalldiffs(i) > 0)
        IItimes = [IItimes newalldiffs(i)];
    end
end

f = var(IItimes)/mean(IItimes);