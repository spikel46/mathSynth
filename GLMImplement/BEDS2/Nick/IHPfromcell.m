function [trialcells]=IHPfromcell(spikes, backfiringrate, du, dl, trials, binsize, width)

% [trialcells] = IHPfromcell(spikes, backfiringrate, du, dl, trials, binsize, width)
% Inhomogeneous Poisson Spike Generator from firing rate estimated from a
% certain cell which has trials spikes
% du = duration
% dl = delay
% ep = end point
ep = du+dl;

%% Initialize variables

%create empty cell vector with size = # of trials
trialcells = cell(trials, 1);

spikes1 = [1,0];
spikenumber = 0;

%% Generate Spikes

for n=1:trials
    %start with the delay and generate with the background firing rate
    for k=(1 + binsize):binsize:dl
        if(rand < backfiringrate*binsize)
            spikenumber = spikenumber +1;
            spikes1(spikenumber) = k - 0.5*binsize;
        end
    end
    % now do the rest by using the inserted spikes
    for k=(dl + binsize):binsize:ep
        newfiringrate = estimfiringrate(spikes, k - 0.5*binsize, width)
        if(rand < newfiringrate*binsize)
            spikenumber = spikenumber + 1;
            spikes1(spikenumber) = k - 0.5*binsize
        end
    end
    % now store it in the trialcells
    trialcells{n} = spikes1;
    clear spikes1;
    clear spikenumber;
    spikes1 = [1,0];
    spikenumber = 0;
end