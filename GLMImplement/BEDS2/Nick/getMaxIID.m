function [maxIID] = getMaxIID(spikes, du, dl, Tmin, Tmax, BINSIZE)

%%

BINS=Tmin:BINSIZE:Tmax;

Ntrial=length(spikes);

IID=zeros(size(BINS));

%alldiffs = [];

for n=1:Ntrial
    sp=spikes{n};
    sp=sp(sp>dl);
    sp=sp(sp<dl+du);
    if ~isempty(sp)
    diffs = diff(sp);
    diffs(length(diffs) + 1) = sp(1) - dl;
    
    
    %alldiffs = [alldiffs diffs];
    IID=IID+histc(diffs,BINS);
    end 
end

[~,ind]=max(IID);

maxIID = BINS(ind) + 0.5*BINSIZE;
