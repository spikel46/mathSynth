function [IID]=IID(spikes,Tmin, Tmax, dl, BINSIZE,titlename, xmax, du)

%IID(spikes,Tmin,Tmax,BINSIZE, title)
%Assumes spikes is a cell array with a cell of spike times for each trial

BINS=Tmin:BINSIZE:Tmax;

Ntrial=length(spikes);

IID=zeros(size(BINS));


%% Takes differences of each trial, adds in the first time minus the delay,
% then adds to the IID
alldiffs = [];

for n=1:Ntrial
    sp=spikes{n};
    sp=sp(sp>dl);
    sp=sp(sp<dl+du);
    if ~isempty(sp)
    diffs = diff(sp);
    diffs(length(diffs) + 1) = sp(1) - dl;
    
    
    alldiffs = [alldiffs diffs];
    IID=IID+histc(diffs,BINS);
    end 
end

    
%% graphs IID and sets axes as well as title
bar(BINS,IID);
title(titlename, 'Fontsize', 16)
xlabel('Interval Length (ms)')
ylabel('Frequency (number of intervals)')
xlim([0, xmax])
