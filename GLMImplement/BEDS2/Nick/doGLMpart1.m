

Tstim=6;

%Downsampling
Dr=24;

%frequencies (best to center around the main area. change this for each
%neuron)
cf=4300:200:5200;
%cf=3900:300:5100;
%cf=3900:300:5700;

%% Parameters
SR=48077;

%Time
Tin=dl+15;
Tfinal=du+dl;


%%

J=ceil(Tstim*SR/Dr/1000);

%Initial component removed
I=2*J;


%% Do preprocessing
[X,R]=preprocessNMNLGLM(stimSet,spikes,J,cf,Dr,Tin,Tfinal,ep);

%% Output
Y=R(I+1:end);

%% NRC
K=length(cf);

xX=X(:,1:J*K);
XX=xX.'*xX;
k=xX.'*Y;

[U,S,V]=svd(XX);
S=diag(S);
Sinv=1./S;

Sinv(30:end)=0;
Sinv=diag(Sinv);
XXinv=V*Sinv*U';
kls=XXinv*k;


%% Plot
strf=reshape(kls(1:K*J),J,K);
xi=linspace(1,K,K*3);
yi=linspace(1,J,J*3);
[xi,yi]=meshgrid(xi,yi);
strf=interp2(strf,xi,yi);
cmax=max(abs(strf(:)));


t=linspace(0,Tstim,J*3);

figure(1);clf;
imagesc(t,cf,strf.');axis xy;caxis([-cmax cmax]);colorbar
title('NRC')
xlabel('Time (ms)','fontsize',15)
ylabel('Frequency (Hz)','fontsize',15)