function [p] = dokstestGamma(spikes, firingrate, dl, phat)

% function [h,p] = dokstest(spikes)
% do kstest on a set of spike data
% return the result along with the p-value (for certain siglevel)
% firingrate is the an average for all trials

%% Get all interspike intervals and put into alldiffs

alldiffs = [];
Ntrial = length(spikes);

for n=1:Ntrial
    sp=spikes{n};
    if ~isempty(sp)
    diffs = diff(sp);
    diffs(length(diffs) + 1) = sp(1) - dl;
    alldiffs = [alldiffs diffs];
    end 
end


%% Get rid of negatives

newalldiffs = alldiffs;
clear alldiffs;
alldiffs = [];

for i=1:length(newalldiffs)
    if(newalldiffs(i) > 0)
        alldiffs = [alldiffs newalldiffs(i)];
    end
end

%% set up cdf for kstest

alldiffs = transpose(alldiffs);

test_cdf = [alldiffs, gamcdf(alldiffs, phat(1), phat(2))];

%% run the kstest

[h, p] = kstest(alldiffs, 'CDF', test_cdf);