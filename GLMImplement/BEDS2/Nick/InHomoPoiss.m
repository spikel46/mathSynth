function InHomoPoiss(backfiringrate, stimlvl, du, dl, trials, tmin, tmax, binsize)

% Inhomogeneous poisson process spike generator
% assume there is a background firing rate (backfiringrate) and the
% stimulus intensity is stimlvl (in dB)
% du = duration
% dl = delay
% ep = end point
ep = du+dl;

%% Initialize variables
%create bins
bins = tmin:binsize:tmax;
%create empty cell vector with size = # of trials
trialcells = cell(trials, 1);

spikes = [1,0];
spikenumber = 0;

%% Generate Spikes

for n=1:trials
    %start with the delay and generate with the background firing rate
    for k=1:binsize:dl
        if(rand < backfiringrate*binsize)
            spikenumber++;
            spikes(spikenumber) = k*binsize;
        end
    end
end

        
            
    




