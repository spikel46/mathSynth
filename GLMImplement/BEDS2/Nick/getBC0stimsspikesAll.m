function [stimsL,stimsR,spikes]=getBC0stimsspikesAll(data)



%Get calibration file
cal=readXDPHYScalib('ear.cal');


% [STA,PESE,spikes,stims]=computeBC0ReverseCorrelation(data,calib)
% data = BEDS format data.  Single file only.
% calibfile = the path to the appropriate calibration file.
% side = number corresponding to the left (2) or right (3) sides
% STA = spike triggered average.
% PESE = the pre-event stimulus ensemble (ie, for each spike, the segment
% of stimulus preceding it)
% spikes = the spikes used in the reverse correlation
% stims = the stimuli used in the reverse correlation.

gatetime=5; % Chunk of the stimulus to discard because of the gate.
rcwin=10; % Time in ms to use for the reverse correlation analysis.

% Get the stimulus time parameters, and find the window of interest.
[ep,du,dl]=BEDSgetEpoch(data);
%window = [dl+gatetime du+dl-gatetime];
window = [0 ep];

% Read in the calibration and then compensate for it.
%cal=readXDPHYScalib(calibfile);
data=BEDSuncalibrateStimulus(data,cal);

%Get BC value
BCvalue = BEDSgetStimParam(data,'bc');



% Make sure there is waveform data, get its length and the sampling rate.
% This code will break if you're saving analog waveforms as well.
wavelen=0;
for n=1:length(data.trial(1).channel)
    if ~isempty(data.trial(1).channel(n).waveform)
        wavelen=length(data.trial(1).channel(n).waveform);
        fs=data.trial(1).channel(n).waveformhz;
        break;
    end
end

if wavelen==0
    error('No stimulus waveforms!');
end

% Go through all the trials, picking out the stimuli and the spikes within
% the reverse correlation window.

%Number of trials with BC = 0
stimsL=zeros(length(data.trial),wavelen);
stimsR=zeros(length(data.trial),wavelen);

spikes=cell(length(data.trial),1);
for n=1:length(data.trial)
    for nn=1:length(data.trial(n).channel)
        if ~isempty(data.trial(n).channel(nn).waveform)
            
            
            if nn==2                                        %Left = 2
            stimsL(n,:)=data.trial(n).channel(nn).waveform;
            elseif nn==3                                    %Right = 3
            stimsR(n,:)=data.trial(n).channel(nn).waveform;
            end
            
        end
    end
    for nn=1:length(data.trial(n).channel)
        if ~isempty(data.trial(n).channel(nn).spikes)
            sp=data.trial(n).channel(nn).spikes*data.trial(n).channel(nn).spikestoms;
            spikes{n}=sp(sp>window(1) & sp<window(2));
            break;
        end
    end
end