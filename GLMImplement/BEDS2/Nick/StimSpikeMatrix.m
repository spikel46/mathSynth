function StimSpikeMatrix(spikes, stims, Tstim, Tref, dl, du, ep)

% function [X]=StimSpikeMatrix(spikes, stims, Tstim, Tref)
% creates matrix with all times as rows and the preceding stimulus and
% spikes as the cols
% Tstim is the amount of preceding stimulus taken to affect the cell for a
% certain spike
% Tref is the same for the spikes (refractory period, etc.)

%% Params

SR = 48077;
Tin = dl + Tstim;
Tfinal = du + dl;

%% Set matrix points

[Nt, Lt] = size(stims);
InStimTime = round(Tin/ep*Lt);
StimTime = round(Tfinal/ep*Lt);

Jstim = ceil(Tstim*SR/1000);
Jref = ceil(Tref*SR/1000);

%% Remove the initial component

I = 2*Jstim;

%% Get the response

BINSIZE = 1000/SR;
r = doPSTH(spikes(1,:),Tin,Tfinal,BINSIZE);
Ns = length(r);
R = zeros(Nt*Ns,1);

for n=1:Nt
    R((n-1)*Ns+1:n*Ns)=doPSTH(spikes(n,:),Tin,Tfinal,BINSIZE);
end

size(R);

%% Get stimulus

Xstim = zeros(Nt*Ns,1);

for n=1:Nt
    Xstim((n-1)*Ns+1:n*Ns,1) = stims(n, InStimTime:StimTime);
end

%% Form the stimulus matrix

L = size(Xstim, 1) - I;

X = zeros(L, Jstim+Jref);

for i=1:L
    
    in = i + I + 1 - (1:Jstim);
    X(i, 1:Jstim) = Xstim(in);
    
    in = i + I + 1 - (1:Jref);
    X(i, Jstim+1:Jstim+Jref) = R(in);
    
end











