function spikes=getBC0spikes(data)



%Get calibration file
%cal=readXDPHYScalib('ear.cal');


% [STA,PESE,spikes,stims]=computeBC0ReverseCorrelation(data,calib)
% data = BEDS format data.  Single file only.
% calibfile = the path to the appropriate calibration file.
% side = number corresponding to the left (2) or right (3) sides
% STA = spike triggered average.
% PESE = the pre-event stimulus ensemble (ie, for each spike, the segment
% of stimulus preceding it)
% spikes = the spikes used in the reverse correlation
% stims = the stimuli used in the reverse correlation.

gatetime=5; % Chunk of the stimulus to discard because of the gate.
rcwin=10; % Time in ms to use for the reverse correlation analysis.

% Get the stimulus time parameters, and find the window of interest.
[ep,du,dl]=BEDSgetEpoch(data);
window = [dl+gatetime du+dl-gatetime]; 

% Read in the calibration and then compensate for it.
%cal=readXDPHYScalib(calibfile);
%data=BEDSuncalibrateStimulus(data,cal);

%Get BC value
%BCvalue = BEDSgetStimParam(data,'bc');



spikes=cell(length(data.trial),1);
for n=1:length(data.trial)
    
    for nn=1:length(data.trial(n).channel)
        if ~isempty(data.trial(n).channel(nn).spikes)
            sp=data.trial(n).channel(nn).spikes*data.trial(n).channel(nn).spikestoms;
            spikes{n}=sp(sp>window(1) & sp<window(2));
            break;
        end
    end
end

