function makenewgIID(spikes, dl, Tmin, Tmax, BINSIZE, titlename, xmax, du)

% makenewgIID(spikes, dl, Tmin, Tmax, BINSIZE, titlename, xmax)
% creates IID with overlayed gamma function 

%% Firing rate

Ntrial=length(spikes);
countspikes = 0;
for n=1:Ntrial
    countspikes = countspikes + length(spikes{n});
end


firingrate = countspikes/(Ntrial*du);

%% Generate IID and overlay the plot


IID(spikes,Tmin, Tmax, dl, BINSIZE,titlename, xmax, du);
ymax = ylim;


syms x;

%get IItimes
IItimes = IIDvector(spikes, dl);

newalldiffs = IItimes;
clear IItimes;
IItimes = [];

for i=1:length(newalldiffs)
    if(newalldiffs(i) > 0)
        IItimes = [IItimes newalldiffs(i)];
    end
end

phat = gamfit(IItimes);

%do the plotting



h = ezplot(0.5*countspikes*(1/(gamma(phat(1))*phat(2)^(phat(1)))*x^(phat(1)-1)*exp(-x/phat(2))), [0, xmax]);
hold on;
IID(spikes,Tmin, Tmax, dl, BINSIZE,titlename, xmax, du);
legend('off');
ylim(ymax);

chH = get(gca, 'Children');
set(gca, 'Children',[chH(end);chH(1:end-1)]);

set(h, 'Color', 'red', 'LineWidth', 2.5)




%% Calculate and add CV text

CV = CofV(spikes, dl);
CVstring = strcat('CV = ', ' ', num2str(CV));
xstuff = xlim;
ystuff = ylim;
text(5*xstuff(2)/10, 9*ystuff(2)/10, CVstring);

%% Calculate and add Fano Factor

FF = FanoFactor(spikes, dl);
FFstring = strcat('FF = ', ' ', num2str(FF));
xstuff = xlim;
ystuff = ylim;
text(5*xstuff(2)/10, 8*ystuff(2)/10, FFstring);

%% Calculate and add KStest text

KS = dokstestGamma(spikes, firingrate, dl, phat);
KSstring = strcat('KS Test P-Value = ', ' ', num2str(KS));
xstuff = xlim;
ystuff = ylim;
text(5*xstuff(2)/10, 7*ystuff(2)/10, KSstring);

%% Calculate and add Chisquare text

C2 = doChi2GOF(spikes, dl, 'gam');
C2string = strcat('Chi Square GOF Test P-Value = ', ' ', num2str(C2));
xstuff = xlim;
ystuff = ylim;
text(5*xstuff(2)/10, 6*ystuff(2)/10, C2string);



