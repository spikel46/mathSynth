function [IIDvec]=IIDvector(spikes, dl)

%[IIDvec] = IIDvector(spikes, dl)
%Assumes spikes is a cell array with a cell of spike times for each trial



Ntrial=length(spikes);
veclength = 0;

for i=1:Ntrial
    veclength = veclength + length(spikes{i});
end

IIDvec=zeros(veclength , 1);


%% Takes differences of each trial, adds in the first time minus the delay,
% then adds to the IIDvec
counter = 0;

for n=1:Ntrial
    sp=spikes{n};
    if ~isempty(sp)
    diffs = diff(sp);
    diffs(length(diffs) + 1) = sp(1) - dl;
    for k=1:length(diffs)
        counter = counter + 1;
        IIDvec(counter) = diffs(k);
    end
    end 
end

    
