function [efr] = efrVIAoptimizationIPP(spikes, time, width, rincrement)

% Finds firing rate at time = time that maximizes the probability of each 
% spike train in spikes assuming an inhomogeneous poisson process

% the firing rate is assumed constant for width of time centered on time

numtrials = length(spikes);

% Optimization will be done by evaluated by multiplying probabilities

maxprob = -realmax;
currentprob = 0;

%% Get rid of extraneous values

newspikes = cell(numtrials, 1)
increment = 0

for k=1:numtrials
    for i=1:length(spikes{k})
        if spikes{k}(i) < time + 0.5*width
            if spikes{k}(i) > time - 0.5*width
                increment = increment + 1;
                newspikes{k}(increment) = spikes{k}(i);
            end
        end
    end
    increment = 0;
end


%% Evaluate Trials - rincrement is the max res. of the efr

number = 0
maxnumber = 0

for r=0 + rincrement:rincrement:1
    number = number + 1;
    for k=1:numtrials
        currentprob = currentprob+ (length(newspikes{k})*log(r)-r*width);
    end
    if (currentprob > maxprob)
        maxprob = currentprob;
        maxnumber = number;
    end
    currentprob = 1;
end


%% Estimate the firing rate
efr = maxnumber*rincrement;

% Very interesting note: gives the same value as the other one, but it much
% more time-consuming








