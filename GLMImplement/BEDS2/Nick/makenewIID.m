function makenewIID(spikes, dl, Tmin, Tmax, BINSIZE, titlename, xmax, du)

% makenewIID(spikes, dl, Tmin, Tmax, BINSIZE, titlename, xmax)
% creates IID with overlayed exponential function and with CV value

%% Firing rate

Ntrial=length(spikes);
countspikes = 0;
for n=1:Ntrial
    countspikes = countspikes + length(spikes{n});
end

firingrate = countspikes/(Ntrial*du);


%% Generate IID and overlay the plot

%fh = @exp(-firingrate*x)*firingrate

%fplot(fh);

IID(spikes,Tmin, Tmax, dl, BINSIZE,titlename, xmax, du);
ymax = ylim;


syms x;

h = ezplot(0.5*countspikes*exp(-firingrate*x)*firingrate, [0, xmax]);
hold on;
IID(spikes,Tmin, Tmax, dl, BINSIZE,titlename, xmax, du);
legend('off');
ylim(ymax);

chH = get(gca, 'Children');
set(gca, 'Children',[chH(end);chH(1:end-1)]);

set(h, 'Color', 'red', 'LineWidth', 2.5)


%% Calculate and add CV text

CV = CofV(spikes, dl);
CVstring = strcat('CV = ', ' ', num2str(CV));
xstuff = xlim;
ystuff = ylim;
text(5*xstuff(2)/10, 9*ystuff(2)/10, CVstring);

%% Calculate and add Fano Factor

FF = FanoFactor(spikes, dl);
FFstring = strcat('FF = ', ' ', num2str(FF));
xstuff = xlim;
ystuff = ylim;
text(5*xstuff(2)/10, 8*ystuff(2)/10, FFstring);

%% Calculate and add KStest text

KS = dokstest(spikes, firingrate, dl);
KSstring = strcat('KS Test P-Value = ', ' ', num2str(KS));
xstuff = xlim;
ystuff = ylim;
text(5*xstuff(2)/10, 7*ystuff(2)/10, KSstring);

%% Calculate and add Chisquare text

C2 = doChi2GOF(spikes, dl, 'exp');
C2string = strcat('Chi Square GOF Test P-Value = ', ' ', num2str(C2));
xstuff = xlim;
ystuff = ylim;
text(5*xstuff(2)/10, 6*ystuff(2)/10, C2string);









