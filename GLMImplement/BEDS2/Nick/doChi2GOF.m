function [p]=doChi2GOF(spikes, dl, shape)

% function [p]=doChi2GOF(spikes, dl)
% Does chi square goodness of fit test, returns p-value

%% Get all interspike intervals

alldiffs = [];
Ntrial = length(spikes);

for n=1:Ntrial
    sp=spikes{n};
    if ~isempty(sp)
    diffs = diff(sp);
    diffs(length(diffs) + 1) = sp(1) - dl;
    alldiffs = [alldiffs diffs];
    end 
end


%% Get rid of negatives

newalldiffs = alldiffs;
clear alldiffs;
alldiffs = [];

for i=1:length(newalldiffs)
    if(newalldiffs(i) > 0)
        alldiffs = [alldiffs newalldiffs(i)];
    end
end

%% Do the test

pd = fitdist(transpose(alldiffs), shape);
[h,p] = chi2gof(transpose(alldiffs),'CDF',pd);