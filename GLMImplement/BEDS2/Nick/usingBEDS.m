daclear all;


load('/Users/fischer9/Dropbox/MATLAB/OwlData/forBrian/NM/2003Nov06/2003Nov06-863.03.1.abi.mat')

%%
data=a863u03f1;


ABL = unique(BEDSgetStimParam(data,'abi'))

ABLtrials=BEDSgetStimParam(data,'abi');

[ep,du,dl]=BEDSgetEpoch(data);


%%
spikes=getBC0spikes(data);



ind1=find(ABLtrials==30);
ind2=find(ABLtrials==40);

spikes1=spikes(ind1);
spikes2=spikes(ind2);

%%

%sp1=spikes(ABLtrials==40);

%%

[PSTH,BINS]=doPSTH(spikes1,0,ep,1);


figure(1);clf;
bar(BINS,PSTH);
xlabel('Time','fontsize',18)
ylabel('Spike count','fontsize',18)
axis([min(BINS) max(BINS) 0 max(PSTH)*1.1])